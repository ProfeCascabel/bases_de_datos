drop database if exists cute_village;
create database cute_village;
use cute_village;

-- MySQL dump 10.16  Distrib 10.1.38-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: cute_village
-- ------------------------------------------------------
-- Server version	10.1.38-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


--
-- Table structure for table `casa`
--

DROP TABLE IF EXISTS `casa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `casa` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `n_habitacion` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `casa`
--

LOCK TABLES `casa` WRITE;
/*!40000 ALTER TABLE `casa` DISABLE KEYS */;
/*!40000 ALTER TABLE `casa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ropa`
--

DROP TABLE IF EXISTS `ropa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ropa` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(250) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ropa`
--

LOCK TABLES `ropa` WRITE;
/*!40000 ALTER TABLE `ropa` DISABLE KEYS */;
/*!40000 ALTER TABLE `ropa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `habitacion`
--

DROP TABLE IF EXISTS `habitacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `habitacion` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_casa` int(10) unsigned NOT NULL,
  `tipo` enum('habitación','cocina','baño','laboratorio','garaje','buhardilla') NOT NULL,
  foreign key (id_casa) references casa(id),
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `habitacion`
--

LOCK TABLES `habitacion` WRITE;
/*!40000 ALTER TABLE `habitacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `habitacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inferior`
--

DROP TABLE IF EXISTS `inferior`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inferior` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_ropa` int(10) unsigned NOT NULL,
  `color` varchar(20) NOT NULL,
  foreign key(id_ropa) references ropa(id),
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inferior`
--

LOCK TABLES `inferior` WRITE;
/*!40000 ALTER TABLE `inferior` DISABLE KEYS */;
/*!40000 ALTER TABLE `inferior` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `objeto`
--

DROP TABLE IF EXISTS `objeto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `objeto` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) NOT NULL,
  `tipo` enum('mueble','decoración de mesa','decoración de pared','herramienta') NOT NULL,
  `tamaño` varchar(20) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `objeto`
--

LOCK TABLES `objeto` WRITE;
/*!40000 ALTER TABLE `objeto` DISABLE KEYS */;
/*!40000 ALTER TABLE `objeto` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `sombrero`
--

DROP TABLE IF EXISTS `sombrero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sombrero` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_ropa` int(10) unsigned NOT NULL,
  `peinado` tinyint(1) NOT NULL,
  foreign key(id_ropa) references ropa(id),
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sombrero`
--

LOCK TABLES `sombrero` WRITE;
/*!40000 ALTER TABLE `sombrero` DISABLE KEYS */;
/*!40000 ALTER TABLE `sombrero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `superior`
--

DROP TABLE IF EXISTS `superior`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `superior` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_ropa` int(10) unsigned NOT NULL,
  `tipo` enum('masculina','femenina','unisex') NOT NULL,
  foreign key(id_ropa) references ropa(id),
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `superior`
--

LOCK TABLES `superior` WRITE;
/*!40000 ALTER TABLE `superior` DISABLE KEYS */;
/*!40000 ALTER TABLE `superior` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calzado`
--

DROP TABLE IF EXISTS `calzado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calzado` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_ropa` int(10) unsigned DEFAULT NULL,
  `terreno` varchar(20) NOT NULL,
  `color` varchar(20) NOT NULL,
  foreign key(id_ropa) references ropa(id),
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calzado`
--

LOCK TABLES `calzado` WRITE;
/*!40000 ALTER TABLE `calzado` DISABLE KEYS */;
/*!40000 ALTER TABLE `calzado` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `avatar`
--

DROP TABLE IF EXISTS `avatar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avatar` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_superior` int(10) unsigned NOT NULL,
  `id_inferior` int(10) unsigned NOT NULL,
  `id_calzado` int(10) unsigned DEFAULT NULL,
  `id_sombrero` int(10) unsigned DEFAULT NULL,
  `nombre` varchar(50) NOT NULL,
  `modelo` varchar(50) NOT NULL,
  `color` varchar(20) NOT NULL,
  `expresion_facial` enum('pasota','triste','contento','pensativo','flipao') NOT NULL,
  foreign key(id_superior) references superior(id),
  foreign key(id_inferior) references inferior(id),
  foreign key(id_calzado) references calzado(id),
  foreign key(id_sombrero) references sombrero(id),
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avatar`
--

LOCK TABLES `avatar` WRITE;
/*!40000 ALTER TABLE `avatar` DISABLE KEYS */;
/*!40000 ALTER TABLE `avatar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `avatar_casa`
--

DROP TABLE IF EXISTS `avatar_casa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avatar_casa` (
  `id_avatar` int(10) unsigned NOT NULL,
  `id_casa` int(10) unsigned NOT NULL,
  fecha_entrada date not null,
  foreign key(id_avatar) references avatar(id),
  foreign key(id_casa) references casa(id),
  PRIMARY KEY (`id_avatar`,`id_casa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avatar_casa`
--

LOCK TABLES `avatar_casa` WRITE;
/*!40000 ALTER TABLE `avatar_casa` DISABLE KEYS */;
/*!40000 ALTER TABLE `avatar_casa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `avatar_objeto`
--

DROP TABLE IF EXISTS `avatar_objeto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avatar_objeto` (
  `id_avatar` int(10) unsigned NOT NULL,
  `id_objeto` int(10) unsigned NOT NULL,
  fecha_adquisicion date not null,
  foreign key(id_avatar) references avatar(id),
  foreign key(id_objeto) references objeto(id),
  PRIMARY KEY (`id_avatar`,`id_objeto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avatar_objeto`
--

LOCK TABLES `avatar_objeto` WRITE;
/*!40000 ALTER TABLE `avatar_objeto` DISABLE KEYS */;
/*!40000 ALTER TABLE `avatar_objeto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `avatar_relacion_avatar`
--

DROP TABLE IF EXISTS `avatar_relacion_avatar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avatar_relacion_avatar` (
  `id_avatar_ejerce` int(10) unsigned NOT NULL,
  `id_avatar_recibe` int(10) unsigned NOT NULL,
  fecha date not null,
  `tipo` enum('matrimonio','adopta') NOT NULL,
  foreign key(id_avatar_ejerce) references avatar(id),
  foreign key(id_avatar_recibe) references avatar(id),
  PRIMARY KEY (`id_avatar_ejerce`,`id_avatar_recibe`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avatar_relacion_avatar`
--

LOCK TABLES `avatar_relacion_avatar` WRITE;
/*!40000 ALTER TABLE `avatar_relacion_avatar` DISABLE KEYS */;
/*!40000 ALTER TABLE `avatar_relacion_avatar` ENABLE KEYS */;
UNLOCK TABLES;




--
-- Table structure for table `habitacion_objeto`
--

DROP TABLE IF EXISTS `habitacion_objeto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `habitacion_objeto` (
  `id_habitacion` int(10) unsigned NOT NULL,
  `id_objeto` int(10) unsigned NOT NULL,
  `x` int(11) NOT NULL,
  `y` int(11) NOT NULL,
  `orientacion` enum('N','S','E','O') NOT NULL,
  foreign key(id_habitacion) references habitacion(id),
  foreign key(id_objeto) references objeto(id),
  PRIMARY KEY (`id_habitacion`,`id_objeto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `habitacion_objeto`
--

LOCK TABLES `habitacion_objeto` WRITE;
/*!40000 ALTER TABLE `habitacion_objeto` DISABLE KEYS */;
/*!40000 ALTER TABLE `habitacion_objeto` ENABLE KEYS */;
UNLOCK TABLES;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-20 20:39:09




SET NAMES 'utf8';
USE cute_village;


--
-- Inserting data into table ropa
--
INSERT INTO ropa(ID, descripcion, nombre) VALUES
(1, 'La última tendencia de la marca RookieFish', 'Cool Attire'),
(2, 'La última tendencia de la marca Lil''Fish', 'Great Pirate'),
(3, 'La última tendencia de la marca Lil''', 'Great Pirate'),
(4, 'La última tendencia de la marca RookieFish', 'Cute Thing'),
(5, 'La última tendencia de la marca Big Sun', 'Hyper-Duper-Cool Outfit'),
(6, ' Un básico de la marca Little Butterfly', 'Great Pirate'),
(7, 'La última tendencia de la marca Little ', 'Duper-Cool Attire'),
(8, 'La última tendencia de la marca RookieFish', 'Hyper-Duper-Great Thing'),
(9, ' Un básico de la marca Pink Fish', 'Cute Thing'),
(10, 'La última tendencia de la marca Gloom ', 'Outfit'),
(11, 'La última tendencia de la marca Gloom Butterfly', 'Duper-Pirate'),
(12, ' Un básico de la marca Lil''Butterfly', 'Attire'),
(13, ' Un básico de la marca Big Star', 'Hyper-Great Outfit'),
(14, 'La última tendencia de la marca Little Sun', 'Hyper-Duper-Cute Thing'),
(15, 'La última tendencia de la marca RookieButterfly', 'Hyper-Attire'),
(16, ' Un básico de la marca Big ', 'Outfit'),
(17, 'La última tendencia de la marca Cute Fish', 'Duper-Outfit'),
(18, 'La última tendencia de la marca Big Fish', 'Hyper-Great Pirate'),
(19, 'La última tendencia de la marca Little Fish', 'Great Outfit'),
(20, 'La última tendencia de la marca Lil''Star', 'Hyper-Duper-Cute Attire'),
(21, 'La última tendencia de la marca Gloom Star', 'Hyper-Cool XTreme'),
(22, 'La última tendencia de la marca Pink Sun', 'Great Attire'),
(23, ' Un básico de la marca Cute Fish', 'Great Outfit'),
(24, ' Un básico de la marca Gloom Fish', 'Duper-Attire'),
(25, 'La última tendencia de la marca RookieButterfly', 'Hyper-Duper-Cute Outfit'),
(26, 'La última tendencia de la marca Gloom ', 'Duper-Great Pirate'),
(27, ' Un básico de la marca Big Butterfly', 'Outfit'),
(28, 'La última tendencia de la marca Cute Star', 'Duper-Great Pirate'),
(29, ' Un básico de la marca Pink Sun', 'Hyper-Duper-Great Outfit'),
(30, 'La última tendencia de la marca Little ', 'Thing'),
(31, 'La última tendencia de la marca Pink Sun', 'Hyper-Duper-Cute Thing'),
(32, ' Un básico de la marca Cute Star', 'Cute Outfit'),
(33, 'La última tendencia de la marca Little Fish', 'Cute Outfit'),
(34, 'La última tendencia de la marca Pink Star', 'Hyper-Duper-Rockstar'),
(35, ' Un básico de la marca Lil''Star', 'XTreme'),
(36, ' Un básico de la marca Lil''Star', 'Duper-Great XTreme'),
(37, ' Un básico de la marca RookieButterfly', 'Duper-Cute Attire'),
(38, 'La última tendencia de la marca Gloom ', 'Cute XTreme'),
(39, ' Un básico de la marca Lil''Butterfly', 'Hyper-Great Rockstar'),
(40, 'La última tendencia de la marca Cute Butterfly', 'Great Attire'),
(41, 'La última tendencia de la marca Lil''Fish', 'Hyper-Great Outfit'),
(42, 'La última tendencia de la marca Cute Fish', 'Duper-Cool Thing'),
(43, ' Un básico de la marca Gloom Star', 'Outfit'),
(44, 'La última tendencia de la marca Cute Fish', 'Duper-Cute Outfit'),
(45, ' Un básico de la marca Cute ', 'Hyper-Duper-Cute XTreme'),
(46, ' Un básico de la marca Little Star', 'Cool Rockstar'),
(47, ' Un básico de la marca Gloom Star', 'Great XTreme'),
(48, 'La última tendencia de la marca Big ', 'Hyper-Great XTreme'),
(49, 'La última tendencia de la marca Little Butterfly', 'Cool Pirate'),
(50, ' Un básico de la marca Gloom ', 'Great Thing'),
(51, 'La última tendencia de la marca Little ', 'Attire'),
(52, 'La última tendencia de la marca Lil''Fish', 'Cute Rockstar'),
(53, ' Un básico de la marca RookieSun', 'Great Pirate'),
(54, 'La última tendencia de la marca Lil''Sun', 'Hyper-Cool XTreme'),
(55, 'La última tendencia de la marca Gloom Fish', 'Great Pirate'),
(56, ' Un básico de la marca Gloom ', 'Duper-Great Attire'),
(57, 'La última tendencia de la marca RookieStar', 'Cool Rockstar'),
(58, ' Un básico de la marca Little Fish', 'Duper-Cool Outfit'),
(59, ' Un básico de la marca Lil''Fish', 'Cool Outfit'),
(60, 'La última tendencia de la marca Gloom Fish', 'Hyper-Great Outfit'),
(61, ' Un básico de la marca Gloom Butterfly', 'Cute Thing'),
(62, 'La última tendencia de la marca RookieButterfly', 'Cute Thing'),
(63, 'La última tendencia de la marca RookieFish', 'Attire'),
(64, ' Un básico de la marca Pink Fish', 'Great Pirate'),
(65, 'La última tendencia de la marca Little Star', 'Cute Outfit'),
(66, 'La última tendencia de la marca Pink Star', 'Duper-Cute Attire'),
(67, ' Un básico de la marca Pink ', 'Outfit'),
(68, ' Un básico de la marca Big Fish', 'Great Pirate'),
(69, 'La última tendencia de la marca Big Butterfly', 'Great Pirate'),
(70, ' Un básico de la marca Lil''Star', 'Attire'),
(71, ' Un básico de la marca Cute Fish', 'Rockstar'),
(72, ' Un básico de la marca Cute Star', 'Cool XTreme'),
(73, ' Un básico de la marca Big Fish', 'Thing'),
(74, ' Un básico de la marca Lil''Fish', 'Hyper-Cute Thing'),
(75, ' Un básico de la marca Lil''Fish', 'Cool Outfit'),
(76, ' Un básico de la marca Rookie', 'Hyper-Duper-Cute Pirate'),
(77, 'La última tendencia de la marca Little Fish', 'Hyper-Cool Outfit'),
(78, ' Un básico de la marca Lil''Fish', 'Great Thing'),
(79, 'La última tendencia de la marca Big Butterfly', 'Cool Rockstar'),
(80, 'La última tendencia de la marca Cute Sun', 'Great Thing'),
(81, 'La última tendencia de la marca Lil''', 'Hyper-Great Outfit'),
(82, 'La última tendencia de la marca Pink Star', 'Cool Pirate'),
(83, ' Un básico de la marca Cute Fish', 'Hyper-Duper-Cute Pirate'),
(84, 'La última tendencia de la marca Cute Star', 'Cool Rockstar'),
(85, 'La última tendencia de la marca Lil''Butterfly', 'Great Thing'),
(86, 'La última tendencia de la marca Little Sun', 'Duper-Cool Outfit'),
(87, ' Un básico de la marca RookieStar', 'Hyper-Duper-Cute XTreme'),
(88, 'La última tendencia de la marca RookieFish', 'Cool XTreme'),
(89, ' Un básico de la marca Lil''Sun', 'Duper-Cool Attire'),
(90, ' Un básico de la marca Cute ', 'Cool Outfit'),
(91, 'La última tendencia de la marca RookieButterfly', 'Cute Pirate'),
(92, 'La última tendencia de la marca RookieFish', 'Great Rockstar'),
(93, 'La última tendencia de la marca Little Fish', 'Attire'),
(94, ' Un básico de la marca Big Fish', 'Cute Rockstar'),
(95, ' Un básico de la marca Lil''Fish', 'Cute Outfit'),
(96, ' Un básico de la marca Little Sun', 'Duper-Pirate'),
(97, ' Un básico de la marca RookieFish', 'Hyper-Great Thing'),
(98, 'La última tendencia de la marca Lil''', 'Cool Rockstar'),
(99, ' Un básico de la marca Pink ', 'Hyper-Duper-Great XTreme'),
(100, 'La última tendencia de la marca Pink Sun', 'Hyper-Duper-Cool Attire'),
(101, ' Un básico de la marca Cute ', 'Duper-Cool Attire'),
(102, 'La última tendencia de la marca Pink ', 'Duper-Great Thing'),
(103, ' Un básico de la marca Lil''Sun', 'Duper-Cool Attire'),
(104, ' Un básico de la marca Big Fish', 'Cool Rockstar'),
(105, 'La última tendencia de la marca Rookie', 'Hyper-Attire'),
(106, ' Un básico de la marca Cute Star', 'Hyper-XTreme'),
(107, 'La última tendencia de la marca Pink Butterfly', 'Duper-Rockstar'),
(108, ' Un básico de la marca Gloom ', 'Rockstar'),
(109, ' Un básico de la marca Pink Butterfly', 'Hyper-Rockstar'),
(110, 'La última tendencia de la marca Cute Star', 'Hyper-Thing'),
(111, 'La última tendencia de la marca Big ', 'Duper-Great Rockstar'),
(112, ' Un básico de la marca Gloom Sun', 'Great Outfit'),
(113, 'La última tendencia de la marca Big Fish', 'Great XTreme'),
(114, 'La última tendencia de la marca RookieSun', 'Hyper-Attire'),
(115, 'La última tendencia de la marca Big Butterfly', 'Cool Outfit'),
(116, ' Un básico de la marca Lil''Fish', 'Cool Pirate'),
(117, 'La última tendencia de la marca Lil''Butterfly', 'Cool Attire'),
(118, 'La última tendencia de la marca RookieButterfly', 'XTreme'),
(119, 'La última tendencia de la marca Lil''Star', 'Hyper-Duper-Great XTreme'),
(120, 'La última tendencia de la marca RookieButterfly', 'Cool Pirate');

--
-- Inserting data into table casa
--
INSERT INTO casa(ID, nombre, n_habitacion) VALUES
(1, 'Mansión El valle del Festejo', 2),
(2, 'Cortijo ChupiAlegría ', 2),
(3, 'Cortijo Chez Alegría ', 2),
(4, 'Rancho Felicidad', 1),
(5, 'Festejo', 4),
(6, 'Mansión Alegría ', 1),
(7, 'Mansión Cantora', 4),
(8, 'El valle de la Felicidad', 1),
(9, 'Villa Alegría ', 5),
(10, 'Cortijo Cantora', 1),
(11, 'Rancho El valle del Jolgorio', 1),
(12, 'El pequeño Jolgorio', 3),
(13, 'Chez Felicidad', 1),
(14, 'Rancho La pequeña Felicidad', 2),
(15, 'Masía El valle de la Alegría ', 5),
(16, 'Cortijo El valle del Jolgorio', 2),
(17, 'Alegría ', 3),
(18, 'Rancho Chez Alegría ', 3),
(19, 'Rancho Relaxo', 1),
(20, 'Cortijo Jolgorio', 5),
(21, 'Mansión El pequeño Jolgorio', 4),
(22, 'Cortijo ChupiFelicidad', 4),
(23, 'Rancho ChupiFelicidad', 4),
(24, 'La pequeña Alegría ', 1),
(25, 'Mansión Chez Felicidad', 3),
(26, 'Mansión La pequeña Felicidad', 1),
(27, 'Cortijo Relaxo', 5),
(28, 'Relaxo', 2),
(29, 'Masía ChupiAlegría ', 2),
(30, 'Villa Jolgorio', 2),
(31, 'Mansión Festejo', 5),
(32, 'Rancho Cantora', 4),
(33, 'Masía Cantora', 5),
(34, 'Masía Chez Festejo', 1),
(35, 'Villa Relaxo', 4),
(36, 'Rancho Jolgorio', 4),
(37, 'Villa Cantora', 5),
(38, 'Masía ChupiFestejo', 5),
(39, 'Chez Jolgorio', 3),
(40, 'Cortijo Felicidad', 3),
(41, 'Cortijo Chez Jolgorio', 4),
(42, 'Masía Alegría ', 5),
(43, 'Cortijo Alegría ', 1),
(44, 'Villa Chez Felicidad', 2),
(45, 'Masía La pequeña Felicidad', 1),
(46, 'Mansión Jolgorio', 3),
(47, 'Mansión ChupiFelicidad', 3),
(48, 'Villa El pequeño Festejo', 4),
(49, 'ChupiFelicidad', 3),
(50, 'Felicidad', 5);

--
-- Inserting data into table habitacion
--
INSERT INTO habitacion(ID, id_casa, tipo) VALUES
(1, 20, 'garaje'),
(2, 12, 'buhardilla'),
(3, 38, 'garaje'),
(4, 35, 'baño'),
(5, 38, 'buhardilla'),
(6, 47, 'laboratorio'),
(7, 24, 'garaje'),
(8, 7, 'buhardilla'),
(9, 8, 'cocina'),
(10, 49, 'garaje'),
(11, 36, 'buhardilla'),
(12, 35, 'buhardilla'),
(13, 20, 'laboratorio'),
(14, 50, 'laboratorio'),
(15, 22, 'buhardilla'),
(16, 50, 'garaje'),
(17, 34, 'habitación'),
(18, 9, 'garaje'),
(19, 19, 'cocina'),
(20, 40, 'buhardilla'),
(21, 40, 'cocina'),
(22, 43, 'habitación'),
(23, 16, 'buhardilla'),
(24, 23, 'buhardilla'),
(25, 47, 'laboratorio'),
(26, 8, 'laboratorio'),
(27, 32, 'garaje'),
(28, 22, 'baño'),
(29, 23, 'baño'),
(30, 1, 'buhardilla'),
(31, 39, 'laboratorio'),
(32, 43, 'buhardilla'),
(33, 22, 'cocina'),
(34, 48, 'laboratorio'),
(35, 39, 'garaje'),
(36, 15, 'garaje'),
(37, 33, 'cocina'),
(38, 39, 'buhardilla'),
(39, 5, 'baño'),
(40, 33, 'garaje'),
(41, 39, 'garaje'),
(42, 1, 'laboratorio'),
(43, 26, 'buhardilla'),
(44, 10, 'baño'),
(45, 11, 'buhardilla'),
(46, 20, 'buhardilla'),
(47, 50, 'cocina'),
(48, 23, 'buhardilla'),
(49, 25, 'buhardilla'),
(50, 35, 'laboratorio'),
(51, 2, 'laboratorio'),
(52, 31, 'laboratorio'),
(53, 27, 'buhardilla'),
(54, 4, 'buhardilla'),
(55, 5, 'garaje'),
(56, 22, 'garaje'),
(57, 21, 'buhardilla'),
(58, 13, 'garaje'),
(59, 37, 'buhardilla'),
(60, 25, 'laboratorio'),
(61, 26, 'buhardilla'),
(62, 49, 'laboratorio'),
(63, 41, 'buhardilla'),
(64, 6, 'laboratorio'),
(65, 25, 'cocina'),
(66, 50, 'buhardilla'),
(67, 6, 'garaje'),
(68, 6, 'buhardilla'),
(69, 30, 'buhardilla'),
(70, 24, 'laboratorio'),
(71, 44, 'garaje'),
(72, 22, 'habitación'),
(73, 19, 'baño'),
(74, 39, 'garaje'),
(75, 22, 'laboratorio'),
(76, 1, 'garaje'),
(77, 42, 'baño'),
(78, 26, 'garaje'),
(79, 29, 'cocina'),
(80, 26, 'garaje'),
(81, 13, 'laboratorio'),
(82, 5, 'buhardilla'),
(83, 38, 'laboratorio'),
(84, 25, 'buhardilla'),
(85, 19, 'laboratorio'),
(86, 29, 'cocina'),
(87, 30, 'buhardilla'),
(88, 36, 'baño'),
(89, 16, 'buhardilla'),
(90, 9, 'buhardilla'),
(91, 12, 'garaje'),
(92, 38, 'laboratorio'),
(93, 3, 'baño'),
(94, 2, 'buhardilla'),
(95, 22, 'buhardilla'),
(96, 18, 'baño'),
(97, 45, 'garaje'),
(98, 8, 'garaje'),
(99, 26, 'cocina'),
(100, 18, 'buhardilla'),
(101, 1, 'baño'),
(102, 20, 'baño'),
(103, 17, 'buhardilla'),
(104, 18, 'laboratorio'),
(105, 48, 'buhardilla'),
(106, 21, 'laboratorio'),
(107, 32, 'cocina'),
(108, 17, 'laboratorio'),
(109, 3, 'garaje'),
(110, 14, 'buhardilla'),
(111, 14, 'laboratorio'),
(112, 1, 'baño'),
(113, 18, 'garaje'),
(114, 22, 'baño'),
(115, 10, 'cocina'),
(116, 21, 'habitación'),
(117, 1, 'buhardilla'),
(118, 40, 'buhardilla'),
(119, 16, 'buhardilla'),
(120, 3, 'buhardilla'),
(121, 29, 'buhardilla'),
(122, 36, 'laboratorio'),
(123, 17, 'cocina'),
(124, 24, 'buhardilla'),
(125, 37, 'buhardilla'),
(126, 31, 'cocina'),
(127, 47, 'buhardilla'),
(128, 28, 'laboratorio'),
(129, 37, 'buhardilla'),
(130, 39, 'cocina'),
(131, 40, 'buhardilla'),
(132, 15, 'garaje'),
(133, 32, 'garaje'),
(134, 41, 'cocina'),
(135, 36, 'laboratorio'),
(136, 25, 'cocina'),
(137, 25, 'buhardilla'),
(138, 29, 'buhardilla'),
(139, 14, 'buhardilla'),
(140, 24, 'habitación'),
(141, 38, 'buhardilla'),
(142, 36, 'garaje'),
(143, 24, 'garaje'),
(144, 35, 'buhardilla'),
(145, 27, 'garaje'),
(146, 49, 'laboratorio'),
(147, 26, 'buhardilla'),
(148, 28, 'buhardilla'),
(149, 42, 'buhardilla'),
(150, 34, 'garaje'),
(151, 9, 'laboratorio'),
(152, 6, 'buhardilla'),
(153, 31, 'garaje'),
(154, 12, 'cocina'),
(155, 7, 'cocina'),
(156, 46, 'buhardilla'),
(157, 37, 'cocina'),
(158, 34, 'baño'),
(159, 30, 'garaje'),
(160, 27, 'buhardilla'),
(161, 43, 'laboratorio'),
(162, 6, 'garaje'),
(163, 13, 'garaje'),
(164, 37, 'garaje'),
(165, 38, 'habitación'),
(166, 4, 'buhardilla'),
(167, 26, 'buhardilla'),
(168, 33, 'laboratorio'),
(169, 40, 'buhardilla'),
(170, 50, 'buhardilla'),
(171, 47, 'baño'),
(172, 48, 'buhardilla'),
(173, 39, 'cocina'),
(174, 27, 'buhardilla'),
(175, 6, 'habitación'),
(176, 1, 'laboratorio'),
(177, 29, 'buhardilla'),
(178, 14, 'baño'),
(179, 14, 'baño'),
(180, 36, 'garaje'),
(181, 18, 'baño'),
(182, 3, 'buhardilla'),
(183, 10, 'garaje'),
(184, 4, 'cocina'),
(185, 21, 'buhardilla'),
(186, 5, 'baño'),
(187, 40, 'habitación'),
(188, 30, 'laboratorio'),
(189, 39, 'cocina'),
(190, 44, 'garaje'),
(191, 41, 'buhardilla'),
(192, 10, 'garaje'),
(193, 6, 'laboratorio'),
(194, 8, 'habitación'),
(195, 2, 'garaje'),
(196, 30, 'laboratorio'),
(197, 40, 'buhardilla'),
(198, 15, 'cocina'),
(199, 13, 'buhardilla'),
(200, 26, 'buhardilla'),
(201, 5, 'garaje'),
(202, 38, 'laboratorio'),
(203, 9, 'baño'),
(204, 28, 'buhardilla'),
(205, 8, 'garaje'),
(206, 20, 'laboratorio'),
(207, 38, 'garaje'),
(208, 39, 'buhardilla'),
(209, 48, 'cocina'),
(210, 27, 'garaje'),
(211, 4, 'buhardilla'),
(212, 2, 'buhardilla'),
(213, 14, 'cocina'),
(214, 2, 'cocina'),
(215, 24, 'garaje'),
(216, 3, 'garaje'),
(217, 8, 'buhardilla'),
(218, 31, 'garaje'),
(219, 30, 'buhardilla'),
(220, 20, 'buhardilla'),
(221, 14, 'buhardilla'),
(222, 22, 'laboratorio'),
(223, 35, 'laboratorio'),
(224, 32, 'cocina'),
(225, 34, 'buhardilla'),
(226, 43, 'garaje'),
(227, 7, 'garaje'),
(228, 2, 'cocina'),
(229, 48, 'baño'),
(230, 6, 'garaje'),
(231, 18, 'garaje'),
(232, 49, 'buhardilla'),
(233, 14, 'laboratorio'),
(234, 14, 'laboratorio'),
(235, 34, 'buhardilla'),
(236, 38, 'buhardilla'),
(237, 6, 'buhardilla'),
(238, 41, 'buhardilla'),
(239, 18, 'garaje'),
(240, 21, 'baño'),
(241, 27, 'garaje'),
(242, 9, 'baño'),
(243, 36, 'laboratorio'),
(244, 43, 'laboratorio'),
(245, 5, 'buhardilla'),
(246, 14, 'cocina'),
(247, 32, 'garaje'),
(248, 27, 'laboratorio'),
(249, 29, 'laboratorio'),
(250, 25, 'cocina');

--
-- Inserting data into table inferior
--
INSERT INTO inferior(ID, id_ropa, color) VALUES
(1, 21, 'CoolGrey'),
(2, 22, 'LaserLemon'),
(3, 23, 'BrightMaroon'),
(4, 24, 'Drab'),
(5, 25, 'CrimsonGlory'),
(6, 26, 'SalmonPink'),
(7, 27, 'Rose'),
(8, 28, 'DukeBlue'),
(9, 29, 'ParisGreen'),
(10, 30, 'TropicalRainForest'),
(11, 31, 'RoseBonbon'),
(12, 32, 'Sand'),
(13, 33, 'PastelBlue'),
(14, 34, 'ZinnwalditeBrown'),
(15, 35, 'MediumElectricBlue'),
(16, 36, 'Cyan'),
(17, 37, 'LaurelGreen'),
(18, 38, 'Copper'),
(19, 39, 'MediumJungleGreen'),
(20, 40, 'TrueBlue'),
(21, 41, 'RoseEbony'),
(22, 42, 'SandDune'),
(23, 43, 'TuftsBlue'),
(24, 44, 'PastelBrown'),
(25, 45, 'BrightPink'),
(26, 46, 'RoseGold'),
(27, 47, 'Sandstorm'),
(28, 48, 'Tumbleweed'),
(29, 49, 'CopperRose'),
(30, 50, 'EarthYellow'),
(31, 51, 'BrightTurquoise'),
(32, 52, 'Coquelicot'),
(33, 53, 'SandyBrown'),
(34, 54, 'PastelGray'),
(35, 55, 'Lava'),
(36, 56, 'BrightUbe'),
(37, 57, 'Daffodil'),
(38, 58, 'Coral'),
(39, 59, 'TurkishRose'),
(40, 60, 'Ecru');

--
-- Inserting data into table objeto
--
INSERT INTO objeto(ID, nombre, tipo, tamaño, descripcion) VALUES
(1, 'Niyë', 'herramienta', 'Grande', 'Imprescindible invención para cualquiera'),
(2, 'Bobeä', 'herramienta', 'Pequeño', 'Útil invención de primera necesidad'),
(3, 'Lëzä', 'decoración de mesa', 'Pequeño', 'Imprescindible cosa de buena relación calidad precio'),
(4, 'Sidö', 'mueble', 'Mediano', 'Eficaz objeto para los más exigentes'),
(5, 'Xesu', 'herramienta', 'Grande', 'Eficaz entelequia de primera necesidad'),
(6, 'Cë-bö', 'herramienta', 'Grande', 'Adorablecacharro de la mayor calidad'),
(7, 'Iäki', 'decoración de mesa', 'Pequeño', 'Adorablecosa para el día a día'),
(8, 'Uëii', 'herramienta', 'Pequeño', 'Estridente invención de la mayor calidad'),
(9, 'Kejä', 'decoración de pared', 'Mediano', 'Adorableinvención para cualquiera'),
(10, 'Cärmus', 'mueble', 'Pequeño', 'Eficaz invención de buena relación calidad precio'),
(11, 'Höka', 'herramienta', 'Pequeño', 'Estridente cosa para el día a día'),
(12, 'Tifapi-qö', 'herramienta', 'Pequeño', 'Imprescindible objeto para los más exigentes'),
(13, 'Hagqarezë', 'mueble', 'Grande', 'Imprescindible cosa de la mayor calidad'),
(14, 'Vëd-ooaloio', 'decoración de mesa', 'Pequeño', 'Imprescindible entelequia de la mayor calidad'),
(15, 'Nägöjisu', 'herramienta', 'Mediano', 'Eficaz objeto para cualquiera'),
(16, 'Ya-ti', 'mueble', 'Pequeño', 'Útil cosa de primera necesidad'),
(17, 'Eësäjë', 'herramienta', 'Grande', 'Eficaz invención de primera necesidad'),
(18, 'Sa-qö', 'mueble', 'Mediano', 'Adorablecacharro para el día a día'),
(19, 'Yödno', 'mueble', 'Pequeño', 'Eficaz cacharro de la mayor calidad'),
(20, 'Mef-xöaec', 'decoración de mesa', 'Pequeño', 'Imprescindible cosa para sacarte de un aprieto'),
(21, 'Feie', 'herramienta', 'Grande', 'Adorablecacharro de una calidad mediocre'),
(22, 'Ga-ma', 'mueble', 'Grande', 'Eficaz entelequia para sacarte de un aprieto'),
(23, 'Säypë', 'decoración de pared', 'Mediano', 'Imprescindible invención de una calidad mediocre'),
(24, 'San-qocëjoworj', 'decoración de mesa', 'Grande', 'Adorableentelequia para el día a día'),
(25, 'Gu-jo', 'herramienta', 'Grande', 'Estridente objeto de primera necesidad'),
(26, 'Ji-ooca', 'mueble', 'Grande', 'Adorablecosa de primera necesidad'),
(27, 'Wöfu', 'herramienta', 'Grande', 'Eficaz entelequia de la mayor calidad'),
(28, 'Juxo', 'herramienta', 'Grande', 'Adorablecosa para el día a día'),
(29, 'Vöbubaki', 'decoración de mesa', 'Grande', 'Eficaz entelequia de buena relación calidad precio'),
(30, 'Nupi', 'herramienta', 'Pequeño', 'Adorablecosa de buena relación calidad precio'),
(31, 'Aöza', 'herramienta', 'Grande', 'Imprescindible invención de primera necesidad'),
(32, 'Yözö', 'herramienta', 'Grande', 'Eficaz invención de la mayor calidad'),
(33, 'Luxiway', 'herramienta', 'Pequeño', 'Imprescindible entelequia de primera necesidad'),
(34, 'Fo-eiqäe', 'decoración de pared', 'Pequeño', 'Adorablecosa para el día a día'),
(35, 'Möeu', 'herramienta', 'Mediano', 'Útil cacharro para los más exigentes'),
(36, 'Aaka', 'herramienta', 'Grande', 'Eficaz invención de la mayor calidad'),
(37, 'Couövu', 'decoración de mesa', 'Pequeño', 'Adorableentelequia para los más exigentes'),
(38, 'Möde', 'herramienta', 'Grande', 'Estridente invención de primera necesidad'),
(39, 'Fi-qähelorj', 'herramienta', 'Grande', 'Estridente entelequia para cualquiera'),
(40, 'Eö-tämu', 'herramienta', 'Pequeño', 'Imprescindible cosa para cualquiera'),
(41, 'Dogfë', 'herramienta', 'Grande', 'Estridente invención de buena relación calidad precio'),
(42, 'Vö-këh', 'decoración de pared', 'Mediano', 'Útil invención de primera necesidad'),
(43, 'Rema', 'mueble', 'Mediano', 'Útil cosa de una calidad mediocre'),
(44, 'Oelög', 'herramienta', 'Pequeño', 'Eficaz invención de buena relación calidad precio'),
(45, 'Zëzböeyë-quqëq', 'herramienta', 'Grande', 'Eficaz cosa de buena relación calidad precio'),
(46, 'Nëyqö', 'mueble', 'Pequeño', 'Útil cosa de primera necesidad'),
(47, 'Ziamös', 'decoración de mesa', 'Mediano', 'Eficaz entelequia para el día a día'),
(48, 'Fifsë', 'herramienta', 'Pequeño', 'Eficaz cacharro para el día a día'),
(49, 'Poxe', 'mueble', 'Mediano', 'Útil cosa de primera necesidad'),
(50, 'Bë-töpi', 'herramienta', 'Mediano', 'Estridente invención de la mayor calidad'),
(51, 'Fihu', 'herramienta', 'Pequeño', 'Imprescindible entelequia para cualquiera'),
(52, 'Jäzere', 'mueble', 'Grande', 'Adorableobjeto para sacarte de un aprieto'),
(53, 'Tiläta', 'herramienta', 'Pequeño', 'Estridente objeto de primera necesidad'),
(54, 'Lëzë', 'herramienta', 'Grande', 'Eficaz objeto para sacarte de un aprieto'),
(55, 'Zä-lif', 'decoración de mesa', 'Grande', 'Útil cosa de una calidad mediocre'),
(56, 'Ri-rö', 'herramienta', 'Grande', 'Útil invención de la mayor calidad'),
(57, 'Zähbixö', 'decoración de mesa', 'Grande', 'Eficaz entelequia para cualquiera'),
(58, 'Geda', 'mueble', 'Pequeño', 'Eficaz objeto de primera necesidad'),
(59, 'Pöhu-në', 'mueble', 'Mediano', 'Eficaz cosa de una calidad mediocre'),
(60, 'Baz-xasup', 'herramienta', 'Mediano', 'Estridente invención de la mayor calidad'),
(61, 'Iönebia', 'herramienta', 'Mediano', 'Adorablecosa para el día a día'),
(62, 'Eë-na-höshö', 'herramienta', 'Mediano', 'Adorableobjeto para sacarte de un aprieto'),
(63, 'Val-eäko', 'herramienta', 'Grande', 'Estridente cosa de la mayor calidad'),
(64, 'Ket-basëf', 'decoración de pared', 'Pequeño', 'Estridente objeto para sacarte de un aprieto'),
(65, 'Gä-rë', 'mueble', 'Grande', 'Eficaz cosa de la mayor calidad'),
(66, 'Zösyu-je', 'herramienta', 'Grande', 'Útil cosa de buena relación calidad precio'),
(67, 'Uitofobeorj', 'mueble', 'Pequeño', 'Eficaz invención para el día a día'),
(68, 'Xovo', 'decoración de pared', 'Mediano', 'Estridente invención para el día a día'),
(69, 'Finäou', 'decoración de mesa', 'Pequeño', 'Estridente cosa de buena relación calidad precio'),
(70, 'Kevöqi-ie', 'mueble', 'Mediano', 'Adorablecacharro de buena relación calidad precio'),
(71, 'Ya-gouaorj', 'herramienta', 'Grande', 'Útil cosa para sacarte de un aprieto'),
(72, 'Rab-kä', 'herramienta', 'Pequeño', 'Imprescindible cosa de buena relación calidad precio'),
(73, 'Föoope', 'mueble', 'Grande', 'Estridente cosa de primera necesidad'),
(74, 'Zuoërö', 'decoración de mesa', 'Mediano', 'Útil cosa de una calidad mediocre'),
(75, 'Pëbëoä-loo', 'herramienta', 'Pequeño', 'Adorablecosa de una calidad mediocre'),
(76, 'Raqöxo', 'mueble', 'Mediano', 'Estridente entelequia de la mayor calidad'),
(77, 'Höza', 'herramienta', 'Pequeño', 'Eficaz entelequia de primera necesidad'),
(78, 'Fëoäkë', 'herramienta', 'Mediano', 'Útil cosa para cualquiera'),
(79, 'Se-oudö', 'decoración de mesa', 'Grande', 'Imprescindible cosa de una calidad mediocre'),
(80, 'Söl-uujëbes', 'decoración de mesa', 'Grande', 'Imprescindible cacharro para sacarte de un aprieto'),
(81, 'Aämöxeji-ya', 'decoración de pared', 'Pequeño', 'Adorableentelequia de una calidad mediocre'),
(82, 'Iëuu', 'mueble', 'Pequeño', 'Útil invención de la mayor calidad'),
(83, 'Wëo-qirpa', 'decoración de pared', 'Pequeño', 'Útil cacharro para sacarte de un aprieto'),
(84, 'Qi-eë', 'decoración de mesa', 'Grande', 'Eficaz entelequia de la mayor calidad'),
(85, 'Ea-wasij', 'mueble', 'Pequeño', 'Imprescindible entelequia de primera necesidad'),
(86, 'Vojilu', 'herramienta', 'Mediano', 'Eficaz cacharro para cualquiera'),
(87, 'Fiwöbe', 'mueble', 'Grande', 'Estridente entelequia para cualquiera'),
(88, 'Së-je', 'herramienta', 'Grande', 'Eficaz cosa de buena relación calidad precio'),
(89, 'Kora-da', 'mueble', 'Grande', 'Estridente cosa para cualquiera'),
(90, 'Yoqeë-hucu', 'mueble', 'Mediano', 'Imprescindible objeto para cualquiera'),
(91, 'Xöka', 'mueble', 'Pequeño', 'Estridente cosa de primera necesidad'),
(92, 'Xiyejazötä', 'decoración de mesa', 'Mediano', 'Útil objeto para el día a día'),
(93, 'Yö-bice', 'decoración de mesa', 'Grande', 'Imprescindible cacharro de una calidad mediocre'),
(94, 'Maoaae', 'mueble', 'Mediano', 'Adorablecacharro de una calidad mediocre'),
(95, 'Fitu', 'herramienta', 'Mediano', 'Útil cacharro de una calidad mediocre'),
(96, 'Bina', 'mueble', 'Grande', 'Útil entelequia para el día a día'),
(97, 'Mo-haaov-uulorj', 'herramienta', 'Mediano', 'Imprescindible invención de buena relación calidad precio'),
(98, 'Sëd-si', 'decoración de pared', 'Pequeño', 'Útil objeto de la mayor calidad'),
(99, 'Looë', 'decoración de pared', 'Grande', 'Estridente cacharro de la mayor calidad'),
(100, 'Sojökodimo', 'mueble', 'Mediano', 'Imprescindible objeto para cualquiera'),
(101, 'Debdö', 'mueble', 'Grande', 'Útil invención para cualquiera'),
(102, 'Iomdë', 'herramienta', 'Pequeño', 'Adorablecacharro de primera necesidad'),
(103, 'Zëdëbë', 'decoración de mesa', 'Mediano', 'Imprescindible entelequia de buena relación calidad precio'),
(104, 'Jëro', 'mueble', 'Mediano', 'Imprescindible entelequia para los más exigentes'),
(105, 'Eer-gu', 'mueble', 'Grande', 'Útil invención de buena relación calidad precio'),
(106, 'Nëcwëaohorj', 'herramienta', 'Mediano', 'Imprescindible entelequia para el día a día'),
(107, 'Jo-uö', 'mueble', 'Pequeño', 'Estridente cacharro para sacarte de un aprieto'),
(108, 'Qähë', 'decoración de pared', 'Mediano', 'Imprescindible cacharro de buena relación calidad precio'),
(109, 'Rë-ii', 'herramienta', 'Mediano', 'Eficaz objeto de buena relación calidad precio'),
(110, 'Uu-nu', 'decoración de mesa', 'Pequeño', 'Adorableobjeto de la mayor calidad'),
(111, 'Uini', 'herramienta', 'Mediano', 'Estridente invención de primera necesidad'),
(112, 'Röou', 'decoración de pared', 'Grande', 'Estridente objeto de una calidad mediocre'),
(113, 'Vëk-në', 'herramienta', 'Pequeño', 'Eficaz objeto para los más exigentes'),
(114, 'Qö-ua', 'herramienta', 'Grande', 'Estridente cacharro de buena relación calidad precio'),
(115, 'Qobö', 'mueble', 'Pequeño', 'Imprescindible entelequia para el día a día'),
(116, 'Yeqiuöhorj', 'herramienta', 'Grande', 'Útil objeto de la mayor calidad'),
(117, 'Sel-boifa', 'mueble', 'Pequeño', 'Imprescindible invención de primera necesidad'),
(118, 'Mäzë', 'mueble', 'Pequeño', 'Estridente objeto para los más exigentes'),
(119, 'Nu-leso-xofj', 'decoración de mesa', 'Mediano', 'Estridente entelequia de una calidad mediocre'),
(120, 'Eëcö', 'decoración de mesa', 'Pequeño', 'Estridente invención para los más exigentes'),
(121, 'Weaiä', 'herramienta', 'Grande', 'Adorableobjeto para el día a día'),
(122, 'Fenecë', 'mueble', 'Grande', 'Estridente objeto de buena relación calidad precio'),
(123, 'Oör-iö', 'decoración de mesa', 'Mediano', 'Útil cosa para cualquiera'),
(124, 'Eëji', 'mueble', 'Grande', 'Estridente invención para cualquiera'),
(125, 'Quxe', 'herramienta', 'Grande', 'Adorableinvención de buena relación calidad precio'),
(126, 'Eua-däj', 'decoración de mesa', 'Grande', 'Eficaz entelequia de una calidad mediocre'),
(127, 'Väsä', 'decoración de pared', 'Mediano', 'Imprescindible objeto para los más exigentes'),
(128, 'Leadäwë', 'herramienta', 'Pequeño', 'Útil cacharro para sacarte de un aprieto'),
(129, 'Tiw-qooöm', 'decoración de mesa', 'Mediano', 'Útil cosa para cualquiera'),
(130, 'Bäwö', 'herramienta', 'Pequeño', 'Adorableentelequia para los más exigentes'),
(131, 'Töjiuäz', 'mueble', 'Grande', 'Eficaz objeto para sacarte de un aprieto'),
(132, 'Dëlë', 'decoración de mesa', 'Mediano', 'Útil entelequia para los más exigentes'),
(133, 'Xihao', 'decoración de mesa', 'Grande', 'Eficaz entelequia para cualquiera'),
(134, 'Uëuemdö', 'mueble', 'Pequeño', 'Adorablecosa de la mayor calidad'),
(135, 'Iepusë', 'decoración de mesa', 'Grande', 'Estridente cacharro para el día a día'),
(136, 'Qö-pö', 'herramienta', 'Mediano', 'Útil cacharro para el día a día'),
(137, 'Yukie', 'herramienta', 'Pequeño', 'Imprescindible cosa de la mayor calidad'),
(138, 'Yowuzën', 'decoración de mesa', 'Pequeño', 'Estridente cosa para los más exigentes'),
(139, 'Xacafucöeövj', 'mueble', 'Mediano', 'Estridente invención para el día a día'),
(140, 'Ei-jës', 'mueble', 'Grande', 'Adorableinvención para sacarte de un aprieto'),
(141, 'Bëfö', 'herramienta', 'Pequeño', 'Adorableobjeto para sacarte de un aprieto'),
(142, 'Vödo-yä', 'decoración de pared', 'Grande', 'Útil invención para los más exigentes'),
(143, 'Lurkegu', 'mueble', 'Pequeño', 'Útil cosa de primera necesidad'),
(144, 'Iuzi', 'herramienta', 'Mediano', 'Útil entelequia para sacarte de un aprieto'),
(145, 'Hiwä', 'decoración de mesa', 'Grande', 'Estridente objeto de buena relación calidad precio'),
(146, 'Pacëkuc', 'herramienta', 'Pequeño', 'Eficaz objeto para cualquiera'),
(147, 'Eöysa', 'herramienta', 'Mediano', 'Útil cacharro de primera necesidad'),
(148, 'Eöbiaifën', 'decoración de pared', 'Pequeño', 'Eficaz invención de la mayor calidad'),
(149, 'Pëk-hi', 'mueble', 'Mediano', 'Útil invención de una calidad mediocre'),
(150, 'Iäläh', 'mueble', 'Pequeño', 'Útil entelequia de primera necesidad'),
(151, 'Uudqufuwösi-föyorj', 'herramienta', 'Pequeño', 'Estridente cacharro de una calidad mediocre'),
(152, 'Sëvazomuuoö', 'herramienta', 'Grande', 'Estridente cacharro de primera necesidad'),
(153, 'Oicop', 'mueble', 'Mediano', 'Eficaz invención de la mayor calidad'),
(154, 'Piiwi', 'mueble', 'Mediano', 'Estridente cosa para el día a día'),
(155, 'Kämwö', 'decoración de pared', 'Grande', 'Útil invención para sacarte de un aprieto'),
(156, 'Pe-jo', 'mueble', 'Mediano', 'Imprescindible entelequia de primera necesidad'),
(157, 'Gu-supo', 'decoración de mesa', 'Pequeño', 'Eficaz invención para el día a día'),
(158, 'Iuca', 'mueble', 'Pequeño', 'Útil cacharro de la mayor calidad'),
(159, 'Këmwose', 'decoración de mesa', 'Pequeño', 'Estridente cosa para cualquiera'),
(160, 'Yib-oi', 'herramienta', 'Pequeño', 'Imprescindible invención para cualquiera'),
(161, 'Väzöeä', 'mueble', 'Mediano', 'Adorablecacharro de buena relación calidad precio'),
(162, 'Cëwi', 'decoración de mesa', 'Grande', 'Eficaz cosa de buena relación calidad precio'),
(163, 'Huw-so', 'mueble', 'Mediano', 'Eficaz invención de primera necesidad'),
(164, 'Carue', 'mueble', 'Pequeño', 'Útil cosa de la mayor calidad'),
(165, 'Xo-paauynö', 'mueble', 'Mediano', 'Imprescindible objeto para cualquiera'),
(166, 'Qëc-që', 'decoración de pared', 'Grande', 'Útil cosa de una calidad mediocre'),
(167, 'Xëxuxa', 'mueble', 'Mediano', 'Útil entelequia de la mayor calidad'),
(168, 'Zinua', 'decoración de mesa', 'Pequeño', 'Útil cacharro para cualquiera'),
(169, 'Qäkë', 'decoración de pared', 'Grande', 'Eficaz invención de la mayor calidad'),
(170, 'Dë-wö', 'decoración de mesa', 'Pequeño', 'Útil cosa de una calidad mediocre'),
(171, 'Suvdunö', 'herramienta', 'Pequeño', 'Adorableinvención para los más exigentes'),
(172, 'Cokmi', 'herramienta', 'Pequeño', 'Imprescindible invención para el día a día'),
(173, 'Ia-ji', 'mueble', 'Mediano', 'Eficaz objeto de buena relación calidad precio'),
(174, 'Se-bäy', 'mueble', 'Pequeño', 'Útil entelequia de primera necesidad'),
(175, 'Cäkcë', 'herramienta', 'Mediano', 'Estridente cosa de buena relación calidad precio'),
(176, 'Këgoyo', 'mueble', 'Grande', 'Imprescindible objeto de primera necesidad'),
(177, 'Zäoe', 'mueble', 'Grande', 'Útil invención para el día a día'),
(178, 'Më-la', 'herramienta', 'Grande', 'Adorablecosa para los más exigentes'),
(179, 'Lum-säj', 'herramienta', 'Mediano', 'Eficaz cacharro de una calidad mediocre'),
(180, 'Uëgëxo', 'mueble', 'Pequeño', 'Imprescindible objeto para sacarte de un aprieto'),
(181, 'Rejösä', 'herramienta', 'Mediano', 'Adorablecacharro de primera necesidad'),
(182, 'Suwli', 'herramienta', 'Pequeño', 'Eficaz objeto para los más exigentes'),
(183, 'Suyib-bu', 'herramienta', 'Mediano', 'Estridente objeto para cualquiera'),
(184, 'De-oö', 'mueble', 'Mediano', 'Estridente cosa para sacarte de un aprieto'),
(185, 'Augta', 'herramienta', 'Pequeño', 'Adorableinvención para cualquiera'),
(186, 'Aa-si', 'decoración de mesa', 'Grande', 'Estridente objeto de buena relación calidad precio'),
(187, 'Weso', 'mueble', 'Mediano', 'Estridente entelequia de la mayor calidad'),
(188, 'Hekxä', 'mueble', 'Mediano', 'Eficaz objeto para cualquiera'),
(189, 'Gojäya', 'herramienta', 'Mediano', 'Estridente invención de la mayor calidad'),
(190, 'Kilä', 'mueble', 'Grande', 'Estridente cosa para el día a día'),
(191, 'Dimu', 'mueble', 'Mediano', 'Útil cosa de buena relación calidad precio'),
(192, 'Dai-xur', 'herramienta', 'Grande', 'Útil entelequia para sacarte de un aprieto'),
(193, 'Lëgihë', 'herramienta', 'Pequeño', 'Imprescindible cosa de una calidad mediocre'),
(194, 'La-xëuëoaj', 'mueble', 'Grande', 'Imprescindible entelequia de una calidad mediocre'),
(195, 'Göza', 'mueble', 'Pequeño', 'Eficaz entelequia de una calidad mediocre'),
(196, 'Newi', 'decoración de mesa', 'Grande', 'Eficaz cacharro de primera necesidad'),
(197, 'Ueu-mojöj', 'decoración de mesa', 'Pequeño', 'Eficaz invención de buena relación calidad precio'),
(198, 'Tusetä', 'herramienta', 'Mediano', 'Estridente invención para cualquiera'),
(199, 'Gë-mi', 'decoración de pared', 'Mediano', 'Útil entelequia para el día a día'),
(200, 'Ea-lutu', 'herramienta', 'Pequeño', 'Imprescindible invención para el día a día'),
(201, 'Do-dudevö', 'decoración de mesa', 'Grande', 'Eficaz entelequia de una calidad mediocre'),
(202, 'Dë-xäwow', 'mueble', 'Mediano', 'Estridente entelequia para cualquiera'),
(203, 'Meoä', 'mueble', 'Pequeño', 'Estridente objeto para cualquiera'),
(204, 'Uö-aö', 'herramienta', 'Pequeño', 'Eficaz cosa de primera necesidad'),
(205, 'Ue-päzimë-göa', 'decoración de mesa', 'Pequeño', 'Adorableentelequia de buena relación calidad precio'),
(206, 'Donona', 'mueble', 'Grande', 'Adorableinvención para los más exigentes'),
(207, 'Bäotö', 'decoración de mesa', 'Pequeño', 'Eficaz cacharro para el día a día'),
(208, 'Iaoo', 'herramienta', 'Grande', 'Útil objeto de buena relación calidad precio'),
(209, 'Lu-näla', 'decoración de pared', 'Mediano', 'Eficaz objeto para el día a día'),
(210, 'Dene', 'decoración de mesa', 'Grande', 'Útil invención de la mayor calidad'),
(211, 'Wöalodögi', 'herramienta', 'Pequeño', 'Eficaz cacharro de primera necesidad'),
(212, 'Aemövah', 'herramienta', 'Grande', 'Imprescindible cacharro de buena relación calidad precio'),
(213, 'Të-oe', 'decoración de pared', 'Mediano', 'Adorablecacharro de la mayor calidad'),
(214, 'Auro', 'decoración de mesa', 'Grande', 'Estridente objeto para cualquiera'),
(215, 'Xövo', 'mueble', 'Grande', 'Estridente cosa de primera necesidad'),
(216, 'Yä-särä', 'mueble', 'Pequeño', 'Adorableinvención de primera necesidad'),
(217, 'Feolö', 'herramienta', 'Pequeño', 'Útil cosa para cualquiera'),
(218, 'Ia-së', 'herramienta', 'Grande', 'Adorablecosa de primera necesidad'),
(219, 'Dävoope', 'mueble', 'Grande', 'Eficaz cosa para sacarte de un aprieto'),
(220, 'Jekuëj', 'herramienta', 'Pequeño', 'Útil cosa de la mayor calidad'),
(221, 'Je-vöti', 'decoración de mesa', 'Mediano', 'Imprescindible cosa de primera necesidad'),
(222, 'Nerö', 'mueble', 'Grande', 'Eficaz entelequia para los más exigentes'),
(223, 'Uëaepij', 'herramienta', 'Mediano', 'Útil invención para sacarte de un aprieto'),
(224, 'Zäaeläni', 'mueble', 'Pequeño', 'Estridente cosa para sacarte de un aprieto'),
(225, 'Ga-wömöz', 'decoración de mesa', 'Pequeño', 'Adorableentelequia para el día a día'),
(226, 'Qäii', 'mueble', 'Pequeño', 'Estridente entelequia de buena relación calidad precio'),
(227, 'Cä-doböborj', 'herramienta', 'Mediano', 'Imprescindible entelequia de buena relación calidad precio'),
(228, 'Möluj', 'mueble', 'Grande', 'Adorablecosa de buena relación calidad precio'),
(229, 'Xäpe', 'mueble', 'Pequeño', 'Adorablecacharro para el día a día'),
(230, 'Sifxo-eöranorj', 'decoración de mesa', 'Mediano', 'Estridente cosa de primera necesidad'),
(231, 'Yu-ye', 'herramienta', 'Pequeño', 'Estridente objeto para los más exigentes'),
(232, 'Tövejoj', 'mueble', 'Mediano', 'Adorableinvención de primera necesidad'),
(233, 'Ti-ri', 'mueble', 'Mediano', 'Útil cacharro para el día a día'),
(234, 'Yifqë', 'herramienta', 'Pequeño', 'Imprescindible invención para cualquiera'),
(235, 'Ee-hö', 'herramienta', 'Mediano', 'Estridente entelequia para el día a día'),
(236, 'Röuize', 'decoración de pared', 'Grande', 'Estridente objeto de una calidad mediocre'),
(237, 'Iärocu', 'herramienta', 'Grande', 'Adorableinvención de buena relación calidad precio'),
(238, 'Uocälo', 'herramienta', 'Grande', 'Adorableentelequia para cualquiera'),
(239, 'Mij-to', 'herramienta', 'Mediano', 'Imprescindible cosa para cualquiera'),
(240, 'Siao', 'herramienta', 'Grande', 'Eficaz invención de una calidad mediocre'),
(241, 'Yöaa', 'mueble', 'Grande', 'Útil invención de la mayor calidad'),
(242, 'Quttay', 'mueble', 'Grande', 'Eficaz cacharro para sacarte de un aprieto'),
(243, 'Säwdö', 'decoración de mesa', 'Mediano', 'Adorablecacharro de la mayor calidad'),
(244, 'Oaou', 'mueble', 'Mediano', 'Adorableinvención para el día a día'),
(245, 'Yetzah', 'decoración de mesa', 'Mediano', 'Imprescindible invención para el día a día'),
(246, 'Pocaoafalorj', 'decoración de mesa', 'Grande', 'Imprescindible objeto de una calidad mediocre'),
(247, 'Aëeöhqocuyj', 'mueble', 'Pequeño', 'Adorableentelequia para los más exigentes'),
(248, 'Iafövej', 'decoración de pared', 'Grande', 'Eficaz entelequia de primera necesidad'),
(249, 'Eu-re', 'herramienta', 'Mediano', 'Útil objeto de primera necesidad'),
(250, 'Mëxzehäj', 'herramienta', 'Mediano', 'Imprescindible cacharro para el día a día'),
(251, 'Rëesëdor', 'herramienta', 'Grande', 'Eficaz objeto de la mayor calidad'),
(252, 'Vasä', 'decoración de pared', 'Mediano', 'Adorableobjeto para cualquiera'),
(253, 'Cemuj', 'herramienta', 'Grande', 'Imprescindible cosa para los más exigentes'),
(254, 'Näju', 'herramienta', 'Mediano', 'Adorablecacharro de la mayor calidad'),
(255, 'Jäjä', 'herramienta', 'Mediano', 'Estridente invención de primera necesidad'),
(256, 'Höo-na', 'mueble', 'Mediano', 'Imprescindible entelequia para sacarte de un aprieto'),
(257, 'Fumu', 'decoración de mesa', 'Pequeño', 'Eficaz cosa de buena relación calidad precio'),
(258, 'Yogahäfo', 'herramienta', 'Grande', 'Útil cosa para cualquiera'),
(259, 'Pöaehëm', 'decoración de mesa', 'Mediano', 'Útil invención para sacarte de un aprieto'),
(260, 'Me-nacuorj', 'mueble', 'Grande', 'Eficaz entelequia para los más exigentes'),
(261, 'Lö-wö', 'mueble', 'Pequeño', 'Estridente invención de la mayor calidad'),
(262, 'Pöce', 'decoración de mesa', 'Mediano', 'Imprescindible invención para los más exigentes'),
(263, 'Homëfe', 'herramienta', 'Pequeño', 'Eficaz entelequia de buena relación calidad precio'),
(264, 'Kogöfi', 'mueble', 'Mediano', 'Estridente cosa de primera necesidad'),
(265, 'Casxö', 'herramienta', 'Mediano', 'Adorableinvención de una calidad mediocre'),
(266, 'Weea', 'herramienta', 'Mediano', 'Estridente cacharro de la mayor calidad'),
(267, 'Zeeo', 'herramienta', 'Grande', 'Eficaz invención para el día a día'),
(268, 'Zëyolö', 'herramienta', 'Pequeño', 'Eficaz objeto para los más exigentes'),
(269, 'Lëväräorj', 'herramienta', 'Mediano', 'Eficaz objeto de primera necesidad'),
(270, 'Cös-fözven', 'herramienta', 'Grande', 'Imprescindible cacharro de buena relación calidad precio'),
(271, 'Cem-seqwöc', 'decoración de mesa', 'Mediano', 'Estridente invención para el día a día'),
(272, 'Yulu', 'herramienta', 'Mediano', 'Adorablecacharro para el día a día'),
(273, 'Jaqwu', 'mueble', 'Mediano', 'Imprescindible cacharro de una calidad mediocre'),
(274, 'Fëz-bayozaorj', 'decoración de mesa', 'Mediano', 'Útil objeto de una calidad mediocre'),
(275, 'Ri-oo', 'herramienta', 'Mediano', 'Estridente cosa para el día a día'),
(276, 'Bö-ui', 'mueble', 'Pequeño', 'Estridente entelequia de la mayor calidad'),
(277, 'Buu-bacähusorj', 'herramienta', 'Pequeño', 'Estridente entelequia de la mayor calidad'),
(278, 'Nalau', 'herramienta', 'Pequeño', 'Eficaz entelequia para sacarte de un aprieto'),
(279, 'Zä-ia', 'herramienta', 'Mediano', 'Adorableentelequia de primera necesidad'),
(280, 'Yëtö', 'mueble', 'Grande', 'Estridente entelequia de buena relación calidad precio'),
(281, 'Dörotövue', 'mueble', 'Pequeño', 'Imprescindible entelequia de buena relación calidad precio'),
(282, 'Mafu', 'decoración de mesa', 'Pequeño', 'Eficaz cosa de una calidad mediocre'),
(283, 'Ligë', 'mueble', 'Grande', 'Adorablecosa para sacarte de un aprieto'),
(284, 'Wäwo', 'decoración de mesa', 'Mediano', 'Adorablecosa para sacarte de un aprieto'),
(285, 'Uëjö', 'herramienta', 'Mediano', 'Adorableinvención de la mayor calidad'),
(286, 'Kowedo', 'mueble', 'Pequeño', 'Útil entelequia de una calidad mediocre'),
(287, 'Pidä', 'decoración de pared', 'Grande', 'Adorableentelequia para el día a día'),
(288, 'Uopaauhij', 'mueble', 'Mediano', 'Estridente objeto de la mayor calidad'),
(289, 'Uäfu', 'mueble', 'Mediano', 'Útil entelequia para sacarte de un aprieto'),
(290, 'Jö-eël', 'decoración de mesa', 'Pequeño', 'Eficaz invención de la mayor calidad'),
(291, 'Buyë', 'mueble', 'Pequeño', 'Útil cacharro de primera necesidad'),
(292, 'Pije', 'mueble', 'Mediano', 'Imprescindible invención para sacarte de un aprieto'),
(293, 'Mac-eëcsë', 'decoración de pared', 'Mediano', 'Adorableinvención para cualquiera'),
(294, 'Uoisi', 'herramienta', 'Grande', 'Útil cosa para cualquiera'),
(295, 'Voaë', 'herramienta', 'Pequeño', 'Eficaz invención de primera necesidad'),
(296, 'Keie', 'herramienta', 'Grande', 'Adorablecacharro para el día a día'),
(297, 'Yuaax', 'decoración de mesa', 'Pequeño', 'Adorablecosa de una calidad mediocre'),
(298, 'Höb-nogö', 'mueble', 'Pequeño', 'Adorableentelequia para cualquiera'),
(299, 'Wi-he', 'decoración de mesa', 'Mediano', 'Eficaz cosa para sacarte de un aprieto'),
(300, 'Ciqazië', 'herramienta', 'Grande', 'Imprescindible entelequia de buena relación calidad precio'),
(301, 'Bilmaboqae', 'decoración de pared', 'Pequeño', 'Adorableentelequia para cualquiera'),
(302, 'Uäqu', 'decoración de mesa', 'Pequeño', 'Útil invención de buena relación calidad precio'),
(303, 'Dina', 'herramienta', 'Mediano', 'Estridente cacharro de una calidad mediocre'),
(304, 'Xifi', 'decoración de mesa', 'Grande', 'Estridente objeto de buena relación calidad precio'),
(305, 'Tö-heeihouga', 'decoración de mesa', 'Mediano', 'Adorablecosa de la mayor calidad'),
(306, 'Göj-eëw', 'herramienta', 'Grande', 'Adorablecacharro para el día a día'),
(307, 'Väibe', 'decoración de mesa', 'Pequeño', 'Útil entelequia para sacarte de un aprieto'),
(308, 'Aicëq', 'mueble', 'Pequeño', 'Eficaz invención para los más exigentes'),
(309, 'Höt-ca-bi', 'mueble', 'Pequeño', 'Imprescindible invención de una calidad mediocre'),
(310, 'Movë', 'decoración de pared', 'Mediano', 'Eficaz cosa de la mayor calidad'),
(311, 'Hufä', 'herramienta', 'Pequeño', 'Útil invención para cualquiera'),
(312, 'Tä-sevinu', 'herramienta', 'Pequeño', 'Eficaz cosa de una calidad mediocre'),
(313, 'Goaevubä', 'herramienta', 'Grande', 'Eficaz entelequia de buena relación calidad precio'),
(314, 'Muve', 'decoración de mesa', 'Mediano', 'Eficaz invención de primera necesidad'),
(315, 'Qä-nä', 'herramienta', 'Pequeño', 'Eficaz invención para el día a día'),
(316, 'Xuaha', 'herramienta', 'Grande', 'Imprescindible cosa para cualquiera'),
(317, 'Kez-xabicoäx', 'decoración de mesa', 'Grande', 'Adorablecosa para los más exigentes'),
(318, 'Zi-jä', 'decoración de mesa', 'Grande', 'Imprescindible cacharro para sacarte de un aprieto'),
(319, 'Siseyi-fäaj', 'decoración de pared', 'Mediano', 'Estridente invención para los más exigentes'),
(320, 'Luyneoe', 'herramienta', 'Grande', 'Útil cacharro para cualquiera'),
(321, 'Tojuqige', 'mueble', 'Mediano', 'Eficaz cacharro de buena relación calidad precio'),
(322, 'Civuia', 'decoración de mesa', 'Grande', 'Estridente objeto de buena relación calidad precio'),
(323, 'Iädë-ka', 'herramienta', 'Mediano', 'Eficaz cosa para el día a día'),
(324, 'Eioaoj', 'decoración de mesa', 'Mediano', 'Adorablecosa de una calidad mediocre'),
(325, 'Gon-qufëp', 'decoración de mesa', 'Mediano', 'Útil invención para el día a día'),
(326, 'Xëvedi', 'decoración de mesa', 'Mediano', 'Adorableobjeto para el día a día'),
(327, 'Uöqäjmo', 'mueble', 'Grande', 'Adorableobjeto de la mayor calidad'),
(328, 'Täz-ci', 'decoración de mesa', 'Mediano', 'Eficaz cosa para el día a día'),
(329, 'Pövë', 'herramienta', 'Pequeño', 'Estridente objeto para el día a día'),
(330, 'Dë-pëm', 'herramienta', 'Mediano', 'Adorablecosa para sacarte de un aprieto'),
(331, 'Oäcenö', 'herramienta', 'Mediano', 'Adorablecacharro para los más exigentes'),
(332, 'Fex-ku', 'decoración de mesa', 'Grande', 'Imprescindible invención de la mayor calidad'),
(333, 'Xäaö', 'mueble', 'Pequeño', 'Estridente cosa de una calidad mediocre'),
(334, 'Pë-uu', 'mueble', 'Grande', 'Eficaz objeto de una calidad mediocre'),
(335, 'Lug-ba-cä', 'decoración de mesa', 'Mediano', 'Estridente cosa para sacarte de un aprieto'),
(336, 'Wërobëoëheb', 'herramienta', 'Mediano', 'Adorableinvención para los más exigentes'),
(337, 'Xetim', 'decoración de mesa', 'Mediano', 'Adorablecacharro para el día a día'),
(338, 'Fätu', 'herramienta', 'Grande', 'Útil cosa para cualquiera'),
(339, 'Aëtoji', 'herramienta', 'Grande', 'Estridente entelequia para cualquiera'),
(340, 'Hälo', 'herramienta', 'Mediano', 'Eficaz cacharro para sacarte de un aprieto'),
(341, 'Re-buru', 'herramienta', 'Pequeño', 'Estridente objeto de una calidad mediocre'),
(342, 'Tire', 'decoración de mesa', 'Pequeño', 'Imprescindible cosa para el día a día'),
(343, 'Aezëfoeorj', 'mueble', 'Pequeño', 'Adorableobjeto para los más exigentes'),
(344, 'Fa-do', 'herramienta', 'Grande', 'Estridente entelequia de buena relación calidad precio'),
(345, 'Na-quxöcëaj', 'herramienta', 'Mediano', 'Eficaz cosa de buena relación calidad precio'),
(346, 'Oägise', 'decoración de pared', 'Pequeño', 'Estridente cosa de la mayor calidad'),
(347, 'Vabe', 'mueble', 'Grande', 'Eficaz invención para sacarte de un aprieto'),
(348, 'Kede', 'mueble', 'Mediano', 'Eficaz objeto de buena relación calidad precio'),
(349, 'Zopena', 'herramienta', 'Grande', 'Adorablecacharro de una calidad mediocre'),
(350, 'Quoö', 'herramienta', 'Grande', 'Útil cacharro para cualquiera'),
(351, 'Wuvë', 'decoración de mesa', 'Pequeño', 'Útil entelequia de buena relación calidad precio'),
(352, 'Yuxdë', 'herramienta', 'Pequeño', 'Útil objeto de primera necesidad'),
(353, 'Cäaä', 'herramienta', 'Mediano', 'Eficaz cosa para sacarte de un aprieto'),
(354, 'Uo-jöorj', 'herramienta', 'Mediano', 'Eficaz cosa de primera necesidad'),
(355, 'Bäpe', 'decoración de pared', 'Pequeño', 'Útil cosa para los más exigentes'),
(356, 'Oebdioo', 'mueble', 'Mediano', 'Estridente invención de primera necesidad'),
(357, 'Ton-gire', 'herramienta', 'Pequeño', 'Útil cosa de la mayor calidad'),
(358, 'Fo-pä', 'decoración de mesa', 'Mediano', 'Útil entelequia para cualquiera'),
(359, 'Jola', 'herramienta', 'Mediano', 'Estridente cacharro de la mayor calidad'),
(360, 'Mäbfë', 'mueble', 'Grande', 'Adorableinvención de la mayor calidad'),
(361, 'Rizoeyorävorj', 'decoración de pared', 'Pequeño', 'Imprescindible cacharro de una calidad mediocre'),
(362, 'Feovebeciiö', 'herramienta', 'Grande', 'Imprescindible invención de buena relación calidad precio'),
(363, 'Hëue', 'decoración de mesa', 'Mediano', 'Estridente cacharro de buena relación calidad precio'),
(364, 'Uäfxo', 'mueble', 'Pequeño', 'Eficaz cosa para los más exigentes'),
(365, 'Cöctä', 'herramienta', 'Mediano', 'Adorableobjeto de una calidad mediocre'),
(366, 'Gu-fikë', 'mueble', 'Grande', 'Útil cacharro de una calidad mediocre'),
(367, 'Zuyëlör', 'decoración de pared', 'Pequeño', 'Imprescindible cacharro para los más exigentes'),
(368, 'Uako', 'herramienta', 'Pequeño', 'Imprescindible invención de primera necesidad'),
(369, 'Patip', 'herramienta', 'Mediano', 'Útil objeto para sacarte de un aprieto'),
(370, 'Cänri', 'mueble', 'Mediano', 'Imprescindible entelequia de una calidad mediocre'),
(371, 'Hilo', 'decoración de mesa', 'Mediano', 'Eficaz cacharro para los más exigentes'),
(372, 'Cekori', 'decoración de pared', 'Pequeño', 'Estridente entelequia para cualquiera'),
(373, 'Bof-kä', 'decoración de mesa', 'Mediano', 'Estridente cacharro para cualquiera'),
(374, 'Päjtu', 'herramienta', 'Pequeño', 'Estridente cosa para cualquiera'),
(375, 'Taoda', 'mueble', 'Mediano', 'Adorableentelequia para los más exigentes'),
(376, 'Uëfihow', 'herramienta', 'Mediano', 'Útil entelequia de la mayor calidad'),
(377, 'Wöcidö', 'decoración de mesa', 'Pequeño', 'Estridente objeto de la mayor calidad'),
(378, 'Qöyë', 'herramienta', 'Mediano', 'Eficaz cosa para sacarte de un aprieto'),
(379, 'Ruoäfemorj', 'herramienta', 'Mediano', 'Adorablecosa de la mayor calidad'),
(380, 'Kimi', 'herramienta', 'Pequeño', 'Estridente entelequia para los más exigentes'),
(381, 'Yëeui', 'mueble', 'Mediano', 'Estridente cacharro de la mayor calidad'),
(382, 'Jëeeye', 'mueble', 'Mediano', 'Imprescindible invención para sacarte de un aprieto'),
(383, 'Eukäiöjj', 'decoración de pared', 'Grande', 'Adorablecacharro para el día a día'),
(384, 'Uud-räba', 'herramienta', 'Pequeño', 'Eficaz cosa de la mayor calidad'),
(385, 'Do-xä', 'herramienta', 'Grande', 'Estridente objeto para sacarte de un aprieto'),
(386, 'Eesöquce', 'herramienta', 'Mediano', 'Adorablecacharro de primera necesidad'),
(387, 'Que-hë', 'herramienta', 'Pequeño', 'Imprescindible cosa para sacarte de un aprieto'),
(388, 'Vëridë', 'decoración de pared', 'Grande', 'Estridente cosa para los más exigentes'),
(389, 'Bë-pä', 'decoración de pared', 'Pequeño', 'Imprescindible cacharro de una calidad mediocre'),
(390, 'Zëiiuno', 'decoración de mesa', 'Mediano', 'Estridente objeto de buena relación calidad precio'),
(391, 'Yaiu', 'mueble', 'Mediano', 'Útil cosa para el día a día'),
(392, 'Ua-ne', 'mueble', 'Grande', 'Imprescindible cacharro para cualquiera'),
(393, 'Qiz-oö', 'herramienta', 'Pequeño', 'Estridente cosa de buena relación calidad precio'),
(394, 'Zeo-eo', 'herramienta', 'Mediano', 'Adorableobjeto para los más exigentes'),
(395, 'Eoxacö', 'decoración de mesa', 'Pequeño', 'Útil objeto para el día a día'),
(396, 'Fiwgo', 'herramienta', 'Pequeño', 'Adorablecosa de la mayor calidad'),
(397, 'Yäaëhö', 'herramienta', 'Pequeño', 'Imprescindible entelequia de una calidad mediocre'),
(398, 'Haj-xefä', 'herramienta', 'Pequeño', 'Imprescindible objeto para los más exigentes'),
(399, 'Zuae', 'herramienta', 'Pequeño', 'Adorablecacharro de primera necesidad'),
(400, 'Xufuä', 'decoración de mesa', 'Grande', 'Eficaz invención para el día a día'),
(401, 'Mo-xe', 'herramienta', 'Pequeño', 'Imprescindible objeto para cualquiera'),
(402, 'Pozapëi', 'mueble', 'Grande', 'Adorablecacharro para los más exigentes'),
(403, 'Aaiefa', 'decoración de mesa', 'Mediano', 'Eficaz cosa de primera necesidad'),
(404, 'Aofiäea', 'herramienta', 'Pequeño', 'Imprescindible invención para el día a día'),
(405, 'Bez-ao', 'decoración de mesa', 'Pequeño', 'Útil invención de buena relación calidad precio'),
(406, 'Iu-aoebä', 'herramienta', 'Pequeño', 'Adorableentelequia para cualquiera'),
(407, 'Nilövi-fi', 'decoración de mesa', 'Mediano', 'Adorablecosa de una calidad mediocre'),
(408, 'Cëcuafub', 'mueble', 'Mediano', 'Imprescindible cacharro de primera necesidad'),
(409, 'Nofrëke', 'herramienta', 'Mediano', 'Útil entelequia para el día a día'),
(410, 'Në-xe', 'herramienta', 'Grande', 'Eficaz entelequia para cualquiera'),
(411, 'Eöviiaiorj', 'herramienta', 'Pequeño', 'Útil cacharro de primera necesidad'),
(412, 'Hö-qupej', 'herramienta', 'Grande', 'Estridente cacharro de la mayor calidad'),
(413, 'Aëltuba', 'herramienta', 'Mediano', 'Estridente entelequia de la mayor calidad'),
(414, 'Eör-ii', 'mueble', 'Grande', 'Adorableentelequia de una calidad mediocre'),
(415, 'Xäkë', 'herramienta', 'Grande', 'Imprescindible cacharro para cualquiera'),
(416, 'Hazäfö-oo', 'decoración de pared', 'Mediano', 'Estridente invención para sacarte de un aprieto'),
(417, 'Famaj', 'decoración de mesa', 'Mediano', 'Estridente entelequia de la mayor calidad'),
(418, 'Cöz-iöyeu', 'decoración de pared', 'Grande', 'Útil entelequia para los más exigentes'),
(419, 'Yepu', 'decoración de pared', 'Mediano', 'Útil cacharro para cualquiera'),
(420, 'Aedä', 'mueble', 'Pequeño', 'Adorableinvención para cualquiera'),
(421, 'Tuydä', 'decoración de pared', 'Grande', 'Adorablecacharro para el día a día'),
(422, 'Hoiuoä', 'mueble', 'Pequeño', 'Estridente objeto de la mayor calidad'),
(423, 'Xögoxo', 'mueble', 'Grande', 'Eficaz entelequia para sacarte de un aprieto'),
(424, 'Tod-ru', 'decoración de pared', 'Pequeño', 'Imprescindible objeto de la mayor calidad'),
(425, 'Raxë', 'herramienta', 'Mediano', 'Eficaz entelequia para sacarte de un aprieto'),
(426, 'Nönalue', 'decoración de mesa', 'Mediano', 'Eficaz invención de la mayor calidad'),
(427, 'Geueqeforj', 'herramienta', 'Grande', 'Estridente invención para cualquiera'),
(428, 'Qauö', 'decoración de pared', 'Mediano', 'Imprescindible entelequia de primera necesidad'),
(429, 'Wäma', 'mueble', 'Grande', 'Útil objeto para cualquiera'),
(430, 'Nona', 'mueble', 'Pequeño', 'Estridente entelequia de primera necesidad'),
(431, 'Jëeöeëduz', 'herramienta', 'Grande', 'Adorableinvención para sacarte de un aprieto'),
(432, 'Zäoëeö', 'herramienta', 'Pequeño', 'Eficaz entelequia de la mayor calidad'),
(433, 'Yira-ve', 'mueble', 'Pequeño', 'Imprescindible cosa de buena relación calidad precio'),
(434, 'Mujmörseqäorj', 'mueble', 'Pequeño', 'Estridente invención para sacarte de un aprieto'),
(435, 'Wuw-lo-nö', 'herramienta', 'Grande', 'Adorableinvención de la mayor calidad'),
(436, 'Kumädë', 'mueble', 'Pequeño', 'Imprescindible invención para los más exigentes'),
(437, 'Nöse', 'herramienta', 'Grande', 'Adorableinvención de buena relación calidad precio'),
(438, 'Oe-böju', 'herramienta', 'Mediano', 'Imprescindible cosa de buena relación calidad precio'),
(439, 'Nu-cägu', 'decoración de pared', 'Pequeño', 'Útil invención para cualquiera'),
(440, 'Ee-lä', 'herramienta', 'Grande', 'Eficaz invención de primera necesidad'),
(441, 'Lowä', 'decoración de mesa', 'Pequeño', 'Adorableinvención para cualquiera'),
(442, 'Aë-ya', 'decoración de pared', 'Pequeño', 'Imprescindible cosa para los más exigentes'),
(443, 'Juiëqeuohlo', 'herramienta', 'Pequeño', 'Estridente objeto para los más exigentes'),
(444, 'Co-cav', 'herramienta', 'Grande', 'Estridente invención de la mayor calidad'),
(445, 'Ja-jöaë', 'herramienta', 'Pequeño', 'Útil objeto para los más exigentes'),
(446, 'Qäkupäej', 'decoración de pared', 'Mediano', 'Eficaz entelequia para sacarte de un aprieto'),
(447, 'Ku-ku', 'mueble', 'Grande', 'Útil invención para los más exigentes'),
(448, 'Aëzew', 'herramienta', 'Mediano', 'Eficaz cacharro para cualquiera'),
(449, 'Qelweda', 'mueble', 'Pequeño', 'Útil cacharro para cualquiera'),
(450, 'Cuyizo', 'herramienta', 'Mediano', 'Estridente cacharro de una calidad mediocre'),
(451, 'Zöoösoe', 'decoración de mesa', 'Grande', 'Eficaz invención para los más exigentes'),
(452, 'Leaeqe', 'decoración de mesa', 'Grande', 'Imprescindible cosa de la mayor calidad'),
(453, 'Rilu', 'mueble', 'Pequeño', 'Adorableobjeto para los más exigentes'),
(454, 'Römyunökvöaeorj', 'herramienta', 'Pequeño', 'Adorablecosa para el día a día'),
(455, 'Suqico-wëlorj', 'herramienta', 'Pequeño', 'Adorableobjeto para cualquiera'),
(456, 'Yawaoxisj', 'herramienta', 'Grande', 'Útil cosa para el día a día'),
(457, 'Oahru', 'herramienta', 'Mediano', 'Imprescindible invención para cualquiera'),
(458, 'Euna', 'herramienta', 'Mediano', 'Adorableinvención de primera necesidad'),
(459, 'Quj-tenojul', 'mueble', 'Mediano', 'Adorableentelequia para cualquiera'),
(460, 'Seieuë', 'mueble', 'Mediano', 'Eficaz cacharro para cualquiera'),
(461, 'Në-lë', 'herramienta', 'Pequeño', 'Estridente invención de primera necesidad'),
(462, 'Qavövauorj', 'herramienta', 'Mediano', 'Útil invención para los más exigentes'),
(463, 'Oic-aog', 'herramienta', 'Mediano', 'Útil objeto para cualquiera'),
(464, 'Meoaxä', 'decoración de mesa', 'Grande', 'Adorablecosa para cualquiera'),
(465, 'Pëja', 'herramienta', 'Grande', 'Imprescindible objeto para sacarte de un aprieto'),
(466, 'Yëuezö', 'mueble', 'Mediano', 'Útil entelequia para los más exigentes'),
(467, 'Oëfijökërj', 'decoración de mesa', 'Pequeño', 'Estridente cosa de primera necesidad'),
(468, 'Aa-vo', 'herramienta', 'Grande', 'Imprescindible cacharro para el día a día'),
(469, 'Uödike', 'mueble', 'Grande', 'Adorablecosa de una calidad mediocre'),
(470, 'Rijkadecä', 'herramienta', 'Mediano', 'Eficaz entelequia de primera necesidad'),
(471, 'Fuiesnë', 'herramienta', 'Grande', 'Adorablecacharro de primera necesidad'),
(472, 'Iewbauucäu', 'decoración de pared', 'Pequeño', 'Eficaz cacharro de buena relación calidad precio'),
(473, 'Fägaxjä', 'herramienta', 'Grande', 'Adorableobjeto de una calidad mediocre'),
(474, 'Löxqo', 'herramienta', 'Mediano', 'Adorableentelequia para sacarte de un aprieto'),
(475, 'Uujë', 'herramienta', 'Grande', 'Adorablecacharro de la mayor calidad'),
(476, 'Uauö', 'herramienta', 'Mediano', 'Imprescindible cacharro para el día a día'),
(477, 'Reeioukj', 'mueble', 'Pequeño', 'Útil entelequia para los más exigentes'),
(478, 'Reduga', 'decoración de mesa', 'Grande', 'Adorableentelequia de la mayor calidad'),
(479, 'Pi-sas', 'herramienta', 'Pequeño', 'Útil invención para sacarte de un aprieto'),
(480, 'Riq-oä', 'decoración de mesa', 'Pequeño', 'Imprescindible cosa para cualquiera'),
(481, 'Iöië', 'mueble', 'Pequeño', 'Útil cacharro de buena relación calidad precio'),
(482, 'Ba-mu', 'herramienta', 'Pequeño', 'Imprescindible cacharro para cualquiera'),
(483, 'Sä-xi', 'decoración de mesa', 'Grande', 'Eficaz entelequia para el día a día'),
(484, 'Sëjo', 'decoración de mesa', 'Pequeño', 'Estridente cacharro para sacarte de un aprieto'),
(485, 'Xiaföcö', 'herramienta', 'Pequeño', 'Estridente cosa para cualquiera'),
(486, 'Aubjëaö', 'herramienta', 'Pequeño', 'Útil entelequia de la mayor calidad'),
(487, 'Lu-yeyivej', 'decoración de mesa', 'Pequeño', 'Estridente entelequia de una calidad mediocre'),
(488, 'Sizij', 'decoración de mesa', 'Grande', 'Eficaz cacharro de la mayor calidad'),
(489, 'Wä-muuey', 'mueble', 'Grande', 'Estridente cosa para el día a día'),
(490, 'Eëmijë', 'herramienta', 'Mediano', 'Eficaz cacharro de primera necesidad'),
(491, 'Häyo', 'mueble', 'Mediano', 'Eficaz entelequia de primera necesidad'),
(492, 'Fucä', 'herramienta', 'Grande', 'Útil invención para cualquiera'),
(493, 'Oivyäoa', 'herramienta', 'Pequeño', 'Eficaz objeto para el día a día'),
(494, 'Yë-jisäio', 'decoración de mesa', 'Mediano', 'Adorableinvención para el día a día'),
(495, 'Euio', 'herramienta', 'Pequeño', 'Útil cosa para sacarte de un aprieto'),
(496, 'Zär-mu', 'decoración de mesa', 'Mediano', 'Imprescindible objeto para cualquiera'),
(497, 'Foto', 'herramienta', 'Pequeño', 'Eficaz cacharro para cualquiera'),
(498, 'Hab-hëou', 'decoración de mesa', 'Pequeño', 'Estridente cosa de la mayor calidad'),
(499, 'Qëw-di', 'mueble', 'Grande', 'Útil objeto de la mayor calidad'),
(500, 'Yözë', 'herramienta', 'Grande', 'Estridente objeto de primera necesidad'),
(501, 'Moku', 'mueble', 'Mediano', 'Útil cosa para sacarte de un aprieto'),
(502, 'Yiguiiu', 'decoración de pared', 'Pequeño', 'Eficaz cacharro para sacarte de un aprieto'),
(503, 'Pöbiahej', 'herramienta', 'Mediano', 'Eficaz cosa de primera necesidad'),
(504, 'Cë-qä', 'decoración de mesa', 'Mediano', 'Eficaz entelequia para los más exigentes'),
(505, 'Ha-pöcajj', 'mueble', 'Grande', 'Eficaz invención para los más exigentes'),
(506, 'Pa-hau', 'herramienta', 'Grande', 'Útil invención para sacarte de un aprieto'),
(507, 'Aih-xi', 'decoración de mesa', 'Pequeño', 'Adorableinvención para cualquiera'),
(508, 'Aiji', 'decoración de mesa', 'Grande', 'Adorableobjeto para sacarte de un aprieto'),
(509, 'Së-hi', 'mueble', 'Pequeño', 'Imprescindible cacharro para cualquiera'),
(510, 'Xupobo', 'mueble', 'Mediano', 'Útil objeto para cualquiera'),
(511, 'Uöräfi', 'herramienta', 'Pequeño', 'Estridente invención de primera necesidad'),
(512, 'Qu-hö', 'decoración de pared', 'Pequeño', 'Estridente invención de primera necesidad'),
(513, 'Iët-la', 'herramienta', 'Grande', 'Útil objeto de la mayor calidad'),
(514, 'Wöfebä', 'herramienta', 'Mediano', 'Eficaz invención de primera necesidad'),
(515, 'Uäfa', 'herramienta', 'Grande', 'Útil objeto de la mayor calidad'),
(516, 'Ni-yofä', 'mueble', 'Mediano', 'Imprescindible entelequia para sacarte de un aprieto'),
(517, 'Näae', 'decoración de mesa', 'Grande', 'Imprescindible cacharro de la mayor calidad'),
(518, 'Eëa-ru', 'decoración de mesa', 'Pequeño', 'Adorablecacharro de primera necesidad'),
(519, 'Qiuëdë', 'decoración de pared', 'Pequeño', 'Eficaz objeto para los más exigentes'),
(520, 'Këduanej', 'mueble', 'Mediano', 'Imprescindible entelequia para cualquiera'),
(521, 'Dëclejö', 'decoración de pared', 'Grande', 'Estridente entelequia de primera necesidad'),
(522, 'Iuqeuo', 'mueble', 'Mediano', 'Útil entelequia para los más exigentes'),
(523, 'Qooi', 'decoración de mesa', 'Pequeño', 'Imprescindible cosa para cualquiera'),
(524, 'Siqö', 'mueble', 'Pequeño', 'Estridente cosa para sacarte de un aprieto'),
(525, 'Ie-iä', 'decoración de pared', 'Grande', 'Útil cosa de primera necesidad'),
(526, 'Tëv-vo', 'herramienta', 'Pequeño', 'Eficaz entelequia para sacarte de un aprieto'),
(527, 'Tem-ië', 'herramienta', 'Mediano', 'Estridente invención para cualquiera'),
(528, 'Ra-qorë', 'decoración de pared', 'Mediano', 'Adorableentelequia de primera necesidad'),
(529, 'Ue-gin', 'herramienta', 'Pequeño', 'Útil entelequia de primera necesidad'),
(530, 'Leyë-seyëfj', 'mueble', 'Grande', 'Adorableinvención para sacarte de un aprieto'),
(531, 'Fë-zë', 'mueble', 'Mediano', 'Imprescindible objeto para cualquiera'),
(532, 'Iohu', 'herramienta', 'Pequeño', 'Útil invención para el día a día'),
(533, 'Mä-vuuwe', 'decoración de mesa', 'Pequeño', 'Imprescindible objeto para los más exigentes'),
(534, 'Lägnue-vo', 'mueble', 'Mediano', 'Adorablecosa para sacarte de un aprieto'),
(535, 'Foma', 'mueble', 'Mediano', 'Imprescindible cosa de primera necesidad'),
(536, 'Fo-xä-aaoo', 'decoración de pared', 'Pequeño', 'Estridente cosa de una calidad mediocre'),
(537, 'Hözo', 'mueble', 'Mediano', 'Eficaz cosa de primera necesidad'),
(538, 'Qësëvësëc', 'herramienta', 'Pequeño', 'Imprescindible cacharro de primera necesidad'),
(539, 'Kiwjoweca', 'decoración de mesa', 'Pequeño', 'Adorablecosa para cualquiera'),
(540, 'Xuniwä', 'mueble', 'Grande', 'Útil entelequia de primera necesidad'),
(541, 'Qäy-ke', 'decoración de mesa', 'Pequeño', 'Útil objeto para el día a día'),
(542, 'Dojeno', 'herramienta', 'Grande', 'Adorablecacharro para sacarte de un aprieto'),
(543, 'Vëla', 'decoración de mesa', 'Pequeño', 'Estridente objeto para cualquiera'),
(544, 'Da-ië', 'mueble', 'Grande', 'Eficaz cosa de una calidad mediocre'),
(545, 'Fee-kölë', 'mueble', 'Mediano', 'Útil invención de primera necesidad'),
(546, 'Funö', 'decoración de pared', 'Mediano', 'Imprescindible cacharro para el día a día'),
(547, 'Oäy-tiaaz', 'mueble', 'Grande', 'Estridente invención para sacarte de un aprieto'),
(548, 'Zoei-nö', 'mueble', 'Mediano', 'Imprescindible cosa para sacarte de un aprieto'),
(549, 'Höhfa', 'decoración de mesa', 'Grande', 'Eficaz invención para el día a día'),
(550, 'Iö-aä', 'herramienta', 'Mediano', 'Útil invención para cualquiera'),
(551, 'Zeeë', 'mueble', 'Mediano', 'Imprescindible cosa de primera necesidad'),
(552, 'Yalö', 'decoración de mesa', 'Grande', 'Eficaz invención de primera necesidad'),
(553, 'Aäba', 'herramienta', 'Mediano', 'Imprescindible invención para sacarte de un aprieto'),
(554, 'Rö-sëho', 'herramienta', 'Mediano', 'Estridente objeto de una calidad mediocre'),
(555, 'Dije', 'mueble', 'Mediano', 'Adorableinvención para el día a día'),
(556, 'Oäfa', 'herramienta', 'Mediano', 'Adorableentelequia de buena relación calidad precio'),
(557, 'Xu-zäcu', 'mueble', 'Pequeño', 'Adorableobjeto de una calidad mediocre'),
(558, 'Va-cucha', 'decoración de pared', 'Grande', 'Imprescindible cosa de primera necesidad'),
(559, 'Kö-qe', 'herramienta', 'Mediano', 'Estridente invención de primera necesidad'),
(560, 'Rahezo', 'mueble', 'Pequeño', 'Imprescindible objeto para cualquiera'),
(561, 'Xoiä', 'herramienta', 'Pequeño', 'Adorableobjeto de la mayor calidad'),
(562, 'Pëutoyëu', 'herramienta', 'Grande', 'Estridente entelequia para sacarte de un aprieto'),
(563, 'Ië-io', 'herramienta', 'Mediano', 'Eficaz cacharro para los más exigentes'),
(564, 'Temecëdj', 'herramienta', 'Mediano', 'Adorableobjeto para el día a día'),
(565, 'Du-ju', 'herramienta', 'Mediano', 'Útil entelequia para cualquiera'),
(566, 'Zëouuëc', 'mueble', 'Pequeño', 'Eficaz entelequia de la mayor calidad'),
(567, 'Vu-vë', 'mueble', 'Mediano', 'Útil entelequia de una calidad mediocre'),
(568, 'Gemhägik', 'herramienta', 'Mediano', 'Eficaz entelequia para el día a día'),
(569, 'Jofo', 'decoración de mesa', 'Pequeño', 'Eficaz invención de una calidad mediocre'),
(570, 'Qä-közë', 'decoración de mesa', 'Mediano', 'Útil entelequia para cualquiera'),
(571, 'Fäcgä', 'herramienta', 'Mediano', 'Estridente cosa para el día a día'),
(572, 'Uu-tu', 'decoración de pared', 'Mediano', 'Estridente objeto de buena relación calidad precio'),
(573, 'Uas-laeë', 'herramienta', 'Pequeño', 'Eficaz entelequia de buena relación calidad precio'),
(574, 'Zëzyuko', 'decoración de mesa', 'Pequeño', 'Adorableentelequia para sacarte de un aprieto'),
(575, 'Fatfö', 'herramienta', 'Pequeño', 'Eficaz cosa de una calidad mediocre'),
(576, 'Lätqu', 'decoración de pared', 'Grande', 'Eficaz objeto para cualquiera'),
(577, 'Jino', 'herramienta', 'Grande', 'Estridente invención de la mayor calidad'),
(578, 'Foualä', 'mueble', 'Grande', 'Eficaz objeto de buena relación calidad precio'),
(579, 'Yöra', 'mueble', 'Pequeño', 'Estridente cacharro de la mayor calidad'),
(580, 'Jëra', 'mueble', 'Mediano', 'Imprescindible cosa para el día a día'),
(581, 'Vähi', 'herramienta', 'Grande', 'Estridente cacharro de la mayor calidad'),
(582, 'Qëca', 'herramienta', 'Mediano', 'Útil entelequia de primera necesidad'),
(583, 'Po-juoeeöea', 'mueble', 'Pequeño', 'Estridente cacharro para sacarte de un aprieto'),
(584, 'Tanseuanj', 'herramienta', 'Mediano', 'Imprescindible invención para sacarte de un aprieto'),
(585, 'Jëmo', 'mueble', 'Pequeño', 'Útil objeto de primera necesidad'),
(586, 'Fëxa', 'herramienta', 'Pequeño', 'Imprescindible entelequia de buena relación calidad precio'),
(587, 'Täq-zä', 'herramienta', 'Grande', 'Adorablecacharro para los más exigentes'),
(588, 'Buwoohäe', 'decoración de mesa', 'Mediano', 'Eficaz objeto de la mayor calidad'),
(589, 'Hë-aeco', 'mueble', 'Grande', 'Estridente cosa de la mayor calidad'),
(590, 'Aipoue', 'mueble', 'Mediano', 'Útil cacharro para sacarte de un aprieto'),
(591, 'Oö-qalo', 'decoración de mesa', 'Mediano', 'Eficaz objeto de buena relación calidad precio'),
(592, 'Ki-iu', 'herramienta', 'Mediano', 'Eficaz objeto para cualquiera'),
(593, 'Pooioa', 'mueble', 'Pequeño', 'Estridente cacharro para los más exigentes'),
(594, 'Wibköv', 'decoración de mesa', 'Pequeño', 'Adorableinvención de la mayor calidad'),
(595, 'Liwäpë', 'mueble', 'Mediano', 'Adorablecosa de primera necesidad'),
(596, 'Za-go', 'decoración de pared', 'Pequeño', 'Útil cosa para cualquiera'),
(597, 'Tu-ya', 'mueble', 'Grande', 'Imprescindible objeto de primera necesidad'),
(598, 'Huwepi', 'decoración de mesa', 'Mediano', 'Estridente cacharro para el día a día'),
(599, 'Oa-yonovize', 'mueble', 'Mediano', 'Útil entelequia para sacarte de un aprieto'),
(600, 'Qög-möeej', 'herramienta', 'Mediano', 'Imprescindible cosa de una calidad mediocre'),
(601, 'Wöue', 'decoración de mesa', 'Pequeño', 'Adorableinvención de la mayor calidad'),
(602, 'Hävo', 'decoración de mesa', 'Mediano', 'Imprescindible objeto para sacarte de un aprieto'),
(603, 'Zeli-ne', 'herramienta', 'Mediano', 'Imprescindible cacharro de buena relación calidad precio'),
(604, 'Mözösa', 'mueble', 'Mediano', 'Eficaz cacharro de primera necesidad'),
(605, 'Puuböca', 'mueble', 'Grande', 'Estridente objeto de primera necesidad'),
(606, 'Aëwo', 'decoración de mesa', 'Grande', 'Útil cosa para los más exigentes'),
(607, 'Eud-tötä', 'decoración de pared', 'Mediano', 'Útil cosa de la mayor calidad'),
(608, 'Gooa', 'decoración de mesa', 'Pequeño', 'Útil entelequia de una calidad mediocre'),
(609, 'Aa-hä', 'mueble', 'Mediano', 'Útil invención de la mayor calidad'),
(610, 'Eusa', 'decoración de pared', 'Mediano', 'Estridente cacharro para sacarte de un aprieto'),
(611, 'Cu-kumaui', 'decoración de mesa', 'Pequeño', 'Útil objeto de primera necesidad'),
(612, 'Ziz-xö', 'herramienta', 'Pequeño', 'Imprescindible objeto de la mayor calidad'),
(613, 'Cutihu', 'decoración de pared', 'Mediano', 'Estridente entelequia de primera necesidad'),
(614, 'Jö-koeu', 'herramienta', 'Mediano', 'Eficaz entelequia para cualquiera'),
(615, 'Mewöte', 'mueble', 'Mediano', 'Útil invención de primera necesidad'),
(616, 'Tägeo', 'mueble', 'Pequeño', 'Eficaz entelequia de buena relación calidad precio'),
(617, 'Pöu-we', 'herramienta', 'Mediano', 'Estridente entelequia de buena relación calidad precio'),
(618, 'Nevë', 'herramienta', 'Mediano', 'Eficaz objeto para sacarte de un aprieto'),
(619, 'Weqyu', 'herramienta', 'Grande', 'Útil cosa para los más exigentes'),
(620, 'Täai-mä', 'mueble', 'Mediano', 'Eficaz invención de primera necesidad'),
(621, 'Röuödvi', 'decoración de mesa', 'Grande', 'Imprescindible entelequia de primera necesidad'),
(622, 'Zu-haiä', 'herramienta', 'Pequeño', 'Estridente cacharro para cualquiera'),
(623, 'Bëva', 'herramienta', 'Mediano', 'Estridente entelequia de buena relación calidad precio'),
(624, 'Wë-bö', 'mueble', 'Grande', 'Estridente cacharro para los más exigentes'),
(625, 'Jawëeö', 'mueble', 'Pequeño', 'Adorableentelequia de buena relación calidad precio'),
(626, 'Böwöcig', 'mueble', 'Grande', 'Adorablecosa de la mayor calidad'),
(627, 'Beea', 'decoración de pared', 'Grande', 'Eficaz invención para el día a día'),
(628, 'Dävi', 'decoración de pared', 'Grande', 'Estridente entelequia de primera necesidad'),
(629, 'Dolici', 'decoración de mesa', 'Pequeño', 'Imprescindible invención de una calidad mediocre'),
(630, 'Reterözj', 'mueble', 'Mediano', 'Estridente invención de buena relación calidad precio'),
(631, 'Gë-yiu-buvi', 'decoración de mesa', 'Pequeño', 'Estridente invención de la mayor calidad'),
(632, 'Jefakuë', 'mueble', 'Mediano', 'Útil invención de buena relación calidad precio'),
(633, 'Sö-gocuqä', 'herramienta', 'Grande', 'Eficaz objeto para los más exigentes'),
(634, 'Oagooei', 'mueble', 'Mediano', 'Útil invención de primera necesidad'),
(635, 'Jäpögë', 'decoración de pared', 'Mediano', 'Imprescindible entelequia para los más exigentes'),
(636, 'Xaetiozu', 'decoración de pared', 'Mediano', 'Eficaz entelequia para los más exigentes'),
(637, 'Mëpä', 'mueble', 'Mediano', 'Imprescindible cacharro de la mayor calidad'),
(638, 'Vëbaoaq', 'mueble', 'Grande', 'Eficaz invención de una calidad mediocre'),
(639, 'Zo-më', 'herramienta', 'Pequeño', 'Adorableinvención de primera necesidad'),
(640, 'Föovö', 'herramienta', 'Pequeño', 'Estridente invención de primera necesidad'),
(641, 'Yë-mi', 'mueble', 'Grande', 'Estridente entelequia de buena relación calidad precio'),
(642, 'Nätiqë', 'decoración de mesa', 'Mediano', 'Estridente invención para los más exigentes'),
(643, 'Däue', 'decoración de mesa', 'Grande', 'Imprescindible cacharro de buena relación calidad precio'),
(644, 'Nökuyo', 'decoración de pared', 'Mediano', 'Adorablecacharro de una calidad mediocre'),
(645, 'Vöoi', 'herramienta', 'Mediano', 'Imprescindible cacharro de la mayor calidad'),
(646, 'Wëwaahä', 'decoración de mesa', 'Pequeño', 'Útil cacharro de una calidad mediocre'),
(647, 'Gäköcu-si', 'mueble', 'Grande', 'Eficaz objeto de la mayor calidad'),
(648, 'Jigixöu', 'mueble', 'Mediano', 'Imprescindible entelequia para los más exigentes'),
(649, 'Uuho', 'mueble', 'Mediano', 'Adorableobjeto de una calidad mediocre'),
(650, 'Iioeyëj', 'herramienta', 'Grande', 'Eficaz entelequia para sacarte de un aprieto'),
(651, 'Eiyëtö', 'herramienta', 'Pequeño', 'Útil cosa de buena relación calidad precio'),
(652, 'Pörli', 'decoración de mesa', 'Mediano', 'Estridente cacharro para los más exigentes'),
(653, 'Yugeu', 'herramienta', 'Pequeño', 'Adorablecacharro de una calidad mediocre'),
(654, 'Këiövvi', 'mueble', 'Pequeño', 'Eficaz objeto de buena relación calidad precio'),
(655, 'Ca-kä', 'herramienta', 'Mediano', 'Útil cacharro para los más exigentes'),
(656, 'Löfusë', 'herramienta', 'Mediano', 'Adorableentelequia de primera necesidad'),
(657, 'Iä-fu', 'herramienta', 'Grande', 'Estridente cosa de buena relación calidad precio'),
(658, 'Fëeë', 'decoración de pared', 'Pequeño', 'Imprescindible cacharro para cualquiera'),
(659, 'Këmä', 'mueble', 'Pequeño', 'Útil cacharro para el día a día'),
(660, 'Ouf-juse', 'herramienta', 'Pequeño', 'Estridente objeto para cualquiera'),
(661, 'Lö-täjocorj', 'herramienta', 'Grande', 'Imprescindible objeto de buena relación calidad precio'),
(662, 'Lona', 'herramienta', 'Grande', 'Útil cosa para cualquiera'),
(663, 'Qäniog', 'herramienta', 'Mediano', 'Imprescindible entelequia para sacarte de un aprieto'),
(664, 'Si-eëdsäaiq', 'herramienta', 'Grande', 'Adorablecacharro de la mayor calidad'),
(665, 'Tëhäfäfj', 'mueble', 'Grande', 'Adorablecacharro para el día a día'),
(666, 'Fö-ju', 'mueble', 'Mediano', 'Estridente objeto de buena relación calidad precio'),
(667, 'Cisaa', 'mueble', 'Mediano', 'Estridente cosa de buena relación calidad precio'),
(668, 'Ou-nösönorj', 'decoración de mesa', 'Mediano', 'Adorableobjeto de buena relación calidad precio'),
(669, 'Fëe-ji', 'decoración de mesa', 'Pequeño', 'Adorablecacharro para cualquiera'),
(670, 'Haxä', 'herramienta', 'Mediano', 'Imprescindible cosa para cualquiera'),
(671, 'Oe-eacomepj', 'mueble', 'Pequeño', 'Adorableentelequia de primera necesidad'),
(672, 'Yëro', 'herramienta', 'Mediano', 'Imprescindible cosa para cualquiera'),
(673, 'Cöh-nävhë', 'mueble', 'Pequeño', 'Eficaz objeto para el día a día'),
(674, 'Zixiao', 'mueble', 'Pequeño', 'Adorableentelequia para sacarte de un aprieto'),
(675, 'Nujöuëyëjj', 'herramienta', 'Grande', 'Útil cacharro de una calidad mediocre'),
(676, 'Liku', 'herramienta', 'Mediano', 'Estridente cosa para sacarte de un aprieto'),
(677, 'Mub-tö', 'mueble', 'Pequeño', 'Eficaz cacharro para los más exigentes'),
(678, 'Mira', 'mueble', 'Grande', 'Eficaz entelequia para cualquiera'),
(679, 'Tirqe', 'mueble', 'Pequeño', 'Útil cacharro para cualquiera'),
(680, 'Aefeji', 'herramienta', 'Pequeño', 'Eficaz cacharro para cualquiera'),
(681, 'Gëbe', 'mueble', 'Grande', 'Adorablecacharro para los más exigentes'),
(682, 'Zi-eo', 'decoración de mesa', 'Pequeño', 'Útil cacharro para sacarte de un aprieto'),
(683, 'Tälule', 'decoración de mesa', 'Grande', 'Útil cosa de la mayor calidad'),
(684, 'Nöao', 'herramienta', 'Mediano', 'Adorableinvención de primera necesidad'),
(685, 'Päg-aä-wäoj', 'mueble', 'Grande', 'Útil cosa para el día a día'),
(686, 'Vaue', 'herramienta', 'Grande', 'Eficaz entelequia para sacarte de un aprieto'),
(687, 'Eëc-nëha', 'decoración de mesa', 'Pequeño', 'Imprescindible cosa para sacarte de un aprieto'),
(688, 'Gu-ve', 'herramienta', 'Grande', 'Adorableobjeto para el día a día'),
(689, 'Jëwna', 'decoración de mesa', 'Grande', 'Eficaz cosa para los más exigentes'),
(690, 'Xëzi', 'herramienta', 'Mediano', 'Estridente entelequia de primera necesidad'),
(691, 'Iëjäböhj', 'herramienta', 'Grande', 'Adorableinvención para el día a día'),
(692, 'Qeqo', 'decoración de pared', 'Pequeño', 'Imprescindible entelequia para sacarte de un aprieto'),
(693, 'Sëyö', 'mueble', 'Grande', 'Adorableentelequia de primera necesidad'),
(694, 'Väwodu', 'herramienta', 'Pequeño', 'Adorableinvención de buena relación calidad precio'),
(695, 'Uu-lo-fi', 'herramienta', 'Grande', 'Útil entelequia para los más exigentes'),
(696, 'Räri', 'mueble', 'Mediano', 'Imprescindible objeto de primera necesidad'),
(697, 'Möxi', 'decoración de mesa', 'Mediano', 'Estridente invención de una calidad mediocre'),
(698, 'Wöjeau', 'decoración de mesa', 'Grande', 'Imprescindible cacharro de una calidad mediocre'),
(699, 'Täzë', 'decoración de mesa', 'Pequeño', 'Imprescindible entelequia de una calidad mediocre'),
(700, 'Dänusäzj', 'herramienta', 'Pequeño', 'Eficaz cosa de la mayor calidad'),
(701, 'Boro', 'decoración de mesa', 'Grande', 'Eficaz cacharro para cualquiera'),
(702, 'Fiväqus', 'decoración de pared', 'Mediano', 'Estridente entelequia para cualquiera'),
(703, 'Goinazëj', 'herramienta', 'Mediano', 'Estridente cacharro para el día a día'),
(704, 'Aogä', 'decoración de pared', 'Pequeño', 'Útil invención para sacarte de un aprieto'),
(705, 'Jäcu', 'herramienta', 'Pequeño', 'Útil entelequia de la mayor calidad'),
(706, 'Lacuu', 'mueble', 'Grande', 'Adorableinvención de primera necesidad'),
(707, 'Bë-zö', 'decoración de mesa', 'Pequeño', 'Útil objeto para sacarte de un aprieto'),
(708, 'Xem-se', 'herramienta', 'Mediano', 'Eficaz objeto de una calidad mediocre'),
(709, 'Kubo', 'decoración de mesa', 'Pequeño', 'Útil entelequia de primera necesidad'),
(710, 'Cuwe', 'herramienta', 'Pequeño', 'Imprescindible objeto para el día a día'),
(711, 'Ieoäxoj', 'herramienta', 'Mediano', 'Estridente cacharro para sacarte de un aprieto'),
(712, 'Ius-uo', 'herramienta', 'Mediano', 'Imprescindible cosa de la mayor calidad'),
(713, 'Yës-aasäf', 'decoración de mesa', 'Grande', 'Adorableentelequia para el día a día'),
(714, 'Fa-bokömo', 'decoración de mesa', 'Mediano', 'Eficaz entelequia de primera necesidad'),
(715, 'Vë-cöge', 'herramienta', 'Grande', 'Eficaz cacharro de buena relación calidad precio'),
(716, 'Këm-hirö', 'herramienta', 'Mediano', 'Útil cacharro para cualquiera'),
(717, 'Goee', 'herramienta', 'Mediano', 'Estridente objeto de una calidad mediocre'),
(718, 'Moa-ba', 'decoración de mesa', 'Mediano', 'Adorablecacharro para sacarte de un aprieto'),
(719, 'Sup-lö', 'herramienta', 'Pequeño', 'Adorableentelequia de buena relación calidad precio'),
(720, 'Gu-wu', 'mueble', 'Grande', 'Útil cosa de buena relación calidad precio'),
(721, 'Jë-lobäj', 'herramienta', 'Mediano', 'Adorableinvención de primera necesidad'),
(722, 'Juxëvëoäorj', 'decoración de mesa', 'Grande', 'Eficaz cosa de buena relación calidad precio'),
(723, 'Ho-piqëiorj', 'mueble', 'Pequeño', 'Imprescindible cacharro de primera necesidad'),
(724, 'Vära', 'herramienta', 'Pequeño', 'Imprescindible entelequia de primera necesidad'),
(725, 'Tavueëxë', 'herramienta', 'Grande', 'Útil cacharro de primera necesidad'),
(726, 'Uo-di', 'decoración de pared', 'Mediano', 'Útil objeto de primera necesidad'),
(727, 'Järohëoë', 'herramienta', 'Mediano', 'Útil objeto de primera necesidad'),
(728, 'Pöunë', 'mueble', 'Grande', 'Estridente cosa de primera necesidad'),
(729, 'Zoxëziua', 'mueble', 'Mediano', 'Estridente objeto para sacarte de un aprieto'),
(730, 'Momö', 'decoración de pared', 'Grande', 'Útil objeto de la mayor calidad'),
(731, 'Pi-vaiu', 'decoración de pared', 'Grande', 'Útil objeto de buena relación calidad precio'),
(732, 'Kih-vö-to', 'herramienta', 'Mediano', 'Útil objeto de la mayor calidad'),
(733, 'Jöso', 'herramienta', 'Mediano', 'Estridente cacharro para sacarte de un aprieto'),
(734, 'Zulöbi', 'decoración de mesa', 'Grande', 'Adorableentelequia de la mayor calidad'),
(735, 'Weeu-le', 'decoración de mesa', 'Grande', 'Adorablecosa de primera necesidad'),
(736, 'Qime', 'decoración de mesa', 'Pequeño', 'Eficaz objeto para sacarte de un aprieto'),
(737, 'Päj-pu', 'decoración de mesa', 'Mediano', 'Estridente entelequia para sacarte de un aprieto'),
(738, 'Kö-tu', 'herramienta', 'Mediano', 'Útil cosa de la mayor calidad'),
(739, 'Cuqveterfolj', 'herramienta', 'Grande', 'Estridente objeto de primera necesidad'),
(740, 'Lödu', 'herramienta', 'Mediano', 'Adorableobjeto de la mayor calidad'),
(741, 'Nun-ro-zëcj', 'decoración de pared', 'Mediano', 'Estridente cosa de la mayor calidad'),
(742, 'Ief-aä', 'herramienta', 'Mediano', 'Imprescindible invención de una calidad mediocre'),
(743, 'Kä-jokä', 'decoración de mesa', 'Pequeño', 'Eficaz entelequia para cualquiera'),
(744, 'Aäke', 'herramienta', 'Mediano', 'Estridente invención para cualquiera'),
(745, 'Aoyui', 'mueble', 'Mediano', 'Estridente objeto para los más exigentes'),
(746, 'Väae', 'mueble', 'Pequeño', 'Adorableentelequia de la mayor calidad'),
(747, 'Uöau', 'mueble', 'Pequeño', 'Adorablecosa de una calidad mediocre'),
(748, 'Peaö', 'herramienta', 'Mediano', 'Estridente cacharro de primera necesidad'),
(749, 'Ta-vu', 'herramienta', 'Pequeño', 'Estridente objeto para los más exigentes'),
(750, 'Fax-fë', 'mueble', 'Pequeño', 'Eficaz invención para los más exigentes'),
(751, 'Vo-la', 'herramienta', 'Mediano', 'Imprescindible objeto de una calidad mediocre'),
(752, 'Döpouä', 'herramienta', 'Grande', 'Eficaz cosa de la mayor calidad'),
(753, 'Aigi', 'herramienta', 'Grande', 'Adorableentelequia para los más exigentes'),
(754, 'Së-vagö', 'mueble', 'Mediano', 'Estridente objeto de primera necesidad'),
(755, 'Gohedkunorj', 'herramienta', 'Grande', 'Imprescindible cosa de una calidad mediocre'),
(756, 'Mi-aäpi', 'mueble', 'Pequeño', 'Adorableentelequia para cualquiera'),
(757, 'Ho-doquo', 'decoración de pared', 'Grande', 'Eficaz cacharro de buena relación calidad precio'),
(758, 'Vivzö', 'mueble', 'Mediano', 'Imprescindible cosa para el día a día'),
(759, 'Qu-ciy', 'herramienta', 'Grande', 'Útil cacharro de buena relación calidad precio'),
(760, 'Quf-fuorj', 'mueble', 'Pequeño', 'Adorablecacharro de una calidad mediocre'),
(761, 'Tu-zä', 'herramienta', 'Pequeño', 'Útil objeto para sacarte de un aprieto'),
(762, 'Räio', 'mueble', 'Grande', 'Adorablecacharro para cualquiera'),
(763, 'Satcejuëzä', 'decoración de pared', 'Mediano', 'Adorablecacharro para cualquiera'),
(764, 'Dip-jö', 'herramienta', 'Grande', 'Imprescindible objeto de una calidad mediocre'),
(765, 'Ya-vij', 'mueble', 'Grande', 'Eficaz entelequia de la mayor calidad'),
(766, 'Köya', 'herramienta', 'Pequeño', 'Imprescindible objeto de la mayor calidad'),
(767, 'Xopezey', 'herramienta', 'Mediano', 'Útil cosa de una calidad mediocre'),
(768, 'Bok-lolevdi', 'herramienta', 'Mediano', 'Imprescindible cosa de buena relación calidad precio'),
(769, 'Liyowziwj', 'herramienta', 'Pequeño', 'Adorableobjeto de la mayor calidad'),
(770, 'Co-zaei', 'decoración de mesa', 'Grande', 'Adorablecacharro para el día a día'),
(771, 'Yonëi', 'mueble', 'Pequeño', 'Eficaz invención para los más exigentes'),
(772, 'Xäiëzë-de', 'herramienta', 'Grande', 'Adorableentelequia de buena relación calidad precio'),
(773, 'Uözeia', 'decoración de mesa', 'Grande', 'Imprescindible objeto para el día a día'),
(774, 'Bu-yë', 'herramienta', 'Mediano', 'Adorableentelequia para los más exigentes'),
(775, 'Giq-wa', 'herramienta', 'Grande', 'Imprescindible cosa para sacarte de un aprieto'),
(776, 'Ya-dëgo', 'mueble', 'Mediano', 'Imprescindible entelequia de primera necesidad'),
(777, 'Nubö', 'herramienta', 'Pequeño', 'Estridente cacharro para los más exigentes'),
(778, 'Qafän', 'decoración de mesa', 'Mediano', 'Eficaz cacharro para los más exigentes'),
(779, 'Oöpoköorj', 'herramienta', 'Mediano', 'Adorablecosa de una calidad mediocre'),
(780, 'Jed-sö', 'decoración de pared', 'Pequeño', 'Útil invención de la mayor calidad'),
(781, 'Yooa', 'decoración de mesa', 'Grande', 'Estridente invención para sacarte de un aprieto'),
(782, 'Pö-oiiaawë', 'herramienta', 'Mediano', 'Eficaz cacharro de la mayor calidad'),
(783, 'Hooi', 'decoración de mesa', 'Grande', 'Eficaz cacharro de buena relación calidad precio'),
(784, 'Zobiu', 'mueble', 'Grande', 'Estridente invención de primera necesidad'),
(785, 'Uo-fe', 'herramienta', 'Mediano', 'Adorablecosa para los más exigentes'),
(786, 'Majä', 'decoración de pared', 'Pequeño', 'Imprescindible entelequia de buena relación calidad precio'),
(787, 'Cöxärö', 'mueble', 'Grande', 'Imprescindible invención para cualquiera'),
(788, 'Uuq-ni', 'decoración de pared', 'Mediano', 'Imprescindible cosa de la mayor calidad'),
(789, 'Rërapä', 'decoración de pared', 'Pequeño', 'Útil cosa de buena relación calidad precio'),
(790, 'Be-dupooëvj', 'decoración de mesa', 'Pequeño', 'Útil objeto de una calidad mediocre'),
(791, 'Cag-ravo', 'herramienta', 'Pequeño', 'Imprescindible invención para el día a día'),
(792, 'Oieozä', 'mueble', 'Pequeño', 'Imprescindible objeto de la mayor calidad'),
(793, 'Air-io', 'mueble', 'Pequeño', 'Imprescindible entelequia de la mayor calidad'),
(794, 'Säaäzä', 'herramienta', 'Mediano', 'Imprescindible cosa para los más exigentes'),
(795, 'Nä-pu', 'decoración de mesa', 'Grande', 'Imprescindible objeto de una calidad mediocre'),
(796, 'Zë-ho', 'decoración de pared', 'Mediano', 'Imprescindible invención para cualquiera'),
(797, 'Cäyueöc', 'mueble', 'Mediano', 'Imprescindible cosa para cualquiera'),
(798, 'Lu-nö', 'herramienta', 'Mediano', 'Estridente cosa para los más exigentes'),
(799, 'Iefö', 'decoración de pared', 'Mediano', 'Estridente invención para los más exigentes'),
(800, 'Docalöj', 'decoración de mesa', 'Mediano', 'Imprescindible invención para el día a día'),
(801, 'Va-gä', 'mueble', 'Mediano', 'Imprescindible cacharro de la mayor calidad'),
(802, 'Ae-rö', 'decoración de mesa', 'Pequeño', 'Útil invención para cualquiera'),
(803, 'Röju', 'herramienta', 'Grande', 'Útil invención para cualquiera'),
(804, 'Fu-bitä', 'decoración de pared', 'Mediano', 'Adorableobjeto para los más exigentes'),
(805, 'Yo-tiqam', 'herramienta', 'Mediano', 'Estridente objeto para sacarte de un aprieto'),
(806, 'Iäzë', 'herramienta', 'Mediano', 'Eficaz invención para sacarte de un aprieto'),
(807, 'Je-fö', 'decoración de pared', 'Mediano', 'Útil cosa de primera necesidad'),
(808, 'Ci-bä', 'herramienta', 'Grande', 'Eficaz invención para sacarte de un aprieto'),
(809, 'Xumoueäfca', 'decoración de mesa', 'Mediano', 'Adorableentelequia de la mayor calidad'),
(810, 'Oi-qi', 'herramienta', 'Grande', 'Útil entelequia para el día a día'),
(811, 'Eoeemö', 'mueble', 'Mediano', 'Imprescindible entelequia de buena relación calidad precio'),
(812, 'Aälöka', 'mueble', 'Mediano', 'Útil cosa para los más exigentes'),
(813, 'To-cä', 'decoración de pared', 'Pequeño', 'Útil entelequia para el día a día'),
(814, 'Ja-sërof', 'mueble', 'Grande', 'Imprescindible cosa para sacarte de un aprieto'),
(815, 'Möl-jo', 'herramienta', 'Grande', 'Estridente invención para cualquiera'),
(816, 'Bek-ea', 'herramienta', 'Grande', 'Estridente objeto para cualquiera'),
(817, 'Köpä', 'herramienta', 'Mediano', 'Estridente cosa de primera necesidad'),
(818, 'Josgë', 'decoración de mesa', 'Mediano', 'Imprescindible cacharro de primera necesidad'),
(819, 'Sëhzi', 'herramienta', 'Pequeño', 'Imprescindible invención de primera necesidad'),
(820, 'Dëfeyi', 'decoración de pared', 'Grande', 'Eficaz entelequia de primera necesidad'),
(821, 'Në-hëjöuba', 'decoración de mesa', 'Mediano', 'Adorablecacharro de primera necesidad'),
(822, 'Qijqëmo', 'decoración de mesa', 'Mediano', 'Adorableobjeto para los más exigentes'),
(823, 'Däseriae', 'decoración de mesa', 'Pequeño', 'Adorableentelequia para los más exigentes'),
(824, 'Gäikiye', 'herramienta', 'Pequeño', 'Estridente invención para los más exigentes'),
(825, 'Wolowtë', 'decoración de mesa', 'Grande', 'Estridente entelequia para los más exigentes'),
(826, 'Vëkbö', 'mueble', 'Pequeño', 'Imprescindible invención para cualquiera'),
(827, 'Kozo', 'herramienta', 'Grande', 'Estridente cacharro para sacarte de un aprieto'),
(828, 'Qö-në', 'decoración de pared', 'Pequeño', 'Útil cacharro de una calidad mediocre'),
(829, 'Kële', 'decoración de mesa', 'Pequeño', 'Eficaz objeto de la mayor calidad'),
(830, 'Të-wä', 'mueble', 'Mediano', 'Estridente objeto de la mayor calidad'),
(831, 'Nainasö', 'herramienta', 'Grande', 'Estridente cosa para cualquiera'),
(832, 'Uuuzetaba', 'decoración de mesa', 'Grande', 'Eficaz cosa de la mayor calidad'),
(833, 'Xëde', 'herramienta', 'Mediano', 'Adorableentelequia de una calidad mediocre'),
(834, 'Lämaömeorj', 'mueble', 'Mediano', 'Útil objeto de la mayor calidad'),
(835, 'Fifo-poqöuoe', 'mueble', 'Grande', 'Eficaz entelequia para los más exigentes'),
(836, 'Su-reiejeqä', 'mueble', 'Mediano', 'Imprescindible invención para cualquiera'),
(837, 'Fa-xöfavul', 'mueble', 'Mediano', 'Útil invención de una calidad mediocre'),
(838, 'Iec-ue', 'herramienta', 'Grande', 'Estridente objeto para cualquiera'),
(839, 'Gäoëopuhë', 'mueble', 'Mediano', 'Útil invención para el día a día'),
(840, 'Uöië', 'herramienta', 'Grande', 'Adorableinvención para sacarte de un aprieto'),
(841, 'Väz-söjäaönj', 'mueble', 'Pequeño', 'Imprescindible entelequia para el día a día'),
(842, 'Mo-cädi', 'decoración de mesa', 'Mediano', 'Estridente objeto para cualquiera'),
(843, 'Jaiuya', 'mueble', 'Pequeño', 'Eficaz cosa para los más exigentes'),
(844, 'Qu-xi', 'herramienta', 'Pequeño', 'Adorableentelequia de buena relación calidad precio'),
(845, 'Taai', 'mueble', 'Grande', 'Útil objeto para cualquiera'),
(846, 'Eöieqä', 'herramienta', 'Mediano', 'Adorableinvención de una calidad mediocre'),
(847, 'Qönve', 'herramienta', 'Mediano', 'Imprescindible objeto de buena relación calidad precio'),
(848, 'Aä-pëvri', 'herramienta', 'Pequeño', 'Imprescindible invención de primera necesidad'),
(849, 'Kacooo', 'mueble', 'Mediano', 'Estridente objeto para los más exigentes'),
(850, 'Wëji', 'herramienta', 'Grande', 'Eficaz objeto de la mayor calidad'),
(851, 'Hi-gu', 'herramienta', 'Grande', 'Útil cosa de una calidad mediocre'),
(852, 'Juui', 'herramienta', 'Grande', 'Útil entelequia de primera necesidad'),
(853, 'Juw-zesuai', 'herramienta', 'Mediano', 'Eficaz entelequia de primera necesidad'),
(854, 'Yezey', 'mueble', 'Mediano', 'Estridente invención de primera necesidad'),
(855, 'Pöri', 'herramienta', 'Mediano', 'Útil invención para cualquiera'),
(856, 'Eömse', 'herramienta', 'Grande', 'Imprescindible cosa para el día a día'),
(857, 'Tuwö', 'herramienta', 'Mediano', 'Adorableentelequia para el día a día'),
(858, 'Moq-eito', 'mueble', 'Mediano', 'Imprescindible invención de buena relación calidad precio'),
(859, 'Oäsozea', 'mueble', 'Pequeño', 'Imprescindible objeto de buena relación calidad precio'),
(860, 'Baku', 'herramienta', 'Grande', 'Imprescindible cosa para sacarte de un aprieto'),
(861, 'Eukaalö', 'mueble', 'Mediano', 'Útil invención de buena relación calidad precio'),
(862, 'Nure', 'mueble', 'Grande', 'Eficaz objeto para el día a día'),
(863, 'Pieë', 'decoración de mesa', 'Grande', 'Útil objeto para cualquiera'),
(864, 'Nazad', 'mueble', 'Mediano', 'Eficaz cosa de una calidad mediocre'),
(865, 'Jä-gödoso', 'herramienta', 'Mediano', 'Eficaz invención para el día a día'),
(866, 'Jëxo', 'herramienta', 'Pequeño', 'Estridente invención para cualquiera'),
(867, 'Vövo', 'decoración de mesa', 'Pequeño', 'Eficaz objeto de buena relación calidad precio'),
(868, 'Wö-zo', 'decoración de mesa', 'Grande', 'Imprescindible entelequia para el día a día'),
(869, 'Oo-eitil', 'herramienta', 'Pequeño', 'Eficaz cosa de primera necesidad'),
(870, 'Yihofegö', 'herramienta', 'Grande', 'Adorableobjeto para sacarte de un aprieto'),
(871, 'Ioqë', 'decoración de pared', 'Grande', 'Imprescindible entelequia para cualquiera'),
(872, 'Soka', 'herramienta', 'Pequeño', 'Estridente invención para el día a día'),
(873, 'Souqa', 'decoración de mesa', 'Grande', 'Útil objeto para el día a día'),
(874, 'Uënä', 'herramienta', 'Grande', 'Adorablecacharro de una calidad mediocre'),
(875, 'Me-ou', 'decoración de pared', 'Pequeño', 'Útil objeto para los más exigentes'),
(876, 'Cävao', 'herramienta', 'Mediano', 'Eficaz entelequia de una calidad mediocre'),
(877, 'Zëuögdiswë', 'mueble', 'Grande', 'Útil entelequia de primera necesidad'),
(878, 'Näauqbäcwi', 'herramienta', 'Grande', 'Adorableentelequia de la mayor calidad'),
(879, 'Uugpö', 'decoración de mesa', 'Grande', 'Eficaz cacharro para el día a día'),
(880, 'Eugökö', 'herramienta', 'Mediano', 'Estridente entelequia de buena relación calidad precio'),
(881, 'Bä-rucämorj', 'decoración de mesa', 'Mediano', 'Imprescindible entelequia de una calidad mediocre'),
(882, 'Keji', 'herramienta', 'Grande', 'Eficaz cosa para cualquiera'),
(883, 'Luzë', 'decoración de mesa', 'Mediano', 'Imprescindible objeto para sacarte de un aprieto'),
(884, 'Ga-zëjä', 'herramienta', 'Grande', 'Útil objeto para los más exigentes'),
(885, 'Vevoqejorj', 'mueble', 'Pequeño', 'Adorableinvención para los más exigentes'),
(886, 'Gö-dötu', 'mueble', 'Pequeño', 'Útil objeto para sacarte de un aprieto'),
(887, 'Nëaioeorj', 'decoración de pared', 'Pequeño', 'Eficaz cosa para los más exigentes'),
(888, 'Ao-rö', 'herramienta', 'Mediano', 'Imprescindible invención para el día a día'),
(889, 'Fomeödoië', 'decoración de mesa', 'Mediano', 'Adorableentelequia para el día a día'),
(890, 'Läna', 'decoración de mesa', 'Mediano', 'Eficaz cacharro de primera necesidad'),
(891, 'Iö-iexo', 'mueble', 'Grande', 'Estridente objeto para cualquiera'),
(892, 'Fimoyvibel', 'mueble', 'Pequeño', 'Imprescindible entelequia de buena relación calidad precio'),
(893, 'Vala', 'herramienta', 'Grande', 'Adorableentelequia de primera necesidad'),
(894, 'Gëv-bujo', 'mueble', 'Mediano', 'Estridente objeto para sacarte de un aprieto'),
(895, 'Mei-soxurj', 'herramienta', 'Pequeño', 'Útil cosa para los más exigentes'),
(896, 'Uuxömöc', 'herramienta', 'Mediano', 'Eficaz objeto para los más exigentes'),
(897, 'Ki-së', 'herramienta', 'Grande', 'Estridente entelequia de una calidad mediocre'),
(898, 'Jiyä', 'herramienta', 'Pequeño', 'Eficaz entelequia de una calidad mediocre'),
(899, 'Xuw-su', 'herramienta', 'Grande', 'Imprescindible entelequia para el día a día'),
(900, 'Fu-su', 'decoración de mesa', 'Pequeño', 'Imprescindible cacharro para cualquiera'),
(901, 'Bud-ki', 'decoración de mesa', 'Pequeño', 'Útil cosa de primera necesidad'),
(902, 'Yäva', 'decoración de mesa', 'Pequeño', 'Eficaz entelequia para cualquiera'),
(903, 'Pi-kö', 'herramienta', 'Pequeño', 'Útil cosa de una calidad mediocre'),
(904, 'Vipufuluw', 'decoración de pared', 'Mediano', 'Adorableinvención para sacarte de un aprieto'),
(905, 'Tamë-euy', 'mueble', 'Grande', 'Estridente entelequia para sacarte de un aprieto'),
(906, 'Noke', 'mueble', 'Pequeño', 'Imprescindible cosa de la mayor calidad'),
(907, 'Xii-aörorj', 'mueble', 'Grande', 'Eficaz cacharro para los más exigentes'),
(908, 'Iëvte', 'herramienta', 'Grande', 'Eficaz entelequia de primera necesidad'),
(909, 'Fäktäj', 'decoración de mesa', 'Mediano', 'Eficaz invención para sacarte de un aprieto'),
(910, 'Wëu-jevöao', 'herramienta', 'Pequeño', 'Estridente invención de la mayor calidad'),
(911, 'Xön-ha', 'decoración de pared', 'Pequeño', 'Útil cosa de primera necesidad'),
(912, 'Woo-jä-du', 'mueble', 'Grande', 'Eficaz invención de primera necesidad'),
(913, 'Jaoä', 'herramienta', 'Grande', 'Imprescindible objeto de la mayor calidad'),
(914, 'Kur-oe', 'decoración de mesa', 'Mediano', 'Útil cacharro para cualquiera'),
(915, 'Pooö', 'decoración de pared', 'Mediano', 'Estridente cosa de buena relación calidad precio'),
(916, 'Ha-hä', 'decoración de pared', 'Grande', 'Estridente entelequia para los más exigentes'),
(917, 'Xiii', 'herramienta', 'Grande', 'Útil objeto para el día a día'),
(918, 'Vuhuiö', 'herramienta', 'Grande', 'Estridente entelequia para los más exigentes'),
(919, 'Yezfuqë', 'herramienta', 'Grande', 'Estridente cosa para cualquiera'),
(920, 'Oegäuëjo', 'herramienta', 'Mediano', 'Adorablecosa para los más exigentes'),
(921, 'Wogëyö', 'mueble', 'Pequeño', 'Adorableobjeto de una calidad mediocre'),
(922, 'Eejëlo', 'herramienta', 'Mediano', 'Estridente invención de una calidad mediocre'),
(923, 'Fug-te', 'herramienta', 'Mediano', 'Estridente cosa para los más exigentes'),
(924, 'Uatioua', 'decoración de mesa', 'Grande', 'Estridente entelequia de la mayor calidad'),
(925, 'Oemë', 'herramienta', 'Mediano', 'Eficaz invención para el día a día'),
(926, 'Mezeö', 'decoración de mesa', 'Mediano', 'Eficaz invención para sacarte de un aprieto'),
(927, 'Iodë', 'mueble', 'Mediano', 'Eficaz cacharro de buena relación calidad precio'),
(928, 'Luvö', 'mueble', 'Grande', 'Imprescindible entelequia de la mayor calidad'),
(929, 'Ne-keza', 'mueble', 'Pequeño', 'Útil entelequia de una calidad mediocre'),
(930, 'Woh-vä', 'decoración de pared', 'Grande', 'Útil invención para el día a día'),
(931, 'Sapa', 'herramienta', 'Pequeño', 'Estridente invención de la mayor calidad'),
(932, 'Oö-qijuj', 'decoración de mesa', 'Pequeño', 'Útil cacharro para sacarte de un aprieto'),
(933, 'Uimiqëx', 'herramienta', 'Pequeño', 'Útil objeto de una calidad mediocre'),
(934, 'Zegu', 'herramienta', 'Pequeño', 'Eficaz cosa para sacarte de un aprieto'),
(935, 'Luzxioër', 'decoración de mesa', 'Pequeño', 'Estridente invención para cualquiera'),
(936, 'Uedö', 'mueble', 'Pequeño', 'Estridente objeto para sacarte de un aprieto'),
(937, 'Sëw-zëca', 'herramienta', 'Grande', 'Adorablecacharro para los más exigentes'),
(938, 'Läaoj', 'herramienta', 'Mediano', 'Imprescindible invención para el día a día'),
(939, 'Böt-sëgaoöjj', 'herramienta', 'Mediano', 'Adorableentelequia de la mayor calidad'),
(940, 'Heka', 'mueble', 'Pequeño', 'Útil entelequia de buena relación calidad precio'),
(941, 'Kemëfo-aa', 'mueble', 'Grande', 'Adorableinvención de una calidad mediocre'),
(942, 'Qet-säyouy', 'herramienta', 'Pequeño', 'Útil cosa de buena relación calidad precio'),
(943, 'Läma', 'mueble', 'Pequeño', 'Adorablecosa para sacarte de un aprieto'),
(944, 'Jete', 'decoración de mesa', 'Pequeño', 'Imprescindible invención para los más exigentes'),
(945, 'Eëoe', 'herramienta', 'Pequeño', 'Estridente objeto de buena relación calidad precio'),
(946, 'Qoeöxui', 'herramienta', 'Grande', 'Adorableobjeto de la mayor calidad'),
(947, 'Qäl-läbä', 'decoración de pared', 'Grande', 'Estridente entelequia de primera necesidad'),
(948, 'Nära-hëzhe', 'mueble', 'Pequeño', 'Eficaz objeto de una calidad mediocre'),
(949, 'Be-uujë', 'decoración de pared', 'Pequeño', 'Adorableobjeto para sacarte de un aprieto'),
(950, 'Pëh-nupotij', 'mueble', 'Pequeño', 'Estridente entelequia para los más exigentes'),
(951, 'Kuw-ho', 'decoración de mesa', 'Pequeño', 'Eficaz cosa para sacarte de un aprieto'),
(952, 'Boqicä', 'decoración de mesa', 'Mediano', 'Útil entelequia de la mayor calidad'),
(953, 'Cooë', 'herramienta', 'Grande', 'Eficaz invención de la mayor calidad'),
(954, 'Yuna', 'mueble', 'Pequeño', 'Eficaz invención para el día a día'),
(955, 'Ui-jë', 'decoración de mesa', 'Mediano', 'Imprescindible objeto de una calidad mediocre'),
(956, 'Iahe', 'herramienta', 'Grande', 'Adorableobjeto para cualquiera'),
(957, 'Säeö', 'decoración de mesa', 'Mediano', 'Estridente invención para los más exigentes'),
(958, 'Döh-iafëm', 'decoración de mesa', 'Grande', 'Imprescindible invención para los más exigentes'),
(959, 'Duku', 'herramienta', 'Grande', 'Estridente objeto de la mayor calidad'),
(960, 'Köuër', 'herramienta', 'Pequeño', 'Adorablecosa de primera necesidad'),
(961, 'Wero', 'herramienta', 'Grande', 'Útil cosa para cualquiera'),
(962, 'La-tëc', 'herramienta', 'Pequeño', 'Estridente cacharro de buena relación calidad precio'),
(963, 'Yöhehä', 'herramienta', 'Mediano', 'Adorablecosa para el día a día'),
(964, 'Xi-hä', 'herramienta', 'Mediano', 'Imprescindible cosa para sacarte de un aprieto'),
(965, 'Kantowuyj', 'herramienta', 'Grande', 'Eficaz objeto para cualquiera'),
(966, 'Wëiö', 'mueble', 'Pequeño', 'Adorablecosa para sacarte de un aprieto'),
(967, 'Kayöjko', 'mueble', 'Mediano', 'Útil cacharro para los más exigentes'),
(968, 'Xëjunë', 'herramienta', 'Grande', 'Imprescindible entelequia de buena relación calidad precio'),
(969, 'Nuoi', 'mueble', 'Grande', 'Eficaz cacharro de buena relación calidad precio'),
(970, 'Come', 'decoración de mesa', 'Mediano', 'Útil cacharro para el día a día'),
(971, 'Da-aö', 'herramienta', 'Mediano', 'Adorableentelequia para cualquiera'),
(972, 'Zë-se', 'herramienta', 'Grande', 'Útil entelequia de buena relación calidad precio'),
(973, 'Bibëyö', 'mueble', 'Grande', 'Estridente cosa de la mayor calidad'),
(974, 'Aid-dägäi', 'herramienta', 'Grande', 'Imprescindible invención para el día a día'),
(975, 'Yä-eö', 'decoración de pared', 'Grande', 'Estridente cosa para los más exigentes'),
(976, 'Ai-eafotu', 'decoración de mesa', 'Grande', 'Adorableobjeto de una calidad mediocre'),
(977, 'Dieo', 'decoración de mesa', 'Mediano', 'Útil entelequia de la mayor calidad'),
(978, 'Tano', 'herramienta', 'Mediano', 'Estridente objeto para sacarte de un aprieto'),
(979, 'Qi-au', 'decoración de pared', 'Pequeño', 'Útil cacharro de buena relación calidad precio'),
(980, 'Re-oo', 'herramienta', 'Mediano', 'Adorableinvención para sacarte de un aprieto'),
(981, 'Yibhölä', 'herramienta', 'Mediano', 'Útil cosa para los más exigentes'),
(982, 'Nezuso-teq', 'herramienta', 'Pequeño', 'Adorableobjeto para sacarte de un aprieto'),
(983, 'Näzcewibë-wuorj', 'herramienta', 'Mediano', 'Estridente objeto de primera necesidad'),
(984, 'Be-pe', 'mueble', 'Grande', 'Útil cosa para el día a día'),
(985, 'Ku-vëeu', 'mueble', 'Mediano', 'Imprescindible invención de la mayor calidad'),
(986, 'Gö-ru', 'herramienta', 'Grande', 'Eficaz entelequia de la mayor calidad'),
(987, 'Daief', 'decoración de mesa', 'Grande', 'Imprescindible objeto para sacarte de un aprieto'),
(988, 'Iëfö', 'herramienta', 'Mediano', 'Estridente entelequia de una calidad mediocre'),
(989, 'Ronuö', 'herramienta', 'Grande', 'Estridente entelequia para cualquiera'),
(990, 'Aäia', 'herramienta', 'Grande', 'Útil objeto de una calidad mediocre'),
(991, 'Fegö', 'herramienta', 'Pequeño', 'Imprescindible entelequia de buena relación calidad precio'),
(992, 'Qit-jub', 'decoración de mesa', 'Mediano', 'Adorablecosa para sacarte de un aprieto'),
(993, 'Xöf-tohu', 'herramienta', 'Grande', 'Estridente entelequia de una calidad mediocre'),
(994, 'Keaa', 'herramienta', 'Pequeño', 'Adorableentelequia de la mayor calidad'),
(995, 'Qijoorj', 'decoración de mesa', 'Grande', 'Eficaz invención para los más exigentes'),
(996, 'Goluru', 'mueble', 'Grande', 'Útil cacharro para los más exigentes'),
(997, 'Rumë', 'mueble', 'Mediano', 'Adorablecosa para cualquiera'),
(998, 'Juletu', 'decoración de mesa', 'Pequeño', 'Imprescindible invención para sacarte de un aprieto'),
(999, 'Në-no', 'decoración de mesa', 'Pequeño', 'Adorableobjeto de primera necesidad'),
(1000, 'Mäca', 'herramienta', 'Grande', 'Estridente objeto para el día a día');


--
-- Inserting data into table sombrero
--
INSERT INTO sombrero(ID, id_ropa, peinado) VALUES
(1, 61, 0),
(2, 62, 1),
(3, 63, 1),
(4, 64, 1),
(5, 65, 0),
(6, 66, 1),
(7, 67, 1),
(8, 68, 1),
(9, 69, 0),
(10, 70, 0),
(11, 71, 1),
(12, 72, 1),
(13, 73, 0),
(14, 74, 0),
(15, 75, 0),
(16, 76, 1),
(17, 77, 0),
(18, 78, 0),
(19, 79, 0),
(20, 80, 1);

--
-- Inserting data into table superior
--
INSERT INTO superior(ID, id_ropa, tipo) VALUES
(1, 81, 'femenina'),
(2, 82, 'masculina'),
(3, 83, 'femenina'),
(4, 84, 'unisex'),
(5, 85, 'unisex'),
(6, 86, 'masculina'),
(7, 87, 'femenina'),
(8, 88, 'femenina'),
(9, 89, 'femenina'),
(10, 90, 'unisex'),
(11, 91, 'unisex'),
(12, 92, 'femenina'),
(13, 93, 'unisex'),
(14, 94, 'masculina'),
(15, 95, 'unisex'),
(16, 96, 'unisex'),
(17, 97, 'unisex'),
(18, 98, 'femenina'),
(19, 99, 'femenina'),
(20, 100, 'masculina'),
(21, 101, 'unisex'),
(22, 102, 'unisex'),
(23, 103, 'unisex'),
(24, 104, 'unisex'),
(25, 105, 'femenina'),
(26, 106, 'unisex'),
(27, 107, 'masculina'),
(28, 108, 'masculina'),
(29, 109, 'femenina'),
(30, 110, 'unisex'),
(31, 111, 'unisex'),
(32, 112, 'unisex'),
(33, 113, 'unisex'),
(34, 114, 'unisex'),
(35, 115, 'femenina'),
(36, 116, 'unisex'),
(37, 117, 'femenina'),
(38, 118, 'femenina'),
(39, 119, 'masculina'),
(40, 120, 'unisex');



--
-- Inserting data into table calzado
--
INSERT INTO calzado(ID, id_ropa, terreno, color) VALUES
(1, 41, 'Roca', 'Waterspout'),
(2, 42, 'Ciudad', 'Fuchsia'),
(3, 43, 'Mojado', 'BrilliantRose'),
(4, 44, 'Roca', 'PinkSherbet'),
(5, 45, 'Roca', 'DavyGrey'),
(6, 46, 'Mojado', 'CoralRed'),
(7, 47, 'Ciudad', 'BrinkPink'),
(8, 48, 'Playa', 'HollywoodCerise'),
(9, 49, 'Deporte', 'ZinnwalditeBrown'),
(10, 50, 'Mojado', 'OrangeYellow'),
(11, 51, 'Ciudad', 'FuchsiaPink'),
(12, 52, 'Deporte', 'Cordovan'),
(13, 53, 'Deporte', 'SpringGreen'),
(14, 54, 'Ciudad', 'PinkPearl'),
(15, 55, 'Deporte', 'DebianRed'),
(16, 56, 'Mojado', 'Bronze'),
(17, 57, 'Mojado', 'Corn'),
(18, 58, 'Ciudad', 'OrangePeel'),
(19, 59, 'Playa', 'Brown'),
(20, 60, 'Roca', 'LincolnGreen');



--
-- Inserting data into table avatar
--
INSERT INTO avatar(ID, id_superior, id_inferior, id_calzado, id_sombrero, nombre, modelo, color, expresion_facial) VALUES
(1, 39, 2, 14, 13, 'DarkSlateBlue', 'Coleta', 'ZinnwalditeBrown', 'contento'),
(2, 14, 13, NULL, 3, 'Sr. Trent', 'Flequi', 'BabyBlue', 'triste'),
(3, 26, 31, 1, 18, 'Ms. Marcelino', 'Rapao', 'TealGreen', 'pensativo'),
(4, 32, 11, 18, 2, 'Francis', 'Moñetes', 'LavenderPink', 'flipao'),
(5, 13, 32, 14, 20, 'Dr. TurquoiseBlue', 'Rapao', 'PowderBlue', 'pensativo'),
(6, 39, 9, NULL, 16, 'Arnold', 'Rapao', 'RoyalPurple', 'pensativo'),
(7, 1, 28, 3, 10, 'Sr. Qo', 'Coleta', 'TerraCotta', 'contento'),
(8, 10, 30, 9, 8, 'Mr. AliceBlue', 'Rapao', 'CaputMortuum', 'flipao'),
(9, 26, 23, 12, 9, 'Sure', 'Rapao', 'Cream', 'triste'),
(10, 16, 22, 20, 7, 'Milan', 'Rapao', 'BabyBlueEyes', 'pensativo'),
(11, 33, 38, 8, 9, 'Dr. Ta', 'Coleta', 'OutrageousOrange', 'pasota'),
(12, 10, 21, 4, 3, 'Ms. Gold', 'Flequi', 'Cardinal', 'contento'),
(13, 39, 4, 1, 7, 'Dr. Jeffrey', 'Rizos', 'Ruby', 'contento'),
(14, 4, 16, 17, NULL, 'Sra. Adam', 'Flequi', 'MediumTaupe', 'flipao'),
(15, 24, 1, 19, 20, 'Dr. Adolph', 'Flequi', 'DogwoodRose', 'flipao'),
(16, 25, 17, 13, 9, 'Buff', 'Rizos', 'Crimson', 'pensativo'),
(17, 30, 30, 6, 2, 'Alonso', 'Rapao', 'Thistle', 'contento'),
(18, 16, 2, 8, 14, 'Mr. LightPink', 'Flequi', 'PrincetonOrange', 'pensativo'),
(19, 6, 38, 3, 19, 'Dr. Foster', 'Flequi', 'OxfordBlue', 'flipao'),
(20, 16, 31, 13, NULL, 'Curtis', 'Rapao', 'DollarBill', 'flipao'),
(21, 24, 14, 8, 14, 'Zeke', 'Rapao', 'PrussianBlue', 'flipao'),
(22, 29, 14, 2, 10, 'Maddie', 'Coleta', 'CrimsonRed', 'triste'),
(23, 23, 6, 16, 3, 'Dr. Dominique', 'Mohawk', 'Ruddy', 'triste'),
(24, 30, 38, 17, 12, 'Alexander', 'Coleta', 'LavenderPurple', 'contento'),
(25, 40, 26, 13, 15, 'FloralWhite', 'Coleta', 'PacificBlue', 'flipao'),
(26, 28, 38, 16, 14, 'Mr. Ai', 'Coleta', 'PsychedelicPurple', 'triste'),
(27, 14, 11, 11, 12, 'Travis', 'Flequi', 'Drab', 'flipao'),
(28, 29, 22, 7, 20, 'Blaine', 'Coleta', 'BabyPink', 'contento'),
(29, 23, 10, 17, 4, 'Eva', 'Coleta', 'CrimsonGlory', 'flipao'),
(30, 17, 38, 2, 6, 'Lil Camel', 'Rapao', 'DukeBlue', 'flipao'),
(31, 3, 33, 15, 5, 'Arden', 'Flequi', 'Cyan', 'pensativo'),
(32, 10, 14, 10, 6, 'Amaranth', 'Coleta', 'MediumTealBlue', 'flipao'),
(33, 5, 18, 3, 8, 'Sr. RuddyPink', 'Flequi', 'LavenderRose', 'flipao'),
(34, 16, 32, 5, 14, 'Sr. Uiqu', 'Rizos', 'CaribbeanGreen', 'pasota'),
(35, 31, 29, 14, 20, 'Ms. BrandeisBlue', 'Coleta', 'MediumTurquoise', 'flipao'),
(36, 10, 15, 16, 12, 'MediumPurple', 'Flequi', 'PakistanGreen', 'flipao'),
(37, 35, 14, 9, 1, 'Dr. Marcellus', 'Coleta', 'Puce', 'flipao'),
(38, 22, 2, 19, 11, 'Fandango', 'Flequi', 'EarthYellow', 'pasota'),
(39, 34, 29, 8, 3, 'Ms. Yo', 'Flequi', 'BallBlue', 'pensativo'),
(40, 3, 6, 6, 1, 'Mr. Zuqi', 'Coleta', 'ThulianPink', 'pensativo'),
(41, 33, 22, 18, 15, 'BleuDeFrance', 'Cresta', 'Daffodil', 'contento'),
(42, 19, 30, 17, 20, 'Sra. Coral', 'Coleta', 'Ecru', 'flipao'),
(43, 2, 24, 13, 3, 'Pa', 'Rapao', 'LawnGreen', 'flipao'),
(44, 12, 11, 10, 14, 'Dr. Freda', 'Cresta', 'Dandelion', 'contento'),
(45, 9, 37, 5, 14, 'Sra. Willian', 'Flequi', 'Carmine', 'pensativo'),
(46, 34, 2, 18, 5, 'Glaucous', 'Rapao', 'MediumVioletRed', 'flipao'),
(47, 22, 5, 9, 4, 'Tyler', 'Coleta', 'RuddyBrown', 'flipao'),
(48, 16, 16, 11, 6, 'Abe', 'Rapao', 'Lemon', 'pensativo'),
(49, 2, 16, 17, 3, 'Ms. Anika', 'Mohawk', 'PalatinateBlue', 'contento'),
(50, 39, 39, 12, 4, 'Dr. Denese', 'Rapao', 'Melon', 'contento'),
(51, 23, 32, 17, 6, 'FuchsiaPink', 'Rapao', 'Pumpkin', 'pensativo'),
(52, 2, 28, 15, 18, 'Sr. Au', 'Flequi', 'BananaMania', 'triste'),
(53, 1, 3, 5, 11, 'Kayla', 'Mohawk', 'TickleMePink', 'contento'),
(54, 3, 14, 5, 3, 'LightKhaki', 'Coleta', 'PalatinatePurple', 'flipao'),
(55, 15, 12, 17, 18, 'Mr. Adrianne', 'Rizos', 'CarminePink', 'contento'),
(56, 20, 33, 5, 10, 'Mr. Alex', 'Rapao', 'Purple', 'triste'),
(57, 15, 5, 11, 9, 'Vivienne', 'Flequi', 'Eggplant', 'flipao'),
(58, 11, 24, 1, 18, 'Sr. DimGray', 'Coleta', 'BananaYellow', 'triste'),
(59, 27, 27, 20, 17, 'Adriene', 'Flequi', 'RuddyPink', 'flipao'),
(60, 18, 13, 4, 6, 'CadmiumOrange', 'Flequi', 'PaleAqua', 'pensativo'),
(61, 1, 7, 5, 11, 'Iu', 'Mohawk', 'DarkBlue', 'pensativo'),
(62, 16, 6, 19, 20, 'Adolph', 'Rapao', 'LemonYellow', 'pasota'),
(63, 9, 28, 16, 8, 'Sra. PaleCornflowerBlue', 'Coleta', 'CarmineRed', 'triste'),
(64, 9, 33, 1, 1, 'Dr. MountbattenPink', 'Rapao', 'MidnightBlue', 'contento'),
(65, 1, 11, 2, 4, 'Ms. Zelda', 'Flequi', 'LemonChiffon', 'triste'),
(66, 8, 27, 15, 15, 'RosePink', 'Moñetes', 'TiffanyBlue', 'contento'),
(67, 15, 6, 10, 17, 'Alphonso', 'Coleta', 'Eggshell', 'pensativo'),
(68, 29, 30, 19, 20, 'Tanesha', 'Flequi', 'Rufous', 'contento'),
(69, 37, 38, 2, 11, 'Lula', 'Rizos', 'PurpleHeart', 'flipao'),
(70, 13, 28, 3, 14, 'Lil FernGreen', 'Coleta', 'MidnightGreen', 'flipao'),
(71, 13, 22, 4, 5, 'Ms. Vance', 'Coleta', 'BattleshipGrey', 'flipao'),
(72, 37, 15, 5, 20, 'Ms. RichCarmine', 'Coleta', 'LemonLime', 'contento'),
(73, 34, 30, 11, 16, 'Uojeeu', 'Coleta', 'CarnationPink', 'triste'),
(74, 12, 13, 19, 17, 'Lil Cindi', 'Flequi', 'PaleBlue', 'triste'),
(75, 30, 29, 9, 12, 'Sr. Rococi', 'Rizos', 'TigerEye', 'pensativo'),
(76, 31, 24, 14, 12, 'Aa', 'Cresta', 'Russet', 'flipao'),
(77, 11, 18, 2, 13, 'Viau', 'Coleta', 'MikadoYellow', 'pensativo'),
(78, 7, 28, 17, 2, 'Mickey', 'Flequi', 'PurplePizzazz', 'flipao'),
(79, 20, 20, 9, 17, 'Sr. Qado', 'Rapao', 'PaleBrown', 'flipao'),
(80, 24, 6, 3, 8, 'Oiquao', 'Coleta', 'Bazaar', 'triste'),
(81, 12, 36, 3, 11, 'Sra. David', 'Flequi', 'DarkBrown', 'pensativo'),
(82, 34, 24, 6, 13, 'AntiqueBrass', 'Flequi', 'EgyptianBlue', 'contento'),
(83, 23, 19, 15, 19, 'Ms. Inchworm', 'Cresta', 'LightCrimson', 'pasota'),
(84, 39, 38, 17, 8, 'Ailene', 'Cresta', 'DarkByzantium', 'pensativo'),
(85, 4, 11, 17, 8, 'Xo', 'Flequi', 'Mint', 'contento'),
(86, 8, 31, 14, 16, 'Mr. Abe', 'Coleta', 'PurpleTaupe', 'contento'),
(87, 8, 32, 4, 20, 'Jeff', 'Rapao', 'Carnelian', 'contento'),
(88, 4, 26, 1, 4, 'Ms. Curtis', 'Coleta', 'PaleCarmine', 'pasota'),
(89, 4, 2, 17, 3, 'Sr. TiffanyBlue', 'Flequi', 'BeauBlue', 'pensativo'),
(90, 13, 38, 3, 12, 'Dr. De', 'Rapao', 'ElectricBlue', 'flipao'),
(91, 34, 8, 13, 10, 'Ms. AndroidGreen', 'Coleta', 'CarolinaBlue', 'flipao'),
(92, 24, 17, 9, 10, 'Sipu', 'Rapao', 'DarkCandyAppleRed', 'flipao'),
(93, 19, 31, 18, 18, 'Lil Van', 'Flequi', 'ElectricCrimson', 'triste'),
(94, 2, 16, 10, 20, 'Lil Signe', 'Flequi', 'Beaver', 'pasota'),
(95, 14, 12, 17, 15, 'Mr. Deja', 'Flequi', 'DarkCerulean', 'flipao'),
(96, 16, 22, 1, 7, 'PastelPurple', 'Flequi', 'ElectricCyan', 'contento'),
(97, 35, 32, 12, 17, 'BrightLavender', 'Rapao', 'Rackley', 'flipao'),
(98, 3, 33, 4, 14, 'Mr. Vebebo', 'Moñetes', 'CarrotOrange', 'pensativo'),
(99, 35, 33, 11, 1, 'Sra. Myrtie', 'Coleta', 'LightThulianPink', 'contento'),
(100, 34, 4, 14, 4, 'AirForceBlue', 'Coleta', 'PaleCerulean', 'triste'),
(101, 21, 11, 9, 3, 'Lil Tandy', 'Coleta', 'Timberwolf', 'triste'),
(102, 19, 20, 1, 18, 'Ms. AliceBlue', 'Cresta', 'DarkChestnut', 'contento'),
(103, 39, 35, 15, 3, 'Lil Garth', 'Coleta', 'RadicalRed', 'pensativo'),
(104, 36, 22, 20, 14, 'Nia', 'Rapao', 'ElectricGreen', 'contento'),
(105, 16, 14, 11, 5, 'Hyon', 'Flequi', 'PaleChestnut', 'pensativo'),
(106, 5, 21, 17, 1, 'Sra. Hehi', 'Coleta', 'Rust', 'contento'),
(107, 28, 37, 3, 17, 'BurntSienna', 'Flequi', 'DarkCoral', 'triste'),
(108, 38, 24, 3, 1, 'Elias', 'Rizos', 'ElectricIndigo', 'triste'),
(109, 17, 26, 10, 11, 'Mu', 'Flequi', 'MintCream', 'contento'),
(110, 27, 22, 5, 9, 'Ms. Adah', 'Cresta', 'DarkCyan', 'pensativo'),
(111, 25, 37, 4, 4, 'Sra. Dorinda', 'Cresta', 'Beige', 'flipao'),
(112, 16, 29, 6, 3, 'Sra. Taylor', 'Rapao', 'ElectricLavender', 'triste'),
(113, 11, 14, 13, 12, 'Dr. Sana', 'Flequi', 'LightApricot', 'triste'),
(114, 16, 6, 8, 10, 'Ms. PalePink', 'Rizos', 'MintGreen', 'flipao'),
(115, 31, 23, 16, 8, 'JungleGreen', 'Rapao', 'DarkElectricBlue', 'flipao'),
(116, 24, 6, 15, 4, 'Fidufa', 'Flequi', 'Celadon', 'contento'),
(117, 11, 25, 8, 18, 'BrightTurquoise', 'Rapao', 'Bisque', 'flipao'),
(118, 19, 21, 12, 2, 'Cole', 'Rapao', 'Celeste', 'pensativo'),
(119, 31, 3, 6, 10, 'Neoma', 'Rapao', 'Raspberry', 'flipao'),
(120, 2, 1, 17, 13, 'Lil Ie', 'Mohawk', 'LightBlue', 'flipao'),
(121, 4, 10, 12, 5, 'Sra. Maricruz', 'Flequi', 'Bistre', 'flipao'),
(122, 26, 3, 20, 10, 'Lil Adan', 'Coleta', 'TitaniumYellow', 'pensativo'),
(123, 27, 1, 11, 1, 'Lil Carroll', 'Coleta', 'SaddleBrown', 'flipao'),
(124, 1, 24, 7, 10, 'Lil MediumTurquoise', 'Flequi', 'ElectricLime', 'triste'),
(125, 5, 2, 20, 13, 'PalePink', 'Rapao', 'MistyRose', 'pasota'),
(126, 27, 30, 5, 15, 'Pi', 'Rapao', 'PaleCopper', 'pasota'),
(127, 13, 36, 6, 12, 'Damon', 'Coleta', 'LightBrown', 'triste'),
(128, 24, 36, 3, 15, 'Dr. LincolnGreen', 'Coleta', 'DarkGoldenrod', 'triste'),
(129, 22, 34, 5, 12, 'Ms. Mafalda', 'Coleta', 'Tomato', 'contento'),
(130, 31, 19, 2, 1, 'Sra. Silas', 'Coleta', 'CelestialBlue', 'contento'),
(131, 20, 21, 10, 4, 'Ouceii', 'Coleta', 'ElectricPurple', 'triste'),
(132, 35, 19, 3, 19, 'Lil Alicia', 'Rizos', 'RaspberryGlace', 'flipao'),
(133, 32, 34, 13, 15, 'Dr. TractorRed', 'Coleta', 'DarkGray', 'pensativo'),
(134, 20, 6, 20, 10, 'Mr. PhthaloBlue', 'Rapao', 'PaleCornflowerBlue', 'flipao'),
(135, 3, 35, 3, 18, 'Me', 'Coleta', 'ElectricUltramarine', 'flipao'),
(136, 3, 5, 16, 14, 'Mr. Lincoln', 'Coleta', 'SafetyOrange', 'triste'),
(137, 11, 8, 5, 13, 'Lil Carie', 'Rapao', 'Moccasin', 'flipao'),
(138, 5, 15, 18, 1, 'DeepSkyBlue', 'Flequi', 'RaspberryPink', 'flipao'),
(139, 23, 10, 9, 8, 'Ms. Dane', 'Coleta', 'PaleGold', 'flipao'),
(140, 38, 1, 19, 14, 'Mr. Oa', 'Rizos', 'Bittersweet', 'pasota'),
(141, 14, 7, 20, 1, 'Wezu', 'Flequi', 'Cerise', 'contento'),
(142, 4, 16, 16, 12, 'Dr. Qi', 'Rapao', 'DarkGreen', 'contento'),
(143, 39, 11, 18, 2, 'Fuyedi', 'Flequi', 'LightCarminePink', 'flipao'),
(144, 20, 39, 10, 13, 'Ms. Mandy', 'Coleta', 'Toolbox', 'triste'),
(145, 18, 29, 16, 10, 'Kelvin', 'Flequi', 'RaspberryRose', 'triste'),
(146, 3, 24, 1, 9, 'Yeio', 'Flequi', 'PaleGoldenrod', 'flipao'),
(147, 4, 11, 14, 5, 'Lil PastelOrange', 'Coleta', 'ModeBeige', 'pensativo'),
(148, 35, 3, 11, 6, 'Corie', 'Rapao', 'Saffron', 'contento'),
(149, 30, 22, 1, 17, 'Sr. Anderson', 'Rizos', 'ElectricViolet', 'pasota'),
(150, 28, 17, 14, 16, 'Bronze', 'Flequi', 'Black', 'pensativo'),
(151, 24, 31, 14, 5, 'Dr. Rima', 'Rapao', 'RawSienna', 'contento'),
(152, 10, 16, 18, 11, 'MediumTealBlue', 'Flequi', 'DarkJungleGreen', 'contento'),
(153, 36, 6, 19, 9, 'Jeanette', 'Rapao', 'Topaz', 'pensativo'),
(154, 33, 29, 13, 12, 'Uiuuro', 'Rapao', 'LightCoral', 'pensativo'),
(155, 13, 1, 20, 8, 'Ms. Meg', 'Mohawk', 'CerisePink', 'triste'),
(156, 10, 21, 15, 18, 'Abigail', 'Coleta', 'PaleGreen', 'pensativo'),
(157, 33, 9, 13, 17, 'AuroMetalSaurus', 'Coleta', 'ElectricYellow', 'flipao'),
(158, 8, 13, 11, 16, 'Uaxi', 'Flequi', 'MoonstoneBlue', 'contento'),
(159, 14, 35, 12, 17, 'Sr. SapGreen', 'Rizos', 'DarkKhaki', 'contento'),
(160, 12, 21, 1, 2, 'Sra. Andy', 'Flequi', 'BlanchedAlmond', 'pensativo'),
(161, 28, 33, 10, 14, 'Damien', 'Flequi', 'Emerald', 'triste'),
(162, 26, 27, 12, 13, 'Lil Mae', 'Coleta', 'RazzleDazzleRose', 'flipao'),
(163, 15, 15, 7, 7, 'Elliott', 'Coleta', 'SaintPatrickBlue', 'flipao'),
(164, 6, 36, 18, 4, 'Mr. Romeo', 'Mohawk', 'LightCornflowerBlue', 'pasota'),
(165, 12, 11, 10, 1, 'Fofi', 'Coleta', 'TractorRed', 'contento'),
(166, 35, 24, 12, 16, 'SmokeyTopaz', 'Mohawk', 'PaleLavender', 'flipao'),
(167, 5, 22, 6, 10, 'Su', 'Rapao', 'MossGreen', 'flipao'),
(168, 31, 6, 13, 7, 'Poai', 'Cresta', 'Cerulean', 'contento'),
(169, 1, 13, 14, 10, 'Mr. Damian', 'Flequi', 'Salmon', 'flipao'),
(170, 35, 8, 1, 2, 'Collin', 'Coleta', 'Razzmatazz', 'flipao'),
(171, 25, 12, 11, 18, 'Danille', 'Coleta', 'PaleMagenta', 'pensativo'),
(172, 8, 3, 10, 12, 'Valencia', 'Flequi', 'BleuDeFrance', 'contento'),
(173, 11, 3, 19, 2, 'Dr. Neville', 'Rapao', 'CeruleanBlue', 'flipao'),
(174, 13, 5, 9, 16, 'Lil Amber', 'Flequi', 'LightCyan', 'flipao'),
(175, 18, 24, 10, 4, 'Sra. Christin', 'Coleta', 'BlizzardBlue', 'contento'),
(176, 20, 24, 16, 8, 'Sra. Dino', 'Rapao', 'MountainMeadow', 'flipao'),
(177, 1, 3, 14, 9, 'Adrianne', 'Flequi', 'TrolleyGrey', 'flipao'),
(178, 11, 14, 20, 14, 'Dr. Chad', 'Rapao', 'Chamoisee', 'pensativo'),
(179, 20, 35, 2, 18, 'Boris', 'Rapao', 'Blond', 'triste'),
(180, 25, 13, 17, 18, 'Mr. DarkPowderBlue', 'Rapao', 'Champagne', 'contento'),
(181, 27, 8, 2, 12, 'Sr. Lilac', 'Coleta', 'DarkLava', 'contento'),
(182, 28, 39, 2, 12, 'Ms. Shamika', 'Moñetes', 'EtonBlue', 'flipao'),
(183, 32, 15, 14, 13, 'Sra. Brass', 'Flequi', 'Blue', 'pasota'),
(184, 31, 22, 9, 8, 'Sra. Rene', 'Coleta', 'DarkLavender', 'flipao'),
(185, 30, 36, 13, 10, 'Lil Darcey', 'Flequi', 'Fallow', 'flipao'),
(186, 31, 18, 14, 9, 'Penney', 'Rapao', 'LightFuchsiaPink', 'flipao'),
(187, 10, 4, 6, 17, 'Fouaea', 'Flequi', 'Red', 'contento'),
(188, 9, 21, 4, 11, 'Mr. Wi', 'Coleta', 'Charcoal', 'flipao'),
(189, 20, 6, 20, 7, 'Adalberto', 'Flequi', 'SalmonPink', 'pensativo'),
(190, 29, 14, 12, 1, 'Awilda', 'Rizos', 'DarkMagenta', 'pensativo'),
(191, 23, 2, 12, 7, 'Sra. Coreen', 'Rapao', 'BlueBell', 'pensativo'),
(192, 30, 5, 4, 15, 'Lore', 'Flequi', 'PalePink', 'pensativo'),
(193, 24, 5, 14, 11, 'Mr. Alfonso', 'Flequi', 'Chartreuse', 'contento'),
(194, 16, 6, 9, 7, 'Dr. Victor', 'Flequi', 'BlueGray', 'contento'),
(195, 16, 10, 19, 1, 'Dr. Ti', 'Flequi', 'FaluRed', 'flipao'),
(196, 30, 24, 10, 16, 'Alisha', 'Rapao', 'TropicalRainForest', 'contento'),
(197, 23, 10, 13, 12, 'Douglass', 'Mohawk', 'DarkMidnightBlue', 'contento'),
(198, 21, 31, 17, 8, 'Sra. Ea', 'Rapao', 'MountbattenPink', 'flipao'),
(199, 5, 27, 10, 9, 'Sr. VividCerise', 'Coleta', 'Famous', 'flipao'),
(200, 37, 23, 12, 13, 'Poaupe', 'Flequi', 'RedOrange', 'contento');

INSERT INTO avatar_casa (id_avatar,id_casa,fecha_entrada) VALUES (1,6,"2020-08-11"),(2,25,"2019-09-10"),(3,5,"2019-05-13"),(4,41,"2019-05-06"),(5,24,"2019-12-07"),(6,32,"2020-06-03"),(7,3,"2020-02-05"),(8,31,"2019-11-18"),(9,1,"2020-12-08"),(10,41,"2020-02-18");
INSERT INTO avatar_casa (id_avatar,id_casa,fecha_entrada) VALUES (11,29,"2020-12-19"),(12,25,"2019-09-21"),(13,47,"2019-09-25"),(14,41,"2021-01-01"),(15,36,"2019-06-16"),(16,23,"2020-12-02"),(17,12,"2020-04-08"),(18,20,"2020-06-20"),(19,26,"2019-10-25"),(20,23,"2021-01-10");
INSERT INTO avatar_casa (id_avatar,id_casa,fecha_entrada) VALUES (21,15,"2020-05-05"),(22,13,"2019-05-25"),(23,32,"2020-10-19"),(24,3,"2020-05-23"),(25,40,"2020-02-26"),(26,43,"2020-05-14"),(27,46,"2019-11-08"),(28,2,"2019-06-10"),(29,4,"2020-10-03"),(30,16,"2020-10-15");
INSERT INTO avatar_casa (id_avatar,id_casa,fecha_entrada) VALUES (31,12,"2021-01-20"),(32,8,"2021-01-25"),(33,44,"2020-06-04"),(34,35,"2019-06-19"),(35,1,"2019-08-05"),(36,35,"2020-10-13"),(37,12,"2019-12-03"),(38,42,"2021-01-25"),(39,10,"2020-01-27"),(40,21,"2019-12-05");
INSERT INTO avatar_casa (id_avatar,id_casa,fecha_entrada) VALUES (41,33,"2019-07-27"),(42,43,"2020-01-27"),(43,29,"2020-09-13"),(44,11,"2019-11-04"),(45,25,"2021-01-17"),(46,5,"2020-09-10"),(47,24,"2019-08-28"),(48,28,"2020-12-18"),(49,23,"2019-04-24"),(50,31,"2019-10-03");
INSERT INTO avatar_casa (id_avatar,id_casa,fecha_entrada) VALUES (51,29,"2020-03-05"),(52,42,"2020-05-04"),(53,35,"2020-08-11"),(54,13,"2020-06-15"),(55,9,"2019-11-27"),(56,26,"2020-02-08"),(57,34,"2019-07-01"),(58,48,"2020-09-10"),(59,26,"2019-07-13"),(60,40,"2019-10-07");
INSERT INTO avatar_casa (id_avatar,id_casa,fecha_entrada) VALUES (61,29,"2020-07-12"),(62,41,"2019-09-26"),(63,21,"2020-08-01"),(64,48,"2019-11-15"),(65,34,"2021-01-26"),(66,6,"2020-05-20"),(67,46,"2020-07-11"),(68,50,"2019-02-19"),(69,23,"2019-12-20"),(70,49,"2020-05-23");
INSERT INTO avatar_casa (id_avatar,id_casa,fecha_entrada) VALUES (71,8,"2021-01-08"),(72,36,"2020-08-10"),(73,27,"2019-08-30"),(74,26,"2020-08-18"),(75,20,"2019-08-27"),(76,9,"2020-03-13"),(77,41,"2019-06-07"),(78,11,"2019-03-09"),(79,26,"2019-04-27"),(80,9,"2019-05-22");
INSERT INTO avatar_casa (id_avatar,id_casa,fecha_entrada) VALUES (81,21,"2020-09-18"),(82,20,"2019-05-29"),(83,5,"2020-05-05"),(84,15,"2020-08-04"),(85,9,"2020-01-19"),(86,34,"2019-12-24"),(87,26,"2019-08-07"),(88,40,"2020-04-23"),(89,20,"2020-01-25"),(90,40,"2019-07-24");
INSERT INTO avatar_casa (id_avatar,id_casa,fecha_entrada) VALUES (91,10,"2020-04-02"),(92,12,"2020-03-22"),(93,24,"2020-04-27"),(94,49,"2020-02-17"),(95,26,"2020-02-18"),(96,37,"2020-01-02"),(97,31,"2020-11-07"),(98,45,"2020-02-25"),(99,4,"2020-06-05"),(100,44,"2020-07-20");

INSERT INTO avatar_casa (id_avatar,id_casa,fecha_entrada) VALUES (101,44,"2020-11-24"),(102,17,"2020-07-11"),(103,42,"2020-06-28"),(104,42,"2020-06-09"),(105,33,"2020-12-04"),(106,44,"2020-07-16"),(107,12,"2019-12-10"),(108,27,"2020-05-19"),(109,28,"2019-12-16"),(110,19,"2019-10-07");
INSERT INTO avatar_casa (id_avatar,id_casa,fecha_entrada) VALUES (111,9,"2020-05-02"),(112,38,"2020-10-28"),(113,24,"2020-04-30"),(114,41,"2020-03-06"),(115,22,"2020-06-14"),(116,39,"2020-10-03"),(117,44,"2019-09-12"),(118,48,"2020-10-07"),(119,32,"2019-09-30"),(120,8,"2020-06-05");
INSERT INTO avatar_casa (id_avatar,id_casa,fecha_entrada) VALUES (121,5,"2020-08-29"),(122,11,"2019-11-28"),(123,47,"2020-11-07"),(124,6,"2019-05-07"),(125,31,"2020-09-19"),(126,8,"2020-03-04"),(127,8,"2020-08-28"),(128,7,"2020-04-06"),(129,24,"2019-04-29"),(130,30,"2020-01-25");
INSERT INTO avatar_casa (id_avatar,id_casa,fecha_entrada) VALUES (131,34,"2020-10-20"),(132,21,"2020-02-07"),(133,33,"2019-05-05"),(134,35,"2021-01-08"),(135,24,"2020-03-27"),(136,49,"2020-05-20"),(137,48,"2020-12-25"),(138,48,"2020-12-08"),(139,32,"2020-08-30"),(140,25,"2020-12-07");
INSERT INTO avatar_casa (id_avatar,id_casa,fecha_entrada) VALUES (141,8,"2019-02-22"),(142,44,"2020-08-10"),(143,40,"2020-11-16"),(144,46,"2019-09-12"),(145,14,"2019-11-14"),(146,9,"2019-03-02"),(147,41,"2019-06-27"),(148,39,"2019-12-26"),(149,16,"2019-10-28"),(150,19,"2019-11-11");
INSERT INTO avatar_casa (id_avatar,id_casa,fecha_entrada) VALUES (151,44,"2020-12-24"),(152,43,"2019-02-18"),(153,39,"2021-01-05"),(154,27,"2020-04-20"),(155,1,"2019-11-06"),(156,37,"2019-12-04"),(157,8,"2020-02-08"),(158,38,"2020-12-26"),(159,8,"2019-09-17"),(160,20,"2020-01-13");
INSERT INTO avatar_casa (id_avatar,id_casa,fecha_entrada) VALUES (161,19,"2020-02-24"),(162,29,"2020-10-27"),(163,30,"2019-08-09"),(164,45,"2020-02-20"),(165,4,"2020-08-05"),(166,7,"2019-09-10"),(167,38,"2020-11-22"),(168,11,"2020-10-16"),(169,48,"2020-10-21"),(170,25,"2020-01-05");
INSERT INTO avatar_casa (id_avatar,id_casa,fecha_entrada) VALUES (171,10,"2019-04-04"),(172,8,"2020-07-18"),(173,29,"2019-11-19"),(174,46,"2020-01-08"),(175,5,"2020-10-24"),(176,10,"2020-01-03"),(177,9,"2020-03-24"),(178,32,"2020-12-17"),(179,27,"2020-10-19"),(180,50,"2019-07-31");
INSERT INTO avatar_casa (id_avatar,id_casa,fecha_entrada) VALUES (181,40,"2020-04-02"),(182,15,"2019-05-03"),(183,48,"2019-07-21"),(184,35,"2019-12-10"),(185,20,"2019-10-21"),(186,8,"2019-12-20"),(187,9,"2019-08-07"),(188,26,"2019-08-09"),(189,17,"2020-07-17"),(190,8,"2019-04-02");
INSERT INTO avatar_casa (id_avatar,id_casa,fecha_entrada) VALUES (191,17,"2019-06-21"),(192,10,"2020-09-07"),(193,9,"2020-09-20"),(194,5,"2020-09-05"),(195,16,"2020-03-06"),(196,35,"2019-02-15"),(197,17,"2020-06-09"),(198,45,"2019-03-05"),(199,41,"2019-11-12"),(200,32,"2020-01-15");

INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (179,667),(166,506),(25,183),(196,188),(139,995),(8,648),(16,921),(51,39),(84,65),(76,701);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (195,90),(80,622),(78,214),(183,971),(126,726),(97,972),(112,682),(181,737),(108,126),(13,48);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (119,755),(35,698),(180,97),(89,555),(103,929),(80,601),(13,636),(200,738),(98,323),(184,134);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (184,645),(95,936),(196,723),(165,649),(50,707),(161,951),(123,526),(147,220),(176,198),(88,691);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (57,842),(140,258),(197,187),(107,88),(158,101),(105,336),(141,351),(4,381),(127,933),(160,330);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (51,381),(144,435),(103,362),(174,1000),(57,259),(144,559),(44,750),(7,36),(192,75),(112,439);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (121,674),(9,861),(188,315),(93,68),(17,285),(84,251),(185,885),(94,136),(165,422),(190,303);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (123,818),(107,850),(182,249),(13,28),(116,225),(130,930),(91,243),(84,894),(22,382),(56,627);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (66,431),(192,390),(5,953),(42,22),(147,171),(176,376),(32,162),(186,92),(135,697),(70,994);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (129,571),(199,384),(165,61),(65,846),(186,490),(187,112),(5,817),(186,903),(41,48),(108,895);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (27,820),(76,429),(173,556),(28,429),(110,873),(197,494),(24,298),(28,692),(57,973),(96,457);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (167,450),(25,390),(33,650),(14,107),(27,721),(35,527),(181,508),(170,172),(139,279),(157,933);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (174,607),(197,243),(59,816),(66,728),(91,542),(134,511),(87,272),(102,373),(7,157),(145,456);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (157,48),(60,335),(127,813),(63,342),(22,719),(92,858),(173,838),(42,100),(43,771),(109,210);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (63,724),(74,959),(170,318),(153,47),(172,554),(23,818),(5,505),(38,936),(198,426),(195,718);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (150,519),(47,644),(87,118),(106,80),(131,612),(138,553),(148,251),(187,761),(172,150),(71,160);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (195,111),(2,407),(144,732),(96,729),(77,567),(137,347),(125,756),(84,332),(166,410),(82,657);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (119,461),(86,476),(65,797),(13,634),(49,229),(120,269),(182,549),(66,393),(200,557),(3,697);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (9,557),(38,48),(193,898),(26,693),(16,472),(185,556),(36,467),(116,556),(188,911),(128,483);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (17,885),(64,520),(12,907),(160,714),(31,306),(199,302),(3,653),(47,402),(31,926),(37,562);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (97,951),(97,741),(151,195),(84,41),(123,783),(117,440),(128,692),(172,939),(154,658),(176,259);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (71,571),(175,705),(13,267),(158,177),(142,54),(90,367),(171,488),(144,241),(37,507),(18,630);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (174,926),(53,886),(5,628),(177,497),(98,448),(160,678),(56,902),(146,501),(41,883),(100,369);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (85,260),(170,650),(31,242),(117,757),(120,503),(59,477),(18,952),(188,568),(148,180),(68,784);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (74,335),(34,345),(53,593),(53,389),(90,880),(3,703),(141,772),(21,543),(193,643),(89,599);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (97,8),(115,195),(32,585),(170,841),(81,733),(129,586),(163,366),(43,826),(139,886),(143,899);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (65,518),(82,68),(140,78),(171,668),(193,925),(125,158),(129,886),(2,378),(34,424),(193,206);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (186,558),(137,331),(8,908),(194,83),(142,421),(48,73),(179,681),(136,180),(158,313),(124,561);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (109,69),(135,646),(30,2),(197,834),(40,432),(95,814),(80,262),(11,878),(71,332),(119,788);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (61,877),(166,524),(81,349),(199,131),(143,810),(143,483),(99,10),(80,774),(14,793),(148,872);

INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (116,746),(5,602),(82,385),(116,769),(9,675),(38,403),(80,805),(58,521),(176,869),(105,287);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (71,729),(7,982),(145,323),(26,607),(94,752),(123,998),(196,79),(180,296),(97,6),(164,76);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (98,214),(144,675),(108,736),(121,543),(138,364),(33,630),(9,478),(126,131),(66,263),(68,806);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (48,785),(173,657),(52,421),(112,435),(157,314),(86,84),(32,408),(59,548),(7,464),(87,344);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (57,264),(85,376),(63,709),(43,363),(45,53),(21,363),(54,596),(19,902),(198,86),(27,235);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (139,268),(119,433),(164,721),(185,807),(3,502),(117,441),(73,848),(14,475),(113,273),(173,554);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (80,570),(24,200),(150,918),(156,562),(90,808),(26,559),(173,673),(152,680),(47,755),(31,914);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (164,90),(111,458),(77,258),(178,967),(186,982),(74,45),(45,510),(153,273),(33,304),(48,6);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (70,201),(126,672),(150,656),(88,455),(86,780),(176,828),(84,803),(114,49),(46,873),(41,787);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (49,860),(156,571),(188,112),(27,587),(96,325),(43,681),(111,509),(14,690),(13,405),(25,609);

INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (84,120),(127,201),(134,721),(86,936),(129,718),(103,543),(148,222),(85,770),(9,485),(148,178);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (112,119),(39,382),(168,63),(149,467),(136,853),(187,114),(51,424),(194,681),(1,890),(149,210);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (69,909),(185,601),(116,73),(108,297),(156,950),(44,829),(108,273),(137,169),(44,774),(177,416);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (58,780),(135,208),(54,907),(61,276),(148,305),(27,461),(28,6),(73,857),(64,564),(154,186);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (153,967),(45,970),(82,418),(183,411),(23,335),(133,822),(116,386),(179,14),(34,299),(40,621);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (101,289),(147,215),(136,613),(71,36),(155,895),(58,958),(92,328),(9,321),(83,852),(12,589);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (173,327),(193,699),(16,715),(81,642),(50,556),(5,497),(40,752),(105,727),(179,858),(148,548);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (27,931),(39,255),(108,578),(132,87),(33,176),(108,682),(56,748),(132,209),(19,322),(79,287);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (165,869),(109,577),(74,940),(195,458),(95,448),(191,119),(166,998),(7,621),(85,887),(19,15);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (192,400),(1,34),(118,269),(111,868),(31,755),(187,840),(126,883),(55,519),(174,653),(73,577);

INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (198,896),(131,714),(197,723),(71,43),(87,439),(50,219),(29,871),(100,421),(37,462),(106,269);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (164,154),(178,283),(128,893),(5,740),(58,646),(1,992),(34,111),(55,872),(95,213),(133,923);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (141,148),(150,489),(40,824),(8,612),(55,284),(157,646),(48,578),(61,813),(141,597),(122,196);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (158,780),(22,69),(94,17),(165,955),(195,348),(175,538),(132,940),(97,461),(58,456),(73,918);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (193,132),(147,344),(72,981),(81,185),(199,483),(90,135),(31,764),(165,857),(175,374),(56,383);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (36,688),(149,792),(89,890),(13,191),(16,30),(197,763),(59,633),(198,650),(111,616),(149,760);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (39,629),(144,488),(182,139),(113,395),(118,705),(22,472),(23,118),(92,927),(38,497),(64,74);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (13,963),(175,105),(50,179),(178,917),(14,631),(39,525),(128,777),(160,928),(38,332),(198,346);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (159,858),(142,569),(65,415),(25,595),(118,710),(71,133),(86,664),(142,294),(160,303),(117,393);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (109,86),(4,56),(25,4),(27,429),(154,423),(66,205),(197,196),(142,380),(45,593),(198,301);



INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (106,724),(144,455),(10,492),(175,989),(10,677),(16,149),(52,136),(18,57),(124,216),(152,2);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (114,666),(149,693),(159,536),(136,571),(180,992),(76,108),(120,782),(194,686),(129,373),(190,67);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (70,124),(170,123),(100,462),(105,274),(53,50),(62,217),(200,780),(138,762),(61,708),(95,153);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (194,933),(24,250),(33,108),(41,62),(65,649),(99,966),(60,625),(32,305),(56,17),(172,163);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (32,170),(190,246),(108,815),(80,787),(164,320),(13,460),(40,291),(184,623),(136,695),(179,120);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (46,959),(78,260),(124,595),(58,669),(83,20),(23,90),(171,733),(187,918),(16,87),(126,969);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (5,428),(3,316),(57,815),(49,646),(18,38),(107,365),(10,42),(154,447),(80,648),(125,678);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (32,51),(99,418),(180,809),(50,608),(167,228),(36,814),(121,823),(52,279),(36,952),(14,952);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (2,996),(117,571),(182,626),(164,281),(59,188),(48,147),(87,710),(190,746),(147,781),(104,543);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (147,898),(185,206),(140,562),(68,152),(30,888),(107,453),(86,448),(17,325),(70,916),(106,747);


INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (86,443),(133,859),(99,293),(191,176),(145,288),(34,152),(182,753),(59,668),(45,338),(168,208);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (21,489),(64,478),(91,467),(77,688),(61,295),(50,149),(137,462),(42,838),(81,787),(71,512);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (89,995),(22,75),(11,609),(184,785),(47,792),(55,388),(138,602),(69,806),(68,220),(77,471);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (145,726),(50,293),(92,700),(120,3),(140,920),(132,137),(195,496),(1,346),(33,114),(18,991);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (123,762),(85,89),(37,572),(100,37),(22,396),(109,655),(144,322),(30,662),(62,365),(147,273);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (57,120),(194,881),(29,557),(8,738),(133,915),(110,531),(35,271),(112,380),(115,955),(46,537);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (101,933),(23,675),(118,439),(106,632),(147,730),(52,974),(161,814),(190,111),(165,45),(78,641);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (197,349),(184,450),(182,607),(62,970),(102,85),(111,257),(101,536),(114,145),(63,796),(195,566);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (15,292),(131,174),(129,470),(177,862),(10,283),(164,302),(158,110),(120,443),(41,896),(19,606);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (183,882),(73,202),(137,790),(12,545),(147,356),(128,903),(161,578),(83,393),(190,718),(118,153);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (156,326),(101,76),(20,243),(70,421),(143,373),(69,262),(170,959),(119,267),(127,138),(187,295);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (39,550),(15,907),(42,328),(169,854),(113,828),(184,723),(139,324),(38,405),(70,55),(177,308);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (115,988),(121,295),(119,10),(187,719),(127,676),(68,847),(14,276),(171,944),(196,779),(117,999);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (19,417),(174,639),(96,175),(149,322),(22,849),(29,199),(129,25),(60,307),(13,56),(59,456);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (97,943),(182,225),(66,875),(46,991),(141,8),(174,124),(184,436),(116,836),(102,859),(183,885);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (21,22),(105,296),(186,121),(30,799),(117,499),(195,874),(16,462),(84,470),(154,836),(26,610);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (153,785),(35,936),(57,376),(192,951),(48,476),(103,496),(134,302),(167,317),(177,249),(193,621);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (162,401),(13,755),(37,345),(82,251),(81,454),(61,332),(71,770),(185,262),(90,127),(168,622);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (34,939),(184,56),(93,741),(35,102),(155,680),(18,597),(174,958),(67,430),(33,542),(108,987);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (137,556),(172,794),(168,781),(25,646),(95,602),(87,102),(135,60),(48,839),(94,961),(65,915);


INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (32,211),(200,924),(154,945),(17,895),(200,270),(56,800),(106,752),(43,677),(2,707),(88,160);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (83,434),(184,262),(187,197),(35,473),(37,54),(158,381),(172,668),(196,1),(1,470),(98,289);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (105,763),(30,386),(3,860),(131,176),(5,149),(196,104),(81,248),(10,828),(48,822),(169,415);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (63,812),(114,535),(42,594),(6,336),(5,367),(175,952),(72,860),(146,373),(71,648),(95,479);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (130,613),(102,915),(35,29),(195,109),(17,378),(31,572),(22,767),(88,161),(12,159),(182,763);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (197,886),(1,995),(74,665),(66,522),(92,474),(123,9),(110,566),(110,826),(69,602),(144,230);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (87,103),(113,740),(119,906),(5,885),(86,657),(24,756),(176,914),(38,102),(56,983),(72,538);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (101,795),(94,404),(156,493),(138,84),(70,628),(25,114),(183,758),(199,218),(149,268),(150,542);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (189,308),(188,235),(32,645),(13,928),(35,326),(80,313),(132,324),(11,110),(69,69),(114,834);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (46,666),(106,407),(194,696),(95,662),(39,365),(199,320),(197,386),(123,391),(199,526),(100,494);

INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (27,507),(118,559),(23,196),(26,292),(137,972),(172,560),(11,953),(51,914),(125,819),(72,250);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (197,476),(70,622),(42,756),(101,362),(98,346),(62,50),(25,790),(183,435),(26,921),(87,793);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (40,261),(156,421),(136,950),(178,123),(176,192),(40,60),(128,590),(70,912),(80,543),(21,902);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (114,919),(150,501),(91,750),(115,266),(36,82),(54,988),(127,600),(77,993),(84,737),(112,872);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (100,199),(166,257),(6,467),(28,519),(113,754),(7,690),(189,704),(123,633),(44,493),(87,135);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (29,427),(73,854),(131,413),(85,554),(159,380),(10,521),(176,752),(103,504),(108,760),(179,473);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (48,411),(23,600),(68,611),(100,904),(136,851),(181,302),(31,805),(168,197),(84,321),(23,697);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (128,569),(189,296),(117,324),(120,462),(23,876),(200,680),(87,598),(84,389),(50,650),(119,832);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (38,24),(115,786),(44,254),(96,438),(137,228),(115,644),(102,44),(167,918),(64,421),(181,272);
INSERT INTO `avatar_objeto` (`id_avatar`,`id_objeto`) VALUES (128,424),(1,860),(148,940),(141,922),(166,15),(115,728),(192,766),(117,271),(159,558),(148,130);



INSERT INTO avatar_relacion_avatar (id_avatar_ejerce,id_avatar_recibe,fecha,tipo) VALUES (10,172,"2020-08-19","adopta"),(81,41,"2020-07-29","adopta"),(92,88,"2020-10-08","matrimonio"),(171,120,"2019-02-23","adopta"),(42,57,"2020-02-20","matrimonio"),(89,16,"2019-05-24","matrimonio"),(111,181,"2020-09-17","matrimonio"),(37,191,"2019-05-02","matrimonio"),(171,44,"2020-01-02","adopta"),(87,77,"2019-06-01","matrimonio");
INSERT INTO avatar_relacion_avatar (id_avatar_ejerce,id_avatar_recibe,fecha,tipo) VALUES (71,197,"2019-11-13","matrimonio"),(79,50,"2020-12-18","matrimonio"),(69,19,"2020-06-08","adopta"),(189,113,"2019-08-01","adopta"),(154,160,"2019-02-13","matrimonio"),(79,140,"2019-12-08","matrimonio"),(58,57,"2019-02-28","matrimonio"),(39,110,"2020-03-19","adopta"),(189,90,"2019-08-08","matrimonio"),(85,65,"2020-10-05","adopta");
INSERT INTO avatar_relacion_avatar (id_avatar_ejerce,id_avatar_recibe,fecha,tipo) VALUES (189,27,"2019-02-23","matrimonio"),(84,50,"2020-03-27","matrimonio"),(99,53,"2019-05-02","adopta"),(191,126,"2019-04-08","adopta"),(75,157,"2020-04-30","adopta"),(99,61,"2020-07-23","adopta"),(7,136,"2020-08-15","matrimonio"),(186,103,"2019-11-22","adopta"),(78,140,"2020-07-18","matrimonio"),(190,3,"2020-01-04","matrimonio");
INSERT INTO avatar_relacion_avatar (id_avatar_ejerce,id_avatar_recibe,fecha,tipo) VALUES (185,88,"2020-05-01","adopta"),(34,135,"2019-12-19","adopta"),(146,21,"2020-08-07","adopta"),(30,184,"2019-03-08","adopta"),(192,121,"2019-05-01","adopta"),(173,40,"2019-06-04","adopta"),(78,36,"2019-09-13","matrimonio"),(114,132,"2020-07-20","adopta"),(116,91,"2019-09-20","matrimonio"),(151,70,"2019-07-27","adopta");
INSERT INTO avatar_relacion_avatar (id_avatar_ejerce,id_avatar_recibe,fecha,tipo) VALUES (116,152,"2019-11-19","adopta"),(67,15,"2020-07-16","adopta"),(147,71,"2019-06-10","adopta"),(86,128,"2019-11-27","adopta"),(1,102,"2019-07-01","matrimonio"),(64,137,"2020-06-11","adopta"),(184,183,"2019-07-01","matrimonio"),(45,43,"2019-03-01","adopta"),(152,176,"2020-03-25","adopta"),(199,29,"2020-03-21","matrimonio");
INSERT INTO avatar_relacion_avatar (id_avatar_ejerce,id_avatar_recibe,fecha,tipo) VALUES (175,143,"2019-10-08","adopta"),(29,115,"2020-12-11","adopta"),(192,130,"2019-10-06","adopta"),(192,103,"2019-07-26","matrimonio"),(20,71,"2020-07-31","adopta"),(131,52,"2019-06-21","adopta"),(194,5,"2020-11-16","matrimonio"),(128,4,"2020-08-30","adopta"),(159,9,"2019-07-16","matrimonio"),(123,41,"2020-06-02","adopta");
INSERT INTO avatar_relacion_avatar (id_avatar_ejerce,id_avatar_recibe,fecha,tipo) VALUES (44,176,"2019-06-27","adopta"),(18,95,"2019-04-02","matrimonio"),(195,127,"2020-02-03","adopta"),(124,102,"2019-02-09","matrimonio"),(145,127,"2021-01-04","adopta"),(103,73,"2019-04-03","matrimonio"),(95,28,"2020-11-10","matrimonio"),(65,154,"2020-07-27","matrimonio"),(193,13,"2020-02-12","adopta"),(39,147,"2019-08-29","adopta");
INSERT INTO avatar_relacion_avatar (id_avatar_ejerce,id_avatar_recibe,fecha,tipo) VALUES (15,77,"2020-08-29","adopta"),(63,92,"2020-12-19","adopta"),(121,120,"2020-01-30","matrimonio"),(128,63,"2019-12-22","adopta"),(159,69,"2020-03-11","adopta"),(112,87,"2020-06-20","adopta"),(57,155,"2021-01-16","adopta"),(65,1,"2019-09-29","adopta"),(176,14,"2021-01-11","matrimonio"),(135,154,"2019-07-02","adopta");
INSERT INTO avatar_relacion_avatar (id_avatar_ejerce,id_avatar_recibe,fecha,tipo) VALUES (12,62,"2020-08-16","matrimonio"),(144,55,"2020-02-12","adopta"),(97,97,"2019-12-25","adopta"),(40,125,"2019-04-11","adopta"),(103,197,"2020-11-13","matrimonio"),(62,144,"2019-02-28","matrimonio"),(85,109,"2020-02-14","adopta"),(63,132,"2021-01-26","adopta"),(186,31,"2020-11-12","adopta"),(148,12,"2019-12-22","matrimonio");
INSERT INTO avatar_relacion_avatar (id_avatar_ejerce,id_avatar_recibe,fecha,tipo) VALUES (15,83,"2020-12-11","matrimonio"),(114,102,"2020-06-06","matrimonio"),(163,189,"2020-12-03","matrimonio"),(110,56,"2020-01-11","adopta"),(184,75,"2020-03-13","adopta"),(198,155,"2019-05-15","matrimonio"),(26,119,"2019-03-20","adopta"),(158,31,"2020-10-11","matrimonio"),(50,142,"2019-03-31","matrimonio"),(95,175,"2020-04-19","adopta");

select * from habitacion;

INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (170,702,9,1,"S"),(152,562,3,8,"E"),(82,417,1,9,"S"),(214,201,7,8,"O"),(56,888,5,9,"O"),(234,989,5,10,"S"),(81,769,2,4,"N"),(151,32,1,10,"N"),(237,708,2,1,"N"),(92,656,7,8,"N");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (123,336,6,5,"N"),(100,295,8,7,"O"),(125,615,4,8,"E"),(98,626,5,4,"N"),(140,489,4,8,"O"),(234,754,4,5,"N"),(30,510,8,1,"E"),(232,687,3,4,"N"),(56,566,5,3,"O"),(170,156,4,5,"O");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (12,23,6,5,"E"),(157,907,5,5,"S"),(192,111,3,5,"E"),(125,505,6,6,"S"),(106,240,8,10,"S"),(249,266,1,6,"O"),(249,763,7,5,"S"),(147,332,9,1,"S"),(37,607,3,9,"E"),(162,234,3,3,"O");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (49,431,9,4,"O"),(245,906,5,5,"S"),(12,176,4,1,"N"),(102,952,5,7,"S"),(185,390,1,3,"O"),(128,941,7,2,"E"),(210,315,2,7,"O"),(110,750,5,7,"O"),(192,626,3,9,"S"),(98,445,4,1,"N");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (74,401,7,5,"O"),(42,829,7,9,"S"),(137,840,6,8,"E"),(207,102,5,6,"N"),(10,968,10,7,"N"),(93,960,1,2,"E"),(182,460,5,5,"E"),(181,15,10,5,"S"),(106,988,1,8,"E"),(19,685,4,7,"E");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (204,717,3,10,"E"),(109,887,4,9,"N"),(157,897,10,2,"O"),(62,969,7,3,"O"),(30,545,3,9,"E"),(107,54,3,3,"N"),(68,53,4,1,"S"),(215,783,8,7,"E"),(218,517,1,9,"E"),(116,347,7,9,"N");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (239,872,9,1,"N"),(141,239,9,3,"E"),(11,51,8,6,"O"),(26,22,8,8,"N"),(13,2,3,1,"E"),(41,65,4,4,"O"),(98,571,5,5,"E"),(147,714,5,5,"E"),(205,236,5,6,"N"),(86,296,4,1,"S");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (66,259,7,8,"N"),(153,424,4,9,"N"),(27,755,10,3,"S"),(193,257,2,7,"N"),(116,339,10,8,"E"),(223,109,8,10,"N"),(156,836,3,1,"N"),(121,153,3,7,"O"),(241,398,2,4,"O"),(67,985,3,4,"S");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (236,520,5,10,"O"),(243,923,2,10,"S"),(196,841,3,6,"S"),(214,795,9,3,"O"),(131,960,7,7,"S"),(190,473,1,5,"O"),(56,53,9,3,"S"),(199,322,9,10,"S"),(30,857,8,7,"S"),(153,233,2,4,"S");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (192,957,4,9,"E"),(63,945,2,10,"E"),(208,695,6,1,"N"),(28,590,5,4,"E"),(25,831,6,9,"E"),(177,8,5,10,"E"),(126,174,5,10,"E"),(62,533,7,9,"S"),(30,683,8,2,"O"),(220,29,1,3,"O");


INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (86,257,1,6,"E"),(50,400,6,4,"S"),(167,314,4,8,"S"),(212,930,5,6,"S"),(87,265,7,3,"S"),(170,942,7,3,"E"),(151,669,2,4,"E"),(19,688,3,3,"O"),(56,376,3,4,"O"),(150,309,2,4,"E");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (56,31,8,3,"S"),(77,154,1,7,"S"),(183,225,6,7,"N"),(105,704,1,1,"O"),(133,959,2,2,"S"),(90,940,6,1,"S"),(133,614,2,8,"N"),(212,810,1,5,"S"),(183,565,5,6,"O"),(12,543,2,8,"E");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (205,922,8,3,"O"),(146,717,4,5,"N"),(103,438,8,6,"N"),(102,488,4,3,"N"),(67,468,8,3,"S"),(177,347,7,4,"S"),(41,990,6,6,"N"),(191,369,7,6,"O"),(83,847,3,3,"O"),(14,121,6,1,"N");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (22,429,7,1,"S"),(225,588,6,5,"O"),(14,160,3,1,"O"),(42,946,6,1,"E"),(105,179,5,5,"O"),(110,928,7,7,"E"),(249,577,6,5,"O"),(49,966,2,2,"O"),(222,783,5,6,"E"),(26,824,8,3,"S");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (44,898,6,3,"E"),(3,50,2,7,"E"),(15,257,5,4,"O"),(123,66,1,1,"N"),(232,423,5,7,"S"),(19,883,4,7,"O"),(7,732,3,2,"E"),(126,153,8,4,"O"),(15,528,8,7,"E"),(107,809,6,8,"E");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (75,40,6,8,"S"),(221,154,8,1,"E"),(133,544,8,6,"O"),(211,352,1,8,"S"),(118,541,2,2,"O"),(237,682,8,2,"N"),(138,281,1,2,"N"),(96,273,3,1,"S"),(185,217,3,5,"O"),(126,516,7,4,"E");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (116,573,3,4,"E"),(55,448,1,7,"S"),(102,218,6,2,"N"),(154,278,2,2,"O"),(95,741,7,8,"E"),(120,66,2,2,"O"),(156,966,1,1,"E"),(135,75,8,6,"O"),(205,905,7,7,"O"),(33,203,5,2,"N");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (145,394,1,1,"E"),(203,580,4,5,"E"),(14,250,4,3,"O"),(138,386,8,5,"N"),(247,974,2,7,"O"),(22,706,3,5,"N"),(64,988,1,3,"O"),(100,709,1,7,"N"),(146,259,7,3,"N"),(150,369,4,1,"S");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (211,810,2,7,"E"),(97,920,8,5,"S"),(9,472,4,4,"N"),(25,44,3,7,"N"),(213,425,3,7,"S"),(242,688,1,7,"N"),(29,171,7,6,"S"),(121,933,3,2,"S"),(184,814,4,7,"E"),(30,36,3,4,"O");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (26,481,3,6,"S"),(238,616,3,4,"N"),(107,480,8,2,"S"),(152,165,1,1,"N"),(52,916,4,6,"N"),(5,177,5,1,"E"),(180,749,4,4,"N"),(195,330,2,6,"N"),(139,732,7,1,"O"),(170,324,2,2,"E");


INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (220,456,4,8,"E"),(32,269,4,6,"O"),(124,564,5,4,"O"),(158,653,2,3,"S"),(217,391,4,8,"E"),(130,20,3,2,"N"),(41,181,7,8,"S"),(222,762,4,7,"O"),(176,452,4,4,"S"),(248,304,2,5,"S");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (42,275,7,5,"S"),(243,51,7,1,"E"),(144,158,1,8,"N"),(195,919,6,1,"O"),(220,303,2,4,"N"),(231,600,4,8,"E"),(38,521,1,4,"O"),(21,263,7,2,"N"),(16,241,5,5,"O"),(148,420,1,3,"O");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (30,168,8,6,"O"),(172,604,5,4,"N"),(57,595,7,8,"E"),(96,955,2,1,"E"),(142,325,8,4,"N"),(101,744,2,1,"O"),(119,775,6,8,"E"),(83,686,3,1,"S"),(74,363,1,3,"N"),(205,873,8,7,"N");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (79,585,8,6,"O"),(241,756,3,3,"N"),(178,971,1,6,"S"),(202,353,2,7,"E"),(229,228,4,5,"N"),(235,568,7,4,"O"),(156,183,7,7,"O"),(75,834,8,2,"E"),(9,425,5,6,"S"),(119,931,1,8,"E");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (61,885,6,1,"O"),(86,365,5,7,"S"),(54,221,5,1,"N"),(9,236,1,7,"E"),(244,941,4,2,"O"),(161,202,8,7,"O"),(135,214,7,1,"N"),(132,89,1,5,"N"),(24,276,7,6,"N"),(116,317,6,8,"O");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (87,660,4,1,"N"),(168,6,7,2,"N"),(166,154,3,4,"S"),(147,222,2,6,"S"),(115,314,5,1,"N"),(111,969,4,8,"N"),(167,136,1,4,"E"),(152,432,7,8,"N"),(209,16,7,7,"E"),(217,938,8,5,"O");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (171,120,1,2,"O"),(231,671,6,3,"N"),(39,759,1,1,"N"),(17,889,7,2,"N"),(6,10,7,5,"E"),(107,186,5,1,"O"),(105,767,1,6,"S"),(146,427,2,6,"E"),(76,149,3,1,"E"),(12,726,6,3,"N");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (94,604,3,2,"O"),(79,837,6,7,"O"),(175,593,6,5,"O"),(14,274,1,8,"E"),(230,964,4,4,"N"),(155,479,7,1,"S"),(98,29,1,1,"S"),(108,733,7,6,"O"),(94,229,5,8,"E"),(83,798,4,4,"E");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (183,896,6,7,"N"),(118,841,5,7,"O"),(63,871,6,5,"O"),(125,960,5,6,"E"),(153,218,7,2,"S"),(216,182,3,7,"N"),(152,38,4,3,"E"),(25,117,1,4,"S"),(133,279,5,7,"N"),(10,557,3,5,"O");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (38,52,2,5,"E"),(7,608,6,3,"O"),(21,158,6,1,"N"),(235,12,5,4,"S"),(23,196,8,2,"E"),(164,956,8,1,"O"),(164,114,8,1,"O"),(27,65,6,2,"S"),(183,250,6,3,"N"),(128,679,2,6,"O");



INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (164,819,2,8,"N"),(52,878,8,1,"O"),(38,467,2,2,"S"),(221,581,5,6,"O"),(69,554,5,6,"O"),(60,263,8,1,"E"),(76,171,8,1,"E"),(60,142,8,3,"S"),(182,259,2,2,"E"),(73,553,4,4,"S");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (71,917,7,4,"N"),(27,947,7,7,"O"),(225,450,3,2,"E"),(30,977,8,3,"N"),(112,743,3,5,"N"),(171,907,2,2,"S"),(178,582,7,4,"S"),(175,588,1,6,"E"),(36,304,3,8,"O"),(119,11,4,2,"O");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (95,131,1,4,"N"),(240,307,1,8,"E"),(4,444,8,7,"E"),(203,962,6,2,"E"),(13,203,2,8,"S"),(67,504,3,7,"O"),(155,505,1,3,"S"),(216,739,2,2,"N"),(122,169,6,2,"E"),(115,315,3,6,"O");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (42,765,7,6,"O"),(106,751,7,8,"O"),(186,702,2,8,"E"),(51,301,4,1,"E"),(150,698,6,4,"O"),(171,971,1,3,"O"),(164,299,7,1,"N"),(162,519,4,8,"S"),(62,752,3,5,"O"),(230,442,5,3,"N");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (2,693,8,6,"S"),(31,204,2,4,"E"),(168,688,5,1,"N"),(154,899,2,5,"E"),(72,181,1,3,"S"),(190,736,4,8,"S"),(91,682,6,2,"O"),(11,462,4,4,"E"),(218,83,1,3,"N"),(167,614,2,6,"E");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (180,283,7,2,"S"),(112,614,3,7,"N"),(208,118,1,7,"S"),(180,280,3,1,"N"),(213,392,7,2,"N"),(169,876,5,8,"N"),(70,500,2,4,"E"),(50,395,6,6,"S"),(170,31,4,8,"N"),(170,673,6,2,"E");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (78,527,1,8,"O"),(65,208,5,8,"O"),(52,47,8,5,"S"),(82,307,4,6,"O"),(174,957,2,2,"O"),(151,30,2,7,"O"),(143,87,6,6,"S"),(248,151,5,6,"N"),(83,633,3,3,"N"),(9,20,5,6,"N");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (227,750,4,8,"E"),(201,212,3,3,"N"),(247,614,4,4,"S"),(249,270,1,3,"N"),(76,803,1,5,"O"),(81,607,1,5,"N"),(84,519,8,5,"E"),(40,305,3,1,"S"),(47,331,8,2,"N"),(65,884,5,3,"N");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (98,249,2,6,"S"),(199,832,1,1,"S"),(132,934,5,7,"N"),(247,774,5,7,"S"),(75,395,4,8,"N"),(57,481,3,2,"S"),(146,883,5,3,"N"),(239,173,5,2,"N"),(21,449,2,8,"O"),(34,563,7,4,"N");
INSERT INTO `habitacion_objeto` (`id_habitacion`,`id_objeto`,`x`,`y`,`orientacion`) VALUES (104,181,1,6,"N"),(108,81,3,4,"S"),(134,326,6,2,"N"),(165,121,8,8,"E"),(246,424,8,2,"N"),(218,650,6,1,"N"),(129,908,6,7,"E"),(66,823,4,2,"E"),(68,537,2,8,"E"),(245,421,8,7,"O");
