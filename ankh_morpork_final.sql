drop database if exists ciudad;
create database ciudad;
use ciudad;
create table ciudadano(
id int primary key auto_increment,
nombre varchar(100) not null,
fecha_nacimiento date,
edad int
);

create table barrio(
id int primary key auto_increment,
nombre varchar(100));


create table calle(
id int primary key auto_increment,
id_barrio int,
nombre varchar(100) unique,
foreign key (id_barrio) references barrio(id));


create table edificio(
id int primary key auto_increment,
numero varchar(10),
total_habitantes int,
id_calle int,
foreign key(id_calle) references calle(id)
);

create table edificio_publico(
id int primary key auto_increment,
nombre varchar(100),
id_edificio int,
tipo enum('guardia','justicia','correos','moneda'),
foreign key(id_edificio) references edificio(id));

CREATE TABLE edificio_privado (
    id INT PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(100),
    id_edificio int,
foreign key(id_edificio) references edificio(id)
);

create table negocio(
id int primary key auto_increment,
nombre varchar(100),
tipo enum('taberna','hospedaje','comercio'),
id_edificio int,
foreign key(id_edificio) references edificio(id)
);

create table gremio(
id int primary key auto_increment,
nombre varchar(150),
lema varchar(250),
id_edificio int,
foreign key(id_edificio) references edificio(id));

create table ciudadano_gremio(
id_ciudadano int,
id_gremio int,
fecha_alta date,
fecha_baja date,
cargo varchar(100),
foreign key(id_ciudadano) references ciudadano(id),
foreign key(id_gremio) references gremio(id),
primary key(id_ciudadano,id_gremio));


insert into barrio (id, nombre)
values(1,'Dolly Sisters'),
(2,'Unreal Estate'),
(3,"Dragon's Landing"),
(4, 'Small Gods'),
(5,'The Scours'),
(6,'The Hippo'),
(7,'The Shades'),
(8, 'Dimwell'),
(9, 'Longwall'),
(10,'Isle of Gods'),
(11, 'Seven Sleepers'),
(12, 'Nap Hill'),
(13, '---');















--
-- Inserting data into table ciudadano
--
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(1, 'Stevie Frizzell', '1976-05-12', 43);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(2, 'Jackeline Acker', '1975-07-05', 19);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(3, 'Twila Bock', '1983-01-15', 116);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(4, 'Reina Adkins', '1997-01-28', 97);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(5, 'Lavonia Whiteside', '1976-10-15', 82);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(6, 'Kendrick Naranjo', '1980-11-22', 4);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(7, 'Kim Desantis', '1974-04-15', 56);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(8, 'Raul Palma', '1978-06-20', 71);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(9, 'Lorette Ortiz', '1971-01-16', 2);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(10, 'Jewel Plummer', '1985-05-28', 121);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(11, 'Carter Matlock', '1970-01-07', 24);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(12, 'Clemente Craddock', '1992-10-16', 58);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(13, 'Rolland Watson', '1999-06-08', 114);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(14, 'Kayleen Hogg', '1973-08-15', 35);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(15, 'Loise Mayers', '1982-12-23', 59);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(16, 'Marine Turley', '1996-06-24', 109);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(17, 'Debby Concepcion', '1983-08-14', 92);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(18, 'Kent Staton', '1979-12-11', 95);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(19, 'Emmett Moulton', '1987-08-05', 1);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(20, 'Clarinda Acker', '1998-05-15', 82);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(21, 'Darrick Cardoza', '1973-01-19', 56);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(22, 'Kecia Adcock', '1984-10-15', 115);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(23, 'Maurice Mireles', '1988-08-10', 97);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(24, 'Ruben Simpson', '1972-05-17', 114);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(25, 'Britt Malley', '1970-01-07', 77);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(26, 'Jamie Creech', '1970-12-16', 88);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(27, 'Luis Cruse', '1986-01-03', 121);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(28, 'Carol Abrams', '1991-09-11', 16);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(29, 'Adah Adair', '1972-07-22', 66);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(30, 'Henry Crump', '1970-01-12', 52);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(31, 'Magdalena Ogle', '1977-12-28', 20);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(32, 'Donovan Allison', '1984-12-13', 42);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(33, 'Jeremy Aranda', '1992-01-27', 106);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(34, 'Camie Burch', '1992-06-30', 54);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(35, 'Robbie Canty', '1971-03-31', 78);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(36, 'Lavone Amos', '1972-06-15', 74);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(37, 'Johnnie Allard', '1972-08-03', 44);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(38, 'Debbra Acuna', '1991-10-24', 61);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(39, 'Abdul Bergstrom', '1974-01-18', 14);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(40, 'Nathanael Medina', '1977-06-23', 86);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(41, 'Kasey Savoy', '1993-04-30', 75);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(42, 'Raphael Beckman', '1970-03-03', 26);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(43, 'Eugenia Johansen', '1999-03-04', 101);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(44, 'Merissa Minter', '1995-03-21', 23);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(45, 'Alejandro Norwood', '1989-11-24', 31);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(46, 'Enoch Dortch', '1993-04-08', 110);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(47, 'Kathleen Burnside', '1997-10-31', 5);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(48, 'Stevie Vallejo', '1974-06-16', 46);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(49, 'Gilda Abney', '1993-03-16', 115);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(50, 'Pearlene Starnes', '1992-12-21', 123);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(51, 'Kelley Hiatt', '2000-04-10', 58);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(52, 'Brady Street', '1970-02-16', 38);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(53, 'Beau Poston', '1991-10-09', 45);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(54, 'Christeen Oconnor', '1994-02-27', 53);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(55, 'Aleisha Story', '1974-12-16', 46);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(56, 'Greta Prosser', '1993-01-22', 22);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(57, 'Shawn Deaton', '1986-01-10', 99);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(58, 'Ada Rapp', '1978-08-24', 11);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(59, 'Abe Christie', '1983-06-08', 21);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(60, 'Abbie Bullard', '1977-05-02', 88);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(61, 'Jenny Schulze', '1997-12-16', 4);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(62, 'Jamel Washington', '1970-01-12', 95);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(63, 'Augustus Barnette', '1991-02-05', 118);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(64, 'Rick Trevino', '1970-03-30', 1);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(65, 'Azalee Wang', '1983-02-14', 1);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(66, 'Jackqueline Abraham', '1972-08-02', 53);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(67, 'Marcel Casas', '1988-12-15', 22);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(68, 'Chiquita Headrick', '1990-05-12', 63);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(69, 'Mitchell Tanner', '1978-09-09', 105);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(70, 'Miesha Goldsmith', '1998-03-12', 90);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(71, 'Ahmed Abernathy', '1999-11-26', 6);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(72, 'Eddy Snodgrass', '1983-07-26', 96);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(73, 'Herschel Allison', '1998-09-06', 78);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(74, 'Carmelita Abel', '1996-09-23', 27);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(75, 'Daron Lowery', '1986-10-28', 87);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(76, 'Rudolph Staley', '1970-01-10', 41);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(77, 'Seth Martin', '1995-06-14', 70);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(78, 'Theola Addison', '1970-04-20', 99);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(79, 'Isela Crittenden', '1973-04-09', 73);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(80, 'Marvella Langston', '1991-08-16', 57);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(81, 'Cassondra Quintana', '1993-11-26', 16);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(82, 'Ada Barron', '1998-04-02', 51);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(83, 'Bob Whitlow', '1996-07-03', 106);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(84, 'Aileen Ricks', '1970-03-24', 63);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(85, 'Carter Wilburn', '1978-04-25', 79);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(86, 'Lewis Kurtz', '1970-01-10', 47);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(87, 'Martin Carpenter', '1974-07-10', 122);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(88, 'Maynard Mullins', '1995-07-19', 46);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(89, 'Deena Seifert', '1974-11-15', 93);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(90, 'Sonny Epps', '1974-09-04', 29);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(91, 'Darwin Aguirre', '1992-02-26', 54);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(92, 'Audie Kearney', '1994-01-17', 1);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(93, 'Ettie Fountain', '1988-06-25', 100);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(94, 'Ada Ackerman', '1976-05-22', 17);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(95, 'Joey Seiler', '1999-10-29', 7);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(96, 'Elbert Abrams', '1989-07-30', 27);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(97, 'Nicolas Dyer', '1970-01-02', 122);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(98, 'Shawnda Cameron', '1986-08-13', 1);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(99, 'Catherin Gaither', '1970-01-18', 44);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(100, 'Quentin Abraham', '1970-01-08', 23);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(101, 'Brain Ashworth', '1991-04-28', 54);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(102, 'Adela Payne', '1989-07-06', 84);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(103, 'Bella Crabtree', '1992-09-03', 112);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(104, 'Corey Turley', '1976-11-29', 78);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(105, 'Boyd Thomason', '1995-07-12', 82);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(106, 'Morgan Burt', '1970-02-24', 60);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(107, 'Seymour Walls', '1986-09-30', 91);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(108, 'Robbie Marquez', '1986-07-07', 3);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(109, 'Renaldo Norton', '1970-01-06', 86);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(110, 'Adolph Thrash', '1990-11-30', 106);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(111, 'Anissa Pimentel', '1976-03-26', 75);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(112, 'Crissy Adler', '1990-12-13', 40);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(113, 'Ada Acevedo', '1976-03-08', 74);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(114, 'Valery Zaragoza', '1970-11-23', 97);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(115, 'Charlotte Morrell', '1973-07-18', 10);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(116, 'Alfredia Bryan', '1970-02-17', 52);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(117, 'Letha Allison', '1970-04-01', 61);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(118, 'Lien Alicea', '1974-04-05', 48);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(119, 'Gwenda Vargas', '1977-05-05', 118);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(120, 'Lavette Theriault', '1973-05-06', 59);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(121, 'Nevada Rios', '1994-12-21', 29);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(122, 'Rosann Dockery', '1989-02-06', 66);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(123, 'Gary Briones', '1970-01-07', 19);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(124, 'Chung Gracia', '1970-02-10', 43);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(125, 'Cordell Addison', '1996-12-04', 120);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(126, 'Milford Nevarez', '1978-09-30', 63);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(127, 'Marcell Boyle', '1984-08-26', 115);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(128, 'Noah Toliver', '1991-02-14', 29);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(129, 'Conrad Bell', '1970-01-01', 63);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(130, 'Bryan Abney', '1995-08-06', 10);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(131, 'Christoper Suarez', '1970-01-05', 96);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(132, 'Adalberto Guerrero', '1972-03-25', 6);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(133, 'Seema Abney', '1970-01-22', 84);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(134, 'Mauricio Stanton', '1994-11-10', 37);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(135, 'Laverne Aiello', '1987-11-11', 37);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(136, 'Erik Abney', '1975-04-30', 36);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(137, 'Judith Ladd', '1971-01-07', 102);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(138, 'Collin Abel', '1977-05-04', 72);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(139, 'Claud Marion', '1991-08-31', 1);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(140, 'Gale Eller', '1992-09-06', 87);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(141, 'Antoinette Adler', '1975-10-21', 27);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(142, 'Cathern Pridgen', '1978-02-23', 56);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(143, 'Ali Fuchs', '1979-02-16', 4);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(144, 'Ozie Abbott', '1981-01-04', 62);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(145, 'Jarrett Lujan', '1970-11-26', 101);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(146, 'Vicki Albertson', '1970-02-10', 83);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(147, 'Grady Riggins', '1983-07-09', 81);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(148, 'Kittie Acker', '1971-10-14', 55);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(149, 'Coleman Chase', '1989-09-03', 45);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(150, 'Beatrice Mena', '1971-10-20', 114);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(151, 'Rhett Abernathy', '1970-01-15', 106);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(152, 'Lawrence Stowe', '1974-07-11', 116);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(153, 'Casey Pendergrass', '1993-05-10', 7);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(154, 'Cristin Batiste', '1986-06-05', 1);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(155, 'Elouise Jarrett', '1974-05-26', 81);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(156, 'Georgann Bergeron', '1988-12-22', 7);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(157, 'Adah Ornelas', '1999-03-29', 7);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(158, 'Noah Batts', '1970-01-01', 13);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(159, 'Tori Cloud', '1970-08-23', 19);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(160, 'Anderson Farrington', '1971-10-05', 78);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(161, 'Reuben Mcallister', '1996-09-21', 93);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(162, 'Daron Champagne', '1990-10-04', 3);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(163, 'Adolph Stephens', '1976-06-06', 28);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(164, 'Alexander Abreu', '1986-11-02', 8);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(165, 'Cesar Abernathy', '1983-09-25', 28);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(166, 'Felix Pereira', '1971-01-07', 118);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(167, 'Matthew Briseno', '1985-10-25', 120);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(168, 'Jerome Barber', '1973-08-22', 4);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(169, 'Major Blais', '1970-02-26', 52);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(170, 'Abigail Mcgrew', '1996-06-05', 118);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(171, 'Adolfo Abbott', '1970-01-27', 86);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(172, 'Madie Sales', '1970-05-31', 41);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(173, 'Ryan Roberge', '1996-01-05', 108);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(174, 'Stevie Flanders', '1995-12-26', 117);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(175, 'Abby Babcock', '1995-06-26', 40);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(176, 'Geraldine Hoover', '1976-03-14', 74);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(177, 'Tari Dickerson', '1979-08-02', 55);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(178, 'Archie Sayre', '1972-05-03', 65);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(179, 'Abbie Abney', '1991-11-27', 68);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(180, 'Kermit Free', '1987-07-17', 103);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(181, 'Crystle Bergeron', '1978-10-16', 120);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(182, 'Cordie Cooks', '1991-07-23', 88);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(183, 'Tristan Amaya', '1977-06-25', 92);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(184, 'Ross Huntley', '1980-04-08', 114);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(185, 'Johnny Nolan', '1996-06-23', 104);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(186, 'Gertude Weir', '1990-02-27', 25);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(187, 'Allen Olivo', '1970-01-04', 17);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(188, 'Agatha Slade', '1987-10-04', 41);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(189, 'Delaine Bowen', '1977-11-14', 111);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(190, 'Eugene Angel', '1977-03-27', 94);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(191, 'Courtney Swann', '1988-09-06', 98);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(192, 'Rhoda Lilley', '1970-03-17', 5);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(193, 'Richelle Fields', '1975-09-02', 4);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(194, 'Charita Monk', '1984-01-27', 120);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(195, 'Lakisha Alexander', '1972-05-09', 23);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(196, 'Alberto Lujan', '1972-07-25', 57);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(197, 'Abel Barnhart', '1980-08-21', 89);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(198, 'Alejandro Legg', '1970-08-10', 5);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(199, 'Tammie Brittain', '1984-10-03', 113);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(200, 'Berneice Bermudez', '1984-03-23', 70);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(201, 'Monte Dougherty', '1972-01-23', 33);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(202, 'Bernardo Hart', '1996-01-16', 47);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(203, 'Maurita Fraser', '1984-03-01', 84);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(204, 'Jarred Mercado', '1982-08-05', 11);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(205, 'Shawn Burnett', '1984-12-19', 14);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(206, 'Alejandrina Hickey', '1988-10-18', 91);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(207, 'Aide Spruill', '1970-02-05', 46);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(208, 'Olin Waite', '1970-02-27', 64);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(209, 'Abigail Hope', '1987-03-18', 40);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(210, 'Charis Yarbrough', '1970-01-06', 28);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(211, 'Jeannette Keaton', '1995-11-19', 22);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(212, 'Jacquelynn Acevedo', '1970-03-12', 3);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(213, 'Amos Jewett', '1973-04-04', 37);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(214, 'Adam Bigelow', '1982-11-09', 91);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(215, 'Manual Saxon', '1986-06-22', 52);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(216, 'Ada Belcher', '1972-03-15', 108);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(217, 'Alia Bachman', '1972-08-15', 75);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(218, 'Alfredo Negrete', '1979-03-11', 121);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(219, 'Denis Mayfield', '1975-10-06', 74);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(220, 'Krishna Ackerman', '1988-03-29', 112);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(221, 'Haywood Sampson', '1970-03-23', 77);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(222, 'Alexis Centeno', '1977-07-31', 68);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(223, 'Harley Greiner', '1970-01-29', 2);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(224, 'Brooke Cartwright', '1975-10-24', 86);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(225, 'Clarisa Acker', '1970-01-04', 27);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(226, 'Barton Drew', '1980-02-07', 17);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(227, 'Lorenzo Langdon', '1987-07-22', 80);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(228, 'Marcelino Madrigal', '1998-11-30', 43);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(229, 'Marvel Hendrix', '1973-08-21', 27);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(230, 'Nakita Homan', '1973-07-23', 41);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(231, 'Steven Abernathy', '1974-09-30', 22);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(232, 'Wendell Alford', '1970-03-16', 34);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(233, 'Cassidy Peek', '1982-08-18', 76);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(234, 'Santiago Wellman', '1974-02-17', 48);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(235, 'Cassey Abbott', '1970-03-26', 115);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(236, 'Ginny Sandoval', '1991-08-05', 51);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(237, 'Jimmie Britt', '1972-09-10', 0);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(238, 'Cara Page', '1981-12-19', 112);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(239, 'Gisele Henning', '1970-01-04', 3);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(240, 'Adah Bradley', '1991-12-23', 37);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(241, 'Della Damon', '1999-05-17', 105);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(242, 'Hester Cook', '1986-02-15', 52);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(243, 'Julian Wilhite', '1989-01-01', 86);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(244, 'Justin Adair', '1989-02-07', 42);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(245, 'Bertram Velasco', '1990-10-05', 3);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(246, 'Tilda Abney', '1996-10-29', 53);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(247, 'Austin Layne', '1970-02-04', 42);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(248, 'Janeth Almeida', '1971-01-08', 37);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(249, 'Eileen Abernathy', '1982-11-13', 0);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(250, 'Adam Cash', '1972-07-11', 10);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(251, 'Adan Tejeda', '1982-07-20', 3);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(252, 'Marcel Bush', '1976-01-14', 47);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(253, 'Dallas Bordelon', '1970-09-25', 41);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(254, 'Deeanna Ackerman', '1970-02-18', 61);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(255, 'January Rayford', '1992-06-04', 99);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(256, 'Vernon Carlson', '1991-11-22', 77);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(257, 'Carmelo Sumner', '1970-04-22', 98);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(258, 'Kristle Locke', '1970-01-07', 107);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(259, 'Luke Findley', '1997-04-02', 103);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(260, 'Marianne Pointer', '1972-04-07', 0);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(261, 'Rico Matteson', '1970-01-04', 47);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(262, 'Perry Lefebvre', '1970-02-23', 17);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(263, 'Roy Padilla', '1974-02-19', 109);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(264, 'Launa Tobin', '1972-09-03', 71);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(265, 'Eun Alcorn', '1991-06-18', 49);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(266, 'Joane Falk', '1971-11-16', 56);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(267, 'Randi Centeno', '1990-04-20', 65);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(268, 'Sharita Nevarez', '1991-03-03', 95);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(269, 'Haywood Macklin', '1974-01-12', 5);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(270, 'Laticia Colbert', '1973-03-24', 76);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(271, 'Hui Abernathy', '1993-12-19', 23);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(272, 'Deena Chan', '1972-03-15', 115);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(273, 'Kenyatta Lauer', '1976-08-15', 12);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(274, 'Benjamin Abney', '1970-01-26', 84);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(275, 'Shu Holguin', '1990-07-04', 117);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(276, 'Dominick Chang', '1986-03-17', 74);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(277, 'Bryanna Massie', '1970-12-21', 28);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(278, 'Jarrett Stearns', '1970-01-04', 39);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(279, 'Dario Bernard', '1983-03-02', 36);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(280, 'Jeff Hawks', '1984-07-16', 44);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(281, 'Adam Polanco', '1997-09-07', 48);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(282, 'Todd Abreu', '1999-11-26', 32);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(283, 'Adela Mitchell', '2000-03-08', 104);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(284, 'Lynnette Coulter', '1972-01-28', 17);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(285, 'Marquerite Chamberlain', '1975-07-22', 82);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(286, 'Kraig Humes', '1991-06-22', 32);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(287, 'Geralyn Mccord', '1983-06-21', 95);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(288, 'Agustin Cambell', '1982-03-07', 71);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(289, 'Amber Sikes', '1974-02-16', 30);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(290, 'Faith Dunn', '1978-03-04', 40);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(291, 'Rebeca Hare', '1970-01-03', 119);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(292, 'Elodia Sanchez', '1979-10-23', 30);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(293, 'Abel Andrade', '2000-01-11', 98);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(294, 'Alona Naquin', '1987-05-07', 62);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(295, 'Una Gerber', '1993-11-07', 29);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(296, 'Manuel Bohannon', '1970-07-08', 62);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(297, 'Oliver Gagnon', '1989-04-15', 54);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(298, 'Alise Abbott', '1970-01-01', 57);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(299, 'Malik Nye', '1970-03-09', 83);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(300, 'Regan Hurt', '1988-02-21', 53);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(301, 'Saran Eckert', '1971-05-09', 121);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(302, 'Hayden Cyr', '1994-08-02', 111);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(303, 'Darryl Billups', '1988-03-20', 20);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(304, 'Trinidad Augustine', '1978-03-03', 100);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(305, 'Tifany Hammons', '1970-04-06', 83);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(306, 'Gregory Swenson', '1982-03-15', 123);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(307, 'Ramonita Hardin', '1970-03-22', 115);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(308, 'Tomas Adame', '1987-11-10', 89);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(309, 'Adalberto Florence', '1995-07-24', 9);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(310, 'Bart Munoz', '1997-03-25', 22);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(311, 'Kermit Labbe', '1996-01-25', 95);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(312, 'Kelly Kaminski', '1979-01-14', 99);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(313, 'Mindy Abel', '1999-01-29', 7);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(314, 'Marlene Coon', '1982-10-15', 26);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(315, 'Leslie Abraham', '1974-02-18', 4);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(316, 'Rodrick Duff', '1992-01-28', 51);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(317, 'Vasiliki Craddock', '1971-09-24', 114);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(318, 'Corine Abrams', '1998-07-29', 36);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(319, 'Ardella Hefner', '1983-05-18', 88);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(320, 'Candie Chester', '1974-11-27', 5);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(321, 'Elroy Aldrich', '1971-06-19', 20);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(322, 'Adolfo Schafer', '1970-01-03', 63);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(323, 'Romeo Clarkson', '1976-09-30', 25);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(324, 'Adolfo Warren', '1970-02-22', 33);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(325, 'Malena Guerin', '1972-03-09', 31);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(326, 'Shanika Wilmoth', '1993-04-30', 48);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(327, 'Lynsey Beckham', '1974-05-21', 96);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(328, 'Adah Mobley', '1990-01-23', 2);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(329, 'Abe Durant', '1971-12-03', 71);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(330, 'Adah Lockett', '1981-08-16', 40);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(331, 'Ada Best', '1979-12-18', 29);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(332, 'Joleen Callaway', '1971-02-16', 24);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(333, 'Roland Guess', '1989-02-21', 29);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(334, 'Adelaida Lerner', '1970-01-08', 75);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(335, 'Alton Norwood', '1981-10-17', 66);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(336, 'Blair Abney', '1970-06-10', 83);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(337, 'Stephan Stone', '1979-09-25', 50);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(338, 'Elroy Adame', '1970-01-02', 75);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(339, 'Francine Velazquez', '1970-03-01', 1);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(340, 'Deandre Steinberg', '1978-04-21', 62);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(341, 'Kenton Provost', '1978-09-01', 62);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(342, 'Sabine Culpepper', '1987-03-05', 25);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(343, 'Chuck Lozano', '1977-08-15', 17);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(344, 'Mechelle Chadwick', '1995-08-08', 43);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(345, 'Kristine Hong', '1971-05-08', 15);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(346, 'Adelaida Abernathy', '1970-01-07', 71);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(347, 'Zachary Gilliam', '1986-02-15', 3);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(348, 'Candy Larry', '1977-01-31', 59);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(349, 'Anika Gregory', '1971-04-02', 9);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(350, 'Fermina Acker', '1979-08-06', 43);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(351, 'Adelaida Tibbs', '1996-03-11', 13);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(352, 'Amos Acker', '1997-04-08', 39);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(353, 'Ermelinda Ackerman', '1984-07-18', 35);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(354, 'Leia Brandt', '1986-07-17', 3);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(355, 'Alise Goodman', '1999-04-23', 69);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(356, 'Madelaine Whited', '1980-09-06', 41);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(357, 'Jadwiga Acker', '1971-06-20', 0);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(358, 'Retta Ali', '1970-10-06', 84);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(359, 'Gabriela Bolen', '1974-02-10', 49);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(360, 'Lisette Dias', '1992-09-29', 39);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(361, 'Jerald Winn', '1970-02-18', 14);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(362, 'Francis Abel', '1998-03-08', 83);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(363, 'Clarence Dooley', '1970-03-16', 7);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(364, 'Euna Hurt', '1970-01-10', 15);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(365, 'Addie Michaels', '1979-08-18', 11);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(366, 'Sadie Houck', '1990-09-27', 0);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(367, 'Korey Prichard', '1983-07-08', 8);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(368, 'Leslie Knapp', '1999-07-07', 100);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(369, 'Dolores Maloney', '1982-06-01', 60);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(370, 'Maryland Rodrigues', '1970-02-23', 80);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(371, 'Lila Ellis', '1978-09-02', 74);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(372, 'Victoria Early', '1970-05-02', 35);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(373, 'Karlyn Thibodeaux', '1996-05-01', 30);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(374, 'Jan Jacoby', '1985-01-14', 51);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(375, 'Noma Coward', '1987-12-08', 73);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(376, 'Levi Mccracken', '1997-10-10', 83);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(377, 'Tristan Arellano', '1971-07-19', 111);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(378, 'Geraldine Trammell', '1986-09-29', 35);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(379, 'Adam Shifflett', '1973-06-15', 64);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(380, 'Shawana Willoughby', '1989-04-03', 35);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(381, 'Tennille Pollard', '1982-08-25', 49);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(382, 'Tifany Abrams', '1971-10-10', 32);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(383, 'Ethan Mcneill', '1986-01-02', 109);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(384, 'Eliseo Hurd', '1981-04-07', 77);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(385, 'Twila Bronson', '1996-10-01', 24);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(386, 'Terence Bone', '1971-08-11', 77);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(387, 'Alejandra Mangum', '1988-05-28', 110);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(388, 'Carlo Stallworth', '1977-01-11', 89);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(389, 'Ai Dangelo', '1970-01-08', 3);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(390, 'Willy Casey', '1992-09-28', 67);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(391, 'Lemuel Buckley', '1993-05-29', 54);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(392, 'Roseanne Banks', '1988-08-21', 60);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(393, 'Tracey Osborn', '1970-01-02', 83);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(394, 'Pennie Milton', '1971-01-02', 91);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(395, 'Chuck Lomax', '1996-05-09', 118);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(396, 'Matthew Cardoza', '1982-03-23', 109);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(397, 'Chang Obryan', '1971-01-30', 114);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(398, 'Deshawn Wiggins', '1974-07-06', 88);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(399, 'Andera Frick', '1989-11-29', 88);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(400, 'Apolonia Acevedo', '1997-04-05', 86);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(401, 'Albertine Schaffer', '1973-08-04', 62);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(402, 'Earl Ayres', '1974-10-29', 76);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(403, 'Diane Burnham', '1988-05-04', 37);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(404, 'Madison Stpierre', '1989-11-17', 17);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(405, 'Clarine Albertson', '1972-03-14', 103);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(406, 'Isobel Rubin', '1970-04-03', 0);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(407, 'Kathline Sinclair', '1970-04-13', 44);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(408, 'Nadia Whyte', '1992-04-09', 9);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(409, 'Junita Bright', '1990-11-06', 21);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(410, 'Adelaida Conklin', '1993-12-20', 65);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(411, 'Odelia Brackett', '1980-01-19', 49);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(412, 'Telma Linville', '1994-02-01', 0);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(413, 'Alissa Branch', '1984-12-08', 6);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(414, 'Alana Abel', '1983-05-17', 104);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(415, 'Merlyn Abney', '1970-01-27', 55);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(416, 'Birdie Mckeever', '1999-04-27', 39);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(417, 'Kent Aldrich', '1995-10-18', 66);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(418, 'Hosea Gillis', '1972-02-15', 57);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(419, 'Alba Clevenger', '1980-04-23', 54);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(420, 'Beata Jaeger', '1973-09-18', 122);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(421, 'Xenia Acker', '1981-05-14', 118);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(422, 'Adelaida Acevedo', '1986-08-06', 43);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(423, 'Antonia Harry', '1970-01-08', 22);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(424, 'Scottie Cade', '1971-05-02', 92);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(425, 'Tamala Gilman', '1970-10-21', 81);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(426, 'Azzie Worthy', '1982-06-29', 13);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(427, 'Myron Eastman', '1976-04-22', 90);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(428, 'Era Brackett', '1987-04-17', 43);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(429, 'Brittny Reichert', '1973-04-13', 20);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(430, 'Dayna Blackwood', '1995-02-05', 6);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(431, 'Ariel Costello', '1975-12-02', 79);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(432, 'Martina Vogel', '1972-07-15', 23);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(433, 'Lance Alonzo', '1994-11-04', 4);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(434, 'Suzy Barbee', '1976-03-15', 2);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(435, 'Gwyn Selby', '1970-09-13', 8);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(436, 'Jarrod Ferrer', '1997-04-14', 27);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(437, 'Israel Alcala', '1981-03-12', 99);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(438, 'Stuart German', '1970-03-07', 47);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(439, 'Corinne Abney', '1998-12-09', 7);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(440, 'Salvatore Heaton', '1983-02-04', 106);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(441, 'Heather Abernathy', '1973-09-19', 1);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(442, 'Adaline Artis', '1995-07-03', 29);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(443, 'Mirella Poirier', '1973-12-18', 52);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(444, 'Ada Bartholomew', '1981-01-22', 7);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(445, 'Stefan Baron', '1971-04-30', 103);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(446, 'Ginger Brackett', '1991-11-11', 90);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(447, 'Abdul Craven', '1993-04-24', 71);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(448, 'Karin Dozier', '1992-06-01', 0);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(449, 'Sherlyn Kang', '1970-01-08', 4);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(450, 'Adrienne Goodman', '1972-09-12', 43);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(451, 'Bernard Aquino', '1976-10-18', 42);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(452, 'Leonel Furman', '1998-01-21', 29);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(453, 'Angila Higgs', '1984-08-17', 94);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(454, 'Allena Finnegan', '1971-04-18', 56);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(455, 'Joellen Van', '1998-02-20', 17);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(456, 'Abe Krause', '1970-01-14', 15);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(457, 'Filiberto Barbour', '1987-07-17', 107);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(458, 'Murray Aiken', '1995-12-07', 67);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(459, 'Thomasena Trent', '1971-04-22', 39);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(460, 'Zack Fairchild', '1987-08-29', 42);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(461, 'Carli Fish', '1986-03-01', 95);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(462, 'Wilfredo Agnew', '1970-01-03', 25);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(463, 'Preston Herzog', '1992-10-17', 42);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(464, 'Newton Hogue', '1988-12-07', 48);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(465, 'Jed Acevedo', '1991-10-28', 64);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(466, 'Parker Cramer', '1997-02-02', 99);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(467, 'Numbers Malley', '1971-05-29', 25);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(468, 'Abraham Paxton', '1974-06-02', 9);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(469, 'Demarcus Murphy', '1980-04-17', 73);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(470, 'Abram Hoffman', '1970-01-04', 89);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(471, 'Abel Aquino', '1999-06-16', 87);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(472, 'Cherri Dodge', '1994-03-23', 2);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(473, 'Jason Cary', '1970-03-19', 58);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(474, 'Carmon Larose', '1984-06-24', 74);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(475, 'Dorla Rocha', '1970-01-08', 34);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(476, 'Eldon Acosta', '1998-10-08', 100);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(477, 'Refugio Candelaria', '1979-10-19', 89);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(478, 'Carmon Alderman', '1979-05-17', 102);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(479, 'Doria Acevedo', '1972-06-24', 75);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(480, 'Adam Breen', '1995-04-25', 11);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(481, 'Trish Mcghee', '1998-10-29', 9);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(482, 'Jerica Bassett', '1988-08-14', 7);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(483, 'Malik Arellano', '1993-09-07', 13);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(484, 'Wade Hubert', '1989-05-17', 80);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(485, 'Abel Hutchens', '1975-02-09', 80);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(486, 'Meggan Alicea', '1970-02-13', 77);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(487, 'Maryjo Devine', '1972-02-14', 93);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(488, 'Lenny Batson', '1981-06-24', 27);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(489, 'Arnetta Manley', '1991-03-23', 121);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(490, 'Thanh Parks', '1970-03-09', 7);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(491, 'Joleen Cotton', '1970-02-09', 61);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(492, 'Adaline Westmoreland', '1970-02-12', 60);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(493, 'Abraham Hong', '1970-04-06', 72);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(494, 'Cleo Borden', '1986-03-03', 116);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(495, 'Lashaun Allison', '1976-12-09', 89);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(496, 'Mira Gordon', '1973-07-24', 111);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(497, 'Hedy Dubois', '1995-12-16', 22);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(498, 'Deandre Cano', '1998-10-20', 113);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(499, 'Vernie Register', '1970-01-26', 70);
INSERT INTO ciudadano(id, nombre, fecha_nacimiento, edad) VALUES
(500, 'Brett Acker', '1986-05-10', 7);

--
-- Inserting data into table calle
--
INSERT INTO calle(id, id_barrio, nombre) VALUES
(1, 8, 'Abbey Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(2, 5, 'East India Dock Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(3, 6, 'Aberconway Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(4, 11, 'Jubilee Way');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(5, 10, 'Brooksby''s Walk');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(6, NULL, 'Cloudsdale Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(7, 13, 'Matlock Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(8, 10, 'Hamilton Place');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(9, 11, 'Rick Roberts Way');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(10, 12, 'Newall Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(11, 7, 'Curzon Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(12, 5, 'Mayville Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(13, 1, 'Abbey Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(14, 12, 'Whitehorse Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(15, 9, 'Byrne Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(16, 13, 'Roehampton High Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(17, 10, 'Cheyne Avenue');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(18, 1, 'Grasmere Avenue');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(19, 9, 'Holmes Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(20, 4, 'St James''s Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(21, 11, 'Frant Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(22, 4, 'Procter Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(23, 2, 'Beryl Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(24, 1, 'Central Hill');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(25, 6, 'Stamford Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(26, 5, 'Wolsey Avenue');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(27, 12, 'Charles Crescent');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(28, 3, 'St John''s Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(29, 1, 'Fellows Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(30, 1, 'Porlock Avenue');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(31, 4, 'Grove Hill Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(32, 3, 'Abbotsbury Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(33, 2, 'Yerbury Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(34, 10, 'Old Compton Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(35, 4, 'Undine Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(36, 8, 'Cranbourn Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(37, 5, 'Balham High Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(38, 12, 'Heath Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(39, 10, 'Second Avenue');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(40, 9, 'Great Queen Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(41, 2, 'Sandwell Crescent');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(42, 11, 'Eccleston Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(43, 1, 'Broadwick Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(44, 9, 'Churchill Avenue');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(45, 7, 'Century Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(46, 2, 'Cheshire Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(47, 1, 'Antill Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(48, 6, 'Mayfield Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(49, 3, 'Ada Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(50, 7, 'Randolph Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(51, 7, 'Gainsford Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(52, 5, 'Abercairn Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(53, 5, 'Berkeley Square');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(54, 7, 'Grimsdyke Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(55, 1, 'Palfrey Place');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(56, 8, 'Fitzroy Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(57, 5, 'King Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(58, 2, 'Bedford Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(59, 13, 'New Square');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(60, 12, 'Abercorn Crescent');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(61, 7, 'Jermyn Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(62, 13, 'Colegrave Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(63, 8, 'Abbey Lane');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(64, 13, 'Acklam Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(65, 11, 'Congreve Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(66, 2, 'Ariel Way');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(67, 8, 'Alexander Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(68, 9, 'Moreton Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(69, 13, 'Crestfield Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(70, 12, 'Sedlescombe Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(71, 7, 'Glasshouse Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(72, 9, 'Shacklewell Lane');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(73, 6, 'Adelaide Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(74, 8, 'Shoot Up Hill');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(75, 4, 'Edison Close');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(76, 8, 'Gerrard Place');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(77, 2, 'Thorpe Close');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(78, 12, 'Bedwell Close');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(79, 7, 'Rowse Close');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(80, 1, 'Macmillan Way');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(81, 2, 'Mexfield Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(82, 12, 'Mercier Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(83, 6, 'Oxendon Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(84, 1, 'Thurloe Court');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(85, 6, 'Wolseley Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(86, 11, 'Lawson Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(87, 13, 'Clarence Place');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(88, 6, 'Montague Close');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(89, 2, 'Horn Lane');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(90, 13, 'Delancey Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(91, 9, 'Bridge Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(92, 5, 'Peters Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(93, 8, 'Admiral Seymore Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(94, 4, 'Altior Court');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(95, 10, 'Mullards Close');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(96, 9, 'Ashburnham Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(97, 4, 'Tadema Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(98, 3, 'Putney High Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(99, 11, 'Mallinson Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(100, 13, 'Abbey Mews');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(101, 12, 'Windmill Grove');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(102, 4, 'Clifton Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(103, 11, 'Appleby Close');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(104, 5, 'Blackfriars Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(105, 7, 'Bowling Green Lane');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(106, 6, 'Kings Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(107, 2, 'Digby Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(108, 10, 'Buckfast Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(109, 4, 'Broad Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(110, 4, 'Colonnade Walk');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(111, 5, 'Marlow Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(112, 11, 'Balmoral Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(113, 8, 'Canada Place');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(114, 9, 'Dornton Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(115, 13, 'Shepherds Hill');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(116, 5, 'Nunhead Lane');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(117, 2, 'Wilmer Lea Close');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(118, 2, 'Hove Avenue');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(119, 3, NULL);
INSERT INTO calle(id, id_barrio, nombre) VALUES
(120, 7, 'Harcourt Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(121, 1, 'Cyprus Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(122, 2, 'New Row');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(123, 11, 'Elphinstone Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(124, 6, 'Abbeville Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(125, 7, 'Newell Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(126, 12, 'Ashfield Close');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(127, 4, 'Tower Hamlets Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(128, 13, 'Keston Avenue');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(129, 6, 'Aphrodite Court');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(130, 9, 'Argyle Square');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(131, 9, 'Sudbury Hill');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(132, 7, 'Carnarvon Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(133, 3, 'Upper Walthamstow Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(134, 6, 'Upper Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(135, 8, 'Milton Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(136, 10, 'Flanders Way');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(137, 12, 'Albion Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(138, 11, 'Denbigh Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(139, 5, 'Hamilton Court');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(140, 4, 'Seymour Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(141, 3, 'Greville Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(142, 8, 'Crown Place');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(143, 7, 'Rectory Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(144, 4, 'Spanish Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(145, 6, 'Old Ford Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(146, 11, 'Hurdwick Place');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(147, 8, 'Battersea Square');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(148, 1, 'Northfield Avenue');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(149, 8, 'Highgate Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(150, 9, 'Campden Grove');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(151, 13, 'City Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(152, 11, 'Uxbridge Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(153, 7, 'Lewisham Way');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(154, 10, 'Northington Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(155, 4, 'Barnet Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(156, 3, 'East End Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(157, 11, 'Honour Lea Avenue');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(158, 3, 'Fulham Palace Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(159, 1, 'Ashwin Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(160, 1, 'Maddox Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(161, 2, 'Whitechapel Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(162, 6, 'Artillery Lane');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(163, 8, 'Grasgarth Close');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(164, 5, 'Campden Hill Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(165, 5, 'Holloway Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(166, 13, 'New Broadway');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(167, 13, 'Dingwall Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(168, 2, 'Warwick Way');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(169, 3, 'Bressey Grove');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(170, 13, 'Bulstrode Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(171, 10, 'Webb''s Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(172, 10, 'Hallingbury Court');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(173, 5, 'Elliott Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(174, 8, 'Alander Mews');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(175, 9, 'St Chad''s Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(176, 3, 'Addington Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(177, 5, 'Azura Court');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(178, 4, 'Northolt Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(179, 4, 'Maplin Harrow');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(180, 12, 'Panton Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(181, 10, 'St. Marks Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(182, 4, 'Wilson Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(183, 8, 'Alder Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(184, 1, 'Lacy Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(185, 3, 'Great Cumberland Mews');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(186, 10, 'Springbridge Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(187, 3, 'Wakefield Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(188, 3, 'Penrhyn Crescent');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(189, 13, 'New Bond Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(190, 1, 'Anerley Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(191, 12, 'Buxton Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(192, 12, 'Windsor Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(193, 2, 'Clemence Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(194, 7, 'South Wharf Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(195, 7, 'Larcom Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(196, 4, 'Wells Mews');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(197, 1, 'Hampton Court Road');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(198, 2, 'Rathbone Street');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(199, 6, 'Catford Broadway');
INSERT INTO calle(id, id_barrio, nombre) VALUES
(200, 8, 'Victoria Park Road');

--
-- Inserting data into table edificio
--
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(1, '5', 18, 72);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(2, '28 b', 4, 1);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(3, '1 a', 12, 77);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(4, '95 b', 18, 72);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(5, '5 a', 19, 89);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(6, '2 b', 2, 80);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(7, '32', 16, 96);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(8, '95 b', 17, 138);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(9, '2 b', 6, 67);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(10, '55 b', 12, 96);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(11, '3 a', 8, 148);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(12, '97 a', 9, 16);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(13, '49', 1, 153);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(14, '0', 15, 10);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(15, '84 b', 9, 143);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(16, '7 b', 13, 97);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(17, '7', 15, 173);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(18, '1', 17, 58);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(19, '25 a', 9, 32);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(20, '32 a', 8, 145);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(21, '27 a', 13, NULL);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(22, '74', 1, 86);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(23, '1 a', 2, 8);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(24, '7 a', 11, 162);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(25, '2 a', 19, 124);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(26, '7', 16, 77);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(27, '25', 12, 134);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(28, '63 b', 18, 65);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(29, '1', 11, 62);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(30, '8', 8, 71);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(31, '10 b', 21, 2);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(32, '82 a', 20, 94);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(33, '2', 12, 167);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(34, '51', 21, 59);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(35, '19', 14, 107);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(36, '41', 19, 34);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(37, '86', 1, 44);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(38, '49', 9, 140);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(39, '98', 11, 56);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(40, '1', 5, 84);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(41, '2', 11, 25);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(42, '9', 13, 114);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(43, '4 a', 11, 109);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(44, '1', 11, NULL);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(45, '8 a', 8, 103);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(46, '28', 7, 185);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(47, '6', 16, 147);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(48, '55', 2, 197);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(49, '30 b', 4, NULL);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(50, '3', 15, 25);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(51, '9 b', 12, 12);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(52, '3 b', 1, 199);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(53, '8 b', 9, 163);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(54, '46', 18, NULL);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(55, '3 b', 15, 176);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(56, '68', 20, 187);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(57, '40', 9, 149);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(58, '10 b', 20, 148);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(59, '35', 15, 102);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(60, '9', 21, 168);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(61, '8', 17, 104);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(62, '5 b', 9, 114);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(63, '71', 14, 84);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(64, '4 a', 8, 106);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(65, '0 a', 7, 116);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(66, '0 b', 4, 194);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(67, '41 b', 4, 147);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(68, '5', 22, 170);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(69, '33', 17, 200);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(70, '51', 16, 183);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(71, '4', 20, 44);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(72, '71 b', 18, NULL);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(73, '8', 5, 26);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(74, '22', 19, 85);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(75, '93 a', 21, 3);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(76, '3 a', 21, 192);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(77, '05', 21, 152);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(78, '22', 12, 175);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(79, '25 b', 3, 125);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(80, '54 a', 18, 82);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(81, '47 b', 16, 34);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(82, '89', 1, 58);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(83, '6', 19, 115);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(84, '9 a', 17, 17);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(85, '17 b', 22, 140);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(86, '3', 14, 41);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(87, '9 b', 13, NULL);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(88, '2', 13, 109);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(89, '4 b', 7, 100);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(90, '9', 16, 178);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(91, '29 a', 16, 45);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(92, '3', 12, 106);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(93, '17', 7, 108);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(94, '32 a', 5, 47);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(95, '80', 10, 51);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(96, '31', 12, NULL);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(97, '4 a', 3, 34);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(98, '69', 19, 147);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(99, '75', 4, 193);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(100, '85', 10, 4);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(101, '95 b', 14, 59);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(102, '2', 16, 71);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(103, '91 b', 17, 11);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(104, '90', 14, 135);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(105, '87 b', 7, NULL);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(106, '30', 20, 93);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(107, '7', 6, 92);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(108, '8', 20, 161);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(109, '1 b', 20, 4);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(110, '91', 15, 195);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(111, '76 a', 16, 35);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(112, '94 a', 5, 25);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(113, '9 a', 15, 146);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(114, '27 a', 20, 126);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(115, '89', 12, 194);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(116, '4', 4, 115);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(117, '9', 14, 19);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(118, '2 b', 16, 175);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(119, '7 a', 1, 33);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(120, '27', 9, 72);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(121, '5 a', 2, 73);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(122, '54 a', 5, 128);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(123, '1', 14, 82);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(124, '6 b', 22, 109);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(125, '49 b', 6, 8);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(126, '60 a', 17, 41);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(127, '6', 11, 141);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(128, '34 b', 11, 115);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(129, '31', 16, 30);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(130, '5 b', 22, NULL);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(131, '6 a', 6, 178);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(132, '1', 5, 156);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(133, '87 b', 1, 124);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(134, '2', 15, 6);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(135, '9', 20, 90);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(136, '92', 1, 143);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(137, '79 b', 14, 187);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(138, '0 b', 8, 27);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(139, '7 a', 13, 119);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(140, '41', 8, 195);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(141, '37 a', 9, 195);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(142, '77 b', 18, 177);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(143, '26', 9, 169);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(144, '75 a', 7, 195);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(145, '5 a', 15, 144);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(146, '37 a', 12, 187);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(147, '6', 5, 120);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(148, '2 b', 1, 114);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(149, '64 a', 11, 37);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(150, '7', 4, 79);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(151, '9', 2, 44);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(152, '50 b', 3, 23);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(153, '70 b', 1, 156);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(154, '9 b', 1, 116);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(155, '81 a', 16, 16);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(156, '65', 8, 191);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(157, '5 b', 19, 41);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(158, '64 a', 6, 122);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(159, '54', 6, 30);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(160, '5', 4, 150);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(161, '8 a', 19, 195);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(162, '4 a', 17, 55);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(163, '79 a', 15, 143);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(164, '41', 12, 19);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(165, '1 b', 2, 136);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(166, '05 b', 17, 1);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(167, '67', 6, 7);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(168, '4 a', 16, 65);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(169, '7', 19, 126);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(170, '3 a', 2, 68);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(171, '3 b', 2, 40);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(172, '06 b', 21, 142);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(173, '2 a', 3, 74);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(174, '8 b', 22, 91);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(175, '3 a', 13, 16);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(176, '86', 22, 127);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(177, '07 b', 22, 166);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(178, '4', 15, 47);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(179, '07 a', 5, 168);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(180, '9', 1, 134);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(181, '02 a', 16, 87);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(182, '9', 17, 130);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(183, '2', 1, 147);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(184, '6', 16, 162);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(185, '8 a', 21, 33);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(186, '29 a', 9, 80);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(187, '30 b', 3, 37);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(188, '1', 13, 10);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(189, '87', 7, 144);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(190, '29', 13, NULL);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(191, '31 b', 19, NULL);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(192, '9', 17, NULL);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(193, '52 b', 3, NULL);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(194, '3 a', 19, NULL);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(195, '40', 3, NULL);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(196, '3 b', 18, NULL);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(197, '4 b', 15, NULL);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(198, '55', 11, NULL);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(199, '9 a', 21, NULL);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(200, '8', 2, NULL);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(201, '46 b', 3, 165);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(202, '54', 21, 149);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(203, '60 a', 12, 115);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(204, '4 b', 4, 129);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(205, '3 b', 6, 140);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(206, '92 b', 4, 185);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(207, '85', 4, 97);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(208, '9', 1, 154);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(209, '36 a', 2, 92);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(210, '94 b', 7, 39);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(211, '97 b', 15, 168);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(212, '9 b', 2, 26);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(213, '7', 9, 16);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(214, '3 b', 5, 102);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(215, '3 a', 21, 75);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(216, '00 a', 16, 77);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(217, '36 a', 13, 194);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(218, '44 b', 21, 1);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(219, '94', 4, 135);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(220, '8', 9, 161);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(221, '5', 21, 136);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(222, '1 b', 22, 182);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(223, '4', 8, 140);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(224, '6', 7, 133);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(225, '5', 8, 100);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(226, '17 a', 1, 93);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(227, '0 b', 9, 143);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(228, '03', 9, 179);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(229, '80', 14, 190);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(230, '2', 1, 99);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(231, '75 b', 6, 58);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(232, '7', 1, 71);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(233, '5 b', 6, 53);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(234, '1', 14, 110);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(235, '0', 2, 57);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(236, '6 a', 10, 138);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(237, '8 a', 11, 174);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(238, '9 a', 17, 128);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(239, '3', 5, 57);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(240, '16 a', 16, 98);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(241, '46 a', 6, 134);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(242, '2 a', 1, 109);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(243, '36 b', 12, 23);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(244, '48 b', 5, 39);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(245, '84 b', 19, 31);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(246, '3', 16, 44);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(247, '0', 9, 53);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(248, '5', 10, 135);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(249, '79 b', 9, 44);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(250, '39 a', 2, 179);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(251, '6', 11, 172);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(252, '00', 14, 138);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(253, '2 b', 16, 69);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(254, '62 b', 22, 65);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(255, '15', 6, 60);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(256, '2', 18, 154);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(257, '8 a', 3, 22);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(258, '3', 12, 97);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(259, '67 a', 19, 2);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(260, '06', 7, 75);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(261, '5', 22, 111);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(262, '1', 8, 198);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(263, '4', 19, 152);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(264, '5', 7, 175);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(265, '6 a', 3, 61);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(266, '50 a', 9, 74);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(267, '80', 3, 54);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(268, '11 b', 9, 168);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(269, '1', 22, 29);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(270, '69 a', 20, 187);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(271, '2 a', 8, 21);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(272, '77', 20, 162);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(273, '0 b', 18, 42);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(274, '0', 22, 182);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(275, '19', 20, 90);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(276, '80', 16, 176);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(277, '80 a', 15, 101);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(278, '8 a', 16, 133);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(279, '2 b', 11, 19);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(280, '4', 3, 64);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(281, '3 a', 8, 167);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(282, '5', 10, 17);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(283, '8 b', 14, 26);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(284, '56 a', 4, 184);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(285, '95 a', 5, 167);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(286, '6 a', 13, 17);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(287, '4 b', 4, 24);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(288, '89', 1, 132);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(289, '5', 2, 99);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(290, '03 b', 22, 174);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(291, '96 a', 21, 63);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(292, '6 a', 21, 173);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(293, '01 b', 14, 24);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(294, '55 a', 5, 63);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(295, '92', 11, 66);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(296, '8 a', 16, 149);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(297, '9', 9, 14);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(298, '88 a', 12, 3);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(299, '08', 12, 179);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(300, '40', 13, 146);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(301, '05 b', 7, 48);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(302, '72', 4, 192);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(303, '48 a', 1, 90);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(304, '7 b', 6, 55);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(305, '11 a', 1, 139);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(306, '90', 20, 40);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(307, '13', 6, 19);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(308, '31 a', 10, 97);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(309, '65 a', 19, 54);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(310, '17', 1, 64);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(311, '83 a', 21, 129);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(312, '0', 20, 22);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(313, '3 b', 21, 83);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(314, '2 b', 9, 64);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(315, '2 a', 1, 194);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(316, '37 a', 14, 68);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(317, '19', 13, 114);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(318, '01 a', 12, 16);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(319, '19 a', 15, 51);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(320, '97', 3, 164);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(321, '4 a', 8, 194);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(322, '09', 11, 57);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(323, '8', 4, 115);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(324, '5', 16, 198);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(325, '0 b', 17, 107);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(326, '97', 6, 33);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(327, '72 a', 18, 34);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(328, '4', 16, 93);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(329, '2 a', 21, 94);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(330, '03', 4, 38);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(331, '8', 16, 109);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(332, '64', 17, 100);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(333, '2', 21, 47);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(334, '0 a', 8, 61);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(335, '59', 8, 26);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(336, '78 a', 21, 102);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(337, '6', 1, 19);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(338, '8', 22, 32);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(339, '20', 3, 43);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(340, '07 a', 8, 11);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(341, '33', 3, 23);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(342, '4 b', 12, 47);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(343, '1', 20, 4);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(344, '63 a', 8, 37);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(345, '84', 21, 77);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(346, '4 a', 6, 32);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(347, '51 a', 16, 119);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(348, '55', 20, 188);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(349, '68', 14, 105);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(350, '3', 12, 70);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(351, '76 a', 11, 79);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(352, '9', 3, 121);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(353, '8', 7, 62);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(354, '8', 3, 73);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(355, '3 a', 22, 45);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(356, '64', 12, 54);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(357, '34', 11, 170);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(358, '1 b', 19, 7);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(359, '3 b', 10, 124);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(360, '7', 4, 5);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(361, '6 a', 20, 160);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(362, '69 b', 16, 134);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(363, '2 b', 7, 153);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(364, '3 a', 3, 199);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(365, '4', 2, 27);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(366, '7 b', 3, 162);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(367, '05 b', 10, 16);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(368, '56 a', 21, 123);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(369, '82 a', 15, 190);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(370, '7 a', 13, 2);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(371, '95 a', 22, 190);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(372, '55 a', 9, 185);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(373, '30 a', 12, 42);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(374, '9', 18, 62);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(375, '99 b', 15, 104);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(376, '46', 17, 200);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(377, '4', 8, 39);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(378, '8 a', 6, 171);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(379, '1', 6, 113);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(380, '5 b', 20, 182);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(381, '9 b', 9, 36);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(382, '26', 11, 120);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(383, '06 b', 2, 102);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(384, '8 a', 1, 70);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(385, '1 a', 8, 154);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(386, '5 b', 13, 108);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(387, '39 b', 22, 118);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(388, '4', 6, 21);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(389, '6 b', 12, 13);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(390, '63 b', 13, 123);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(391, '32', 22, 21);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(392, '2', 3, 62);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(393, '07', 12, 192);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(394, '3 b', 4, 158);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(395, '1 a', 13, 72);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(396, '33 b', 21, 110);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(397, '89 a', 18, 33);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(398, '2 b', 22, 120);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(399, '9 b', 16, 18);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(400, '56 a', 15, 5);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(401, '66', 14, 83);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(402, '6', 10, 139);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(403, '5 a', 3, 186);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(404, '03', 13, 153);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(405, '4', 11, 32);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(406, '4', 4, 119);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(407, '20', 18, 42);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(408, '90 b', 7, 169);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(409, '62', 16, 105);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(410, '4 b', 2, 10);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(411, '4', 7, 196);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(412, '7', 2, 174);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(413, '7', 12, 51);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(414, '50', 8, 164);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(415, '2 a', 19, 95);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(416, '53 a', 13, 40);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(417, '05', 4, 63);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(418, '4', 16, 106);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(419, '8', 3, 62);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(420, '8', 11, 8);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(421, '77 a', 18, 108);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(422, '3 b', 3, 75);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(423, '2 a', 18, 188);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(424, '8', 10, 57);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(425, '64', 14, 80);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(426, '95 a', 4, 186);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(427, '6 a', 11, 86);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(428, '22 a', 20, 55);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(429, '7 b', 5, 98);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(430, '24', 7, 76);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(431, '12 a', 9, 97);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(432, '9', 19, 120);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(433, '2 a', 7, 61);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(434, '91 b', 6, 132);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(435, '4 b', 18, 83);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(436, '2', 6, 161);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(437, '4 a', 16, 134);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(438, '1 b', 5, 162);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(439, '1 b', 11, 163);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(440, '61 b', 10, 181);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(441, '4', 10, 145);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(442, '9', 21, 129);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(443, '0', 8, 153);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(444, '1', 15, 193);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(445, '8', 20, 180);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(446, '6', 2, 140);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(447, '67', 15, 92);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(448, '6', 5, 133);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(449, '2 a', 7, 17);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(450, '67', 6, 116);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(451, '3 a', 2, 170);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(452, '67 a', 18, 83);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(453, '80', 11, 101);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(454, '43', 10, 165);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(455, '06 a', 10, 31);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(456, '40', 2, 46);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(457, '63', 11, 189);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(458, '0 b', 19, 157);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(459, '31', 10, 106);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(460, '2', 3, 48);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(461, '0', 8, 136);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(462, '66 b', 7, 73);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(463, '46', 15, 71);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(464, '67', 4, 187);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(465, '7', 20, 61);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(466, '20 b', 5, 103);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(467, '33', 22, 129);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(468, '4 a', 1, 10);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(469, '80 a', 3, 100);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(470, '87', 10, 89);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(471, '10 a', 11, 162);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(472, '8 a', 11, 68);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(473, '22', 16, 168);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(474, '03', 15, 102);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(475, '42 b', 18, 166);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(476, '9 b', 16, 152);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(477, '78', 21, 120);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(478, '25 b', 9, 36);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(479, '58 b', 11, 61);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(480, '9', 16, 37);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(481, '53 b', 22, 73);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(482, '90', 15, 57);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(483, '87 a', 20, 168);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(484, '72 b', 4, 152);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(485, '1', 8, 174);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(486, '6 b', 16, 36);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(487, '1', 12, 83);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(488, '6', 8, 133);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(489, '8', 18, 55);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(490, '86', 22, 25);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(491, '0', 5, 171);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(492, '0 b', 8, 104);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(493, '17', 19, 90);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(494, '25 b', 16, 71);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(495, '05 a', 5, 83);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(496, '7 b', 9, 20);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(497, '1 a', 22, 184);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(498, '75 b', 17, 188);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(499, '41 b', 11, 130);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(500, '4', 6, 196);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(501, '28', 8, 162);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(502, '4 a', 1, 64);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(503, '02', 18, 133);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(504, '0', 12, 67);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(505, '2', 1, 175);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(506, '80 b', 21, 96);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(507, '42', 9, 60);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(508, '7', 17, 111);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(509, '8', 2, 72);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(510, '87', 16, 141);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(511, '80 b', 8, 61);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(512, '08', 9, 133);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(513, '01 b', 16, 173);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(514, '1 b', 11, 41);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(515, '4', 4, 63);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(516, '0 a', 15, 122);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(517, '3 a', 6, 174);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(518, '0', 18, 38);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(519, '76', 8, 1);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(520, '6', 7, 153);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(521, '06 b', 2, 169);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(522, '3 a', 8, 162);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(523, '9 b', 17, 17);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(524, '1 b', 19, 24);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(525, '5 b', 9, 163);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(526, '31', 13, 28);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(527, '6 a', 15, 51);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(528, '59 a', 6, 91);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(529, '6', 2, 83);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(530, '5 a', 19, 143);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(531, '00 a', 18, 109);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(532, '34', 5, 157);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(533, '8 a', 14, 183);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(534, '43', 13, 54);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(535, '58 b', 17, 80);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(536, '5 a', 12, 95);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(537, '1', 10, 157);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(538, '1 a', 13, 129);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(539, '8 b', 12, 149);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(540, '93 a', 6, 137);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(541, '02 b', 13, 195);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(542, '3 a', 8, 92);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(543, '52 b', 14, 8);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(544, '58 b', 19, 128);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(545, '0 a', 7, 8);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(546, '90 b', 14, 149);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(547, '3', 14, 91);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(548, '69 b', 2, 41);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(549, '14 b', 11, 141);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(550, '24 b', 22, 17);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(551, '47 a', 4, 180);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(552, '1', 18, 78);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(553, '17 b', 2, 136);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(554, '4 b', 21, 64);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(555, '88 a', 1, 1);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(556, '0', 7, 166);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(557, '67', 15, 3);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(558, '84 a', 7, 112);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(559, '79 a', 15, 44);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(560, '29 b', 19, 188);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(561, '32', 17, 24);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(562, '1 a', 12, 133);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(563, '1 a', 17, 76);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(564, '6 a', 20, 2);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(565, '45', 2, 166);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(566, '4', 2, 68);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(567, '6', 21, 153);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(568, '5', 8, 22);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(569, '23', 20, 118);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(570, '16 a', 10, 40);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(571, '00 a', 11, 150);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(572, '0', 8, 17);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(573, '3', 17, 7);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(574, '6', 2, 165);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(575, '69', 21, 193);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(576, '49 a', 14, 65);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(577, '3', 3, 156);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(578, '45 a', 6, 16);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(579, '9 a', 10, 91);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(580, '74', 4, 69);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(581, '1', 19, 175);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(582, '99 a', 1, 19);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(583, '35 a', 3, 142);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(584, '1', 2, 51);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(585, '78 a', 21, 111);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(586, '06', 4, 69);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(587, '4 b', 7, 188);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(588, '54', 7, 107);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(589, '4 a', 5, 173);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(590, '55 a', 7, 177);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(591, '31 b', 9, 15);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(592, '2', 10, 170);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(593, '7', 8, 14);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(594, '3', 1, 100);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(595, '0', 9, 28);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(596, '7', 10, 142);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(597, '7', 17, 22);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(598, '8', 11, 57);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(599, '69', 12, 136);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(600, '4', 21, 92);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(601, '1 b', 2, 85);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(602, '6', 4, 71);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(603, '52', 4, 142);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(604, '05 a', 3, 3);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(605, '45 a', 7, 30);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(606, '3', 7, 29);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(607, '5', 19, 43);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(608, '6 a', 14, 16);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(609, '29', 7, 62);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(610, '8 b', 2, 71);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(611, '9', 12, 180);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(612, '01 a', 20, 175);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(613, '0 b', 15, 181);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(614, '42', 4, 61);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(615, '8', 2, 43);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(616, '5 b', 16, 83);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(617, '17 b', 6, 75);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(618, '1 b', 11, 84);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(619, '67 b', 10, 131);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(620, '84 a', 5, 43);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(621, '62 b', 8, 96);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(622, '97', 15, 123);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(623, '4', 19, 183);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(624, '85 a', 11, 192);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(625, '3 a', 1, 75);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(626, '76', 3, 124);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(627, '7 a', 20, 149);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(628, '8', 22, 149);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(629, '3 a', 10, 134);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(630, '2 b', 8, 175);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(631, '73', 12, 124);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(632, '6', 14, 141);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(633, '70 a', 12, 139);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(634, '3', 9, 51);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(635, '4', 20, 137);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(636, '94', 8, 178);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(637, '92 b', 2, 92);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(638, '3', 3, 184);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(639, '5', 3, 159);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(640, '15', 14, 80);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(641, '75', 9, 124);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(642, '5 b', 4, 161);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(643, '47', 4, 125);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(644, '5', 2, 132);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(645, '42 a', 9, 92);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(646, '19 a', 4, 192);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(647, '06', 6, 155);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(648, '39 b', 19, 194);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(649, '18 b', 8, 123);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(650, '54', 3, 98);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(651, '88', 18, 117);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(652, '2 b', 19, 2);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(653, '85', 2, 34);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(654, '1 a', 9, 15);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(655, '58', 6, 61);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(656, '2 a', 10, 111);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(657, '6', 4, 14);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(658, '66 a', 5, 23);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(659, '0 a', 10, 74);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(660, '82', 14, 26);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(661, '7', 10, 140);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(662, '82', 11, 91);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(663, '0', 4, 102);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(664, '3', 20, 6);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(665, '3 a', 12, 52);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(666, '0 b', 13, 64);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(667, '7 b', 6, 142);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(668, '1', 12, 26);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(669, '7 b', 1, 51);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(670, '63', 17, 170);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(671, '0 b', 7, 64);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(672, '72', 14, 140);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(673, '41 a', 12, 74);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(674, '21', 15, 134);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(675, '6', 4, 193);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(676, '3', 3, 167);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(677, '05', 14, 134);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(678, '01 b', 22, 157);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(679, '33 a', 14, 47);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(680, '16', 16, 21);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(681, '27 a', 11, 175);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(682, '58 b', 13, 78);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(683, '3', 4, 2);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(684, '84', 7, 32);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(685, '47 a', 21, 197);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(686, '93', 11, 39);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(687, '88', 18, 87);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(688, '63', 21, 167);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(689, '9', 10, 27);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(690, '33', 7, 82);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(691, '77 a', 22, 161);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(692, '8', 19, 45);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(693, '36 b', 13, 140);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(694, '3 a', 15, 155);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(695, '47', 20, 176);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(696, '2 b', 4, 143);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(697, '61', 16, 7);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(698, '85', 5, 25);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(699, '2 a', 12, 186);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(700, '62 b', 16, 139);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(701, '73', 1, 194);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(702, '56 b', 20, 132);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(703, '4 a', 16, 182);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(704, '46 b', 7, 10);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(705, '66', 20, 155);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(706, '5 b', 21, 149);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(707, '6 b', 16, 150);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(708, '3 a', 11, 62);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(709, '4', 20, 88);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(710, '3 a', 9, 15);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(711, '0 b', 15, 34);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(712, '3', 20, 10);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(713, '50 a', 15, 176);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(714, '8 a', 7, 33);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(715, '91 a', 3, 78);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(716, '2 b', 1, 140);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(717, '58', 4, 35);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(718, '9 a', 16, 45);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(719, '26', 21, 66);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(720, '59', 11, 42);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(721, '59', 2, 160);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(722, '0 a', 11, 61);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(723, '61', 8, 13);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(724, '5', 9, 130);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(725, '89', 5, 124);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(726, '0 a', 13, 24);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(727, '3', 20, 34);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(728, '36', 2, 45);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(729, '50', 8, 115);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(730, '8', 7, 197);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(731, '74 b', 17, 138);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(732, '9 b', 13, 159);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(733, '0', 13, 43);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(734, '9 a', 4, 136);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(735, '2 b', 12, 150);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(736, '20', 2, 168);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(737, '18', 18, 11);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(738, '5', 2, 47);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(739, '88 a', 3, 122);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(740, '5 b', 11, 197);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(741, '8', 11, 144);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(742, '5', 15, 64);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(743, '6', 13, 101);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(744, '8 b', 16, 96);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(745, '06', 17, 15);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(746, '15 b', 5, 44);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(747, '8', 22, 27);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(748, '94', 17, 155);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(749, '9 a', 3, 102);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(750, '83', 7, 24);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(751, '73 b', 19, 30);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(752, '8 b', 13, 90);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(753, '8', 15, 100);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(754, '38 b', 21, 189);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(755, '7 a', 5, 123);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(756, '8', 4, 135);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(757, '7', 20, 196);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(758, '41', 1, 52);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(759, '9', 3, 161);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(760, '4', 21, 10);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(761, '57', 19, 156);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(762, '58 b', 11, 34);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(763, '0', 2, 9);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(764, '1', 12, 127);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(765, '1 a', 22, 32);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(766, '79', 19, 96);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(767, '15', 2, 33);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(768, '88 b', 5, 131);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(769, '5', 7, 23);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(770, '6 a', 8, 133);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(771, '8', 16, 195);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(772, '60 a', 11, 164);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(773, '57 b', 7, 69);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(774, '75', 19, 43);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(775, '7 b', 12, 108);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(776, '46', 6, 197);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(777, '4 b', 5, 63);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(778, '6', 17, 46);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(779, '4 b', 6, 8);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(780, '2', 17, 20);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(781, '0', 2, 58);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(782, '46 a', 6, 81);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(783, '38 a', 3, 101);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(784, '6', 14, 142);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(785, '4 b', 17, 31);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(786, '75 b', 19, 86);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(787, '6', 17, 147);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(788, '4 a', 1, 147);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(789, '96', 5, 53);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(790, '7', 9, 44);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(791, '2 a', 10, 65);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(792, '08', 14, 173);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(793, '05', 2, 120);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(794, '34 b', 9, 197);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(795, '21', 9, 195);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(796, '06 a', 5, 141);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(797, '23', 7, 145);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(798, '52 a', 13, 125);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(799, '12 b', 1, 34);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(800, '39', 7, 2);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(801, '1', 17, 122);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(802, '5 b', 4, 133);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(803, '18', 21, 194);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(804, '07 a', 17, 144);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(805, '86 a', 1, 165);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(806, '6 a', 8, 142);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(807, '8 b', 6, 196);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(808, '9', 4, 145);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(809, '8 b', 15, 93);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(810, '2 a', 8, 70);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(811, '2 a', 20, 151);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(812, '3', 3, 10);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(813, '0 a', 7, 115);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(814, '84', 9, 2);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(815, '0 b', 10, 37);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(816, '57 b', 3, 95);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(817, '4', 9, 60);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(818, '05', 3, 121);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(819, '4', 16, 151);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(820, '69 b', 19, 8);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(821, '91', 1, 5);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(822, '85 b', 7, 183);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(823, '44', 5, 116);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(824, '81 a', 5, 72);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(825, '63 b', 15, 148);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(826, '54', 8, 104);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(827, '47', 20, 134);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(828, '41', 7, 59);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(829, '1 b', 17, 49);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(830, '9', 8, 83);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(831, '07', 11, 141);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(832, '0 b', 9, 20);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(833, '90 b', 21, 191);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(834, '79', 2, 163);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(835, '7', 21, 31);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(836, '7', 7, 57);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(837, '12', 19, 113);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(838, '70 a', 6, 72);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(839, '84', 7, 182);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(840, '27 b', 15, 27);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(841, '40 a', 3, 69);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(842, '5', 14, 74);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(843, '1', 9, 158);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(844, '0 b', 22, 96);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(845, '70 a', 6, 165);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(846, '34', 19, 131);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(847, '2 b', 2, 62);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(848, '1', 1, 100);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(849, '56', 3, 154);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(850, '8', 14, 8);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(851, '26 a', 4, 9);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(852, '3 a', 7, 12);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(853, '21', 5, 80);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(854, '50', 14, 92);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(855, '3', 14, 171);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(856, '66 b', 11, 193);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(857, '41 a', 14, 156);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(858, '75', 13, 149);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(859, '1', 22, 180);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(860, '4', 20, 178);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(861, '4 b', 9, 92);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(862, '9 a', 20, 186);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(863, '96', 8, 131);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(864, '43', 10, 120);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(865, '1', 12, 106);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(866, '19 b', 16, 12);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(867, '7 b', 7, 190);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(868, '4', 1, 62);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(869, '1', 20, 112);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(870, '01 a', 13, 164);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(871, '6', 14, 62);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(872, '3 a', 14, 159);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(873, '50', 21, 48);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(874, '53', 13, 85);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(875, '4', 6, 79);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(876, '8', 15, 166);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(877, '9 b', 2, 86);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(878, '8 b', 7, 142);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(879, '45', 20, 37);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(880, '77', 13, 93);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(881, '1', 20, 191);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(882, '1', 14, 93);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(883, '7 b', 3, 67);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(884, '4 b', 8, 112);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(885, '57 b', 13, 135);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(886, '1', 12, 198);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(887, '4 a', 14, 65);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(888, '73', 6, 24);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(889, '2', 7, 138);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(890, '20', 7, 124);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(891, '2', 9, 49);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(892, '4', 11, 99);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(893, '74 b', 7, 86);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(894, '85', 13, 182);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(895, '20 a', 4, 177);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(896, '53', 6, 165);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(897, '3', 12, 98);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(898, '7', 15, 13);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(899, '92 b', 14, 1);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(900, '7', 12, 93);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(901, '58 b', 16, 93);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(902, '6', 3, 9);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(903, '8', 16, 57);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(904, '88', 4, 44);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(905, '89', 2, 116);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(906, '3 b', 7, 68);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(907, '8 a', 5, 186);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(908, '8 a', 2, 186);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(909, '8', 8, 4);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(910, '8', 9, 122);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(911, '5', 17, 179);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(912, '4', 8, 197);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(913, '34 a', 4, 11);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(914, '65', 1, 55);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(915, '30 a', 7, 195);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(916, '6 a', 15, 3);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(917, '42', 18, 10);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(918, '4 b', 13, 139);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(919, '2 b', 6, 18);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(920, '64', 21, 162);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(921, '0 a', 2, 147);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(922, '9', 20, 124);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(923, '1', 3, 61);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(924, '13 b', 4, 50);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(925, '86', 6, 149);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(926, '78 b', 13, 11);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(927, '2', 16, 62);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(928, '28', 8, 96);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(929, '7', 4, 80);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(930, '97', 1, 30);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(931, '21', 16, 184);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(932, '7', 19, 139);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(933, '68', 14, 148);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(934, '16 a', 12, 190);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(935, '5', 2, 108);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(936, '79', 3, 149);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(937, '30 b', 6, 89);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(938, '29 b', 20, 135);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(939, '17 a', 3, 95);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(940, '7', 2, 133);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(941, '6', 20, 199);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(942, '86', 19, 47);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(943, '5', 2, 91);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(944, '48', 10, 167);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(945, '6', 10, 154);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(946, '88', 14, 144);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(947, '55 b', 12, 198);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(948, '08 a', 13, 83);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(949, '9 b', 14, 80);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(950, '5', 3, 55);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(951, '7 b', 9, 20);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(952, '86 a', 9, 11);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(953, '50', 4, 40);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(954, '5', 19, 10);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(955, '1', 21, 99);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(956, '8', 4, 1);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(957, '40', 9, 173);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(958, '8', 7, 108);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(959, '6', 17, 184);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(960, '72 a', 4, 21);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(961, '22', 13, 166);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(962, '28 b', 13, 1);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(963, '01', 11, 160);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(964, '6 b', 1, 107);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(965, '94 b', 18, 18);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(966, '19 a', 5, 67);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(967, '34 a', 1, 34);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(968, '13 b', 15, 62);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(969, '74 a', 22, 19);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(970, '68 b', 12, 137);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(971, '2 a', 13, 39);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(972, '51', 8, 92);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(973, '88', 3, 67);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(974, '81', 4, 80);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(975, '8', 14, 41);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(976, '6', 19, 113);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(977, '31 a', 19, 110);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(978, '3', 16, 109);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(979, '9', 2, 145);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(980, '0 a', 10, 37);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(981, '45', 1, 128);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(982, '20', 9, 86);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(983, '5', 4, 4);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(984, '5', 10, 122);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(985, '97', 9, 6);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(986, '6', 19, 176);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(987, '4 b', 17, 14);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(988, '11 b', 6, 136);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(989, '14 a', 14, 103);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(990, '4', 8, 177);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(991, '5', 4, 194);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(992, '35 a', 5, 198);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(993, '8 b', 7, 128);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(994, '5', 4, 87);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(995, '3 a', 6, 99);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(996, '10 a', 9, 136);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(997, '88 b', 8, 7);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(998, '7 b', 22, 122);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(999, '7', 9, 180);
INSERT INTO edificio(id, numero, total_habitantes, id_calle) VALUES
(1000, '87', 15, 80);


insert into calle(id_barrio, nombre)values( 13,'Merchants Street');

insert into edificio(numero,id_calle )values('S/N',last_insert_id());

insert into gremio(nombre, lema, id_edificio) values('Guild of accountants and usurers','',last_insert_id());

insert into calle(id_barrio, nombre)values( 10,'Quilldaisy Stairs');
insert into edificio(numero,id_calle )values('S/N',last_insert_id());
insert into gremio(nombre, lema, id_edificio) values('Guild of actors','',last_insert_id());

insert into calle(id_barrio, nombre)values( 13,'Streets of Alchemists');
insert into edificio(numero,id_calle )values('S/N',last_insert_id());
insert into gremio(nombre, lema, id_edificio) values('Guld of alchemists','Omnis quis coruscat est or',last_insert_id());

insert into calle(id_barrio, nombre)values( 13,'By the wall');
insert into edificio(numero,id_calle )values('27b',last_insert_id());
insert into gremio(nombre, lema, id_edificio) values('Guild of archaelogistst','',last_insert_id());

insert into calle(id_barrio, nombre)values( 10,'Siege Street');
set @id=last_insert_id();
insert into edificio(numero,id_calle )values('S/N',@id);
set @id=last_insert_id();
insert into edificio_privado ( nombre,id_edificio)values('The Armoury',@id);
insert into gremio(nombre, lema, id_edificio) values('Guild of armourers','',@id);

insert into calle(id_barrio, nombre)values( 13,'Filigree Street');
insert into edificio(numero,id_calle )values('S/N',last_insert_id());
insert into gremio(nombre, lema, id_edificio) values('Guild of assassins','Nil mortifi sine lucre',last_insert_id());


insert into calle(id_barrio, nombre)values( 13,'Baker Street');
insert into edificio(numero,id_calle )values('S/N',last_insert_id());
insert into gremio(nombre, lema, id_edificio) values('Guild of bakers','',last_insert_id());


insert into calle(id_barrio, nombre)values( 13,'Ettercap Street');
insert into edificio(numero,id_calle )values('S/N',last_insert_id());
insert into gremio(nombre, lema, id_edificio) values('Guild of barber surgeons','',last_insert_id());


insert into calle(id_barrio, nombre)values( 13,'Whalebone Lane');
insert into edificio(numero,id_calle )values('S/N',last_insert_id());
insert into gremio(nombre, lema, id_edificio) values('Guild of beggars','Moneta superuacanea magister?',last_insert_id());


insert into calle(id_barrio, nombre)values( 13,'Peach Pie Street');
insert into edificio(numero,id_calle )values('S/N',last_insert_id());
insert into gremio(nombre, lema, id_edificio) values('Guild of butchers','',last_insert_id());


insert into calle(id_barrio, nombre)values( 13,'Snapcase Street');
set @id=last_insert_id();
insert into edificio(numero,id_calle )values('S/N',@id);
set @id=last_insert_id();
insert into edificio_privado ( nombre,id_edificio)values('Snapcase House',@id);
insert into gremio(nombre, lema, id_edificio) values("Guild of butlers, valets and gentlemen's gentlemen",'',@id);

insert into calle(id_barrio, nombre)values( 13,'Pigsty Hill');
set @id=last_insert_id();
insert into edificio(numero,id_calle )values('S/N',@id);
set @id=last_insert_id();
insert into edificio_privado ( nombre,id_edificio)values("The Drover's Rest",@id);
insert into gremio(nombre, lema, id_edificio) values('Guild of carters and drovers','',@id);

insert into calle(id_barrio, nombre)values( 13,'Street of Cunning Artificers');
insert into edificio(numero,id_calle )values('S/N',last_insert_id());
insert into gremio(nombre, lema, id_edificio) values('Guild of clock and watch makers','',last_insert_id());

insert into calle(id_barrio, nombre)values( 13,'So-So Street');
insert into edificio(numero,id_calle )values('S/N',last_insert_id());
insert into gremio(nombre, lema, id_edificio) values('Guild of ecdysiasts, nautchers, cancaniÃ¨res and exponents of exotic dance','',last_insert_id());


insert into edificio(numero,id_calle )values('S/N',(select id from calle where nombre='Peach Pie Street'));
insert into gremio(nombre, lema, id_edificio) values('Guild of embalmers and allied trades','',last_insert_id());

insert into calle(id_barrio, nombre)values( 13,'Widdershins Broadway');
insert into edificio(numero,id_calle )values('S/N',last_insert_id());
insert into gremio(nombre, lema, id_edificio) values('Guild of fools and joculators and college of clowns','',last_insert_id());

insert into edificio(numero,id_calle )values('S/N',(select id from calle where nombre='Street of Cunning Artificers'));
insert into gremio(nombre, lema, id_edificio) values('Guild of glassblowers','',last_insert_id());

insert into calle(id_barrio, nombre)values( 13,'Treacle Mine Road');
insert into edificio(numero,id_calle )values('S/N',last_insert_id());
insert into gremio(nombre, lema, id_edificio) values('Guild of historians','',last_insert_id());

insert into calle(id_barrio, nombre)values( 13,'Knocker-up Street');
insert into edificio(numero,id_calle )values('27',last_insert_id());
insert into gremio(nombre, lema, id_edificio) values('Association of knockers-up','',last_insert_id());

insert into calle(id_barrio, nombre)values( 13,'Fount Yard');
insert into edificio(numero,id_calle )values('S/N',last_insert_id());
insert into gremio(nombre, lema, id_edificio) values('Guild of launderers','',last_insert_id());

insert into calle(id_barrio, nombre)values( 13,"Pleader's Row");
insert into edificio(numero,id_calle )values('S/N',last_insert_id());
insert into gremio(nombre, lema, id_edificio) values('Guild of lawyers','',last_insert_id());


insert into edificio(numero,id_calle )values('S/N',(select id from calle where nombre='Baker Street'));
insert into gremio(nombre, lema, id_edificio) values('Guild of merchants','',last_insert_id());

insert into edificio(numero,id_calle )values('S/N',(select id from calle where nombre="Pleader's Row"));
insert into gremio(nombre, lema, id_edificio) values('Guild of plumbers and dunnykin divers','',last_insert_id());

insert into calle(id_barrio, nombre)values( 13,"Sheer Street");
insert into edificio(numero,id_calle )values('S/N',last_insert_id());
insert into gremio(nombre, lema, id_edificio) values('Guild of seamstresses','Nil volupti sine lucre',last_insert_id());

insert into calle(id_barrio, nombre)values( 13,"Easy Street");
insert into edificio(numero,id_calle )values('S/N',last_insert_id());
insert into gremio(nombre, lema, id_edificio) values('The federation of sedan chair carriers','',last_insert_id());

insert into edificio(numero,id_calle )values('S/N',(select id from calle where nombre="Merchants Street"));

insert into gremio(nombre, lema, id_edificio) values('Guild of shoemakers and leatherworkers','',last_insert_id());

insert into edificio(numero,id_calle )values('S/N',(select id from calle where nombre="The Street of Alchemist"));
insert into gremio(nombre, lema, id_edificio) values('Guild of thieves, burglars and allied trades','Acutus id verbarat',last_insert_id());

insert into calle(id_barrio, nombre)values( 13,'The Goose Gate');
set @id=last_insert_id();
insert into edificio(numero,id_calle )values('S/N',@id);
set @id=last_insert_id();
insert into edificio_privado ( nombre,id_edificio)values("Old Goosegate Tavern",@id);
insert into gremio(nombre, lema, id_edificio) values('Guild of town criers','',@id);

insert into calle(id_barrio, nombre)values( 13,"Esoteric Street");
insert into edificio(numero,id_calle )values('24b',last_insert_id());
insert into gremio(nombre, lema, id_edificio) values('Guild of trespassers','',last_insert_id());


insert into calle(id_barrio, nombre)values( 2,'Unseen University Street');
set @id=last_insert_id();
insert into edificio(numero,id_calle )values('S/N',@id);
set @id=last_insert_id();
insert into edificio_privado ( nombre,id_edificio)values("Unseen University",@id);
insert into gremio(nombre, lema, id_edificio) values('Unseen University','Nunc id vides, nunc ne vides',last_insert_id());



--
-- Inserting data into table ciudadano_gremio
--
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(37, 7, '2001-05-03', '1976-12-22', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 29, '1998-11-03', '1970-01-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(21, 14, '1997-04-03', '1970-01-09', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(21, 19, '2019-12-27', '1975-03-15', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(28, 10, '1998-04-26', '1970-01-01', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(43, 10, '2014-02-18', '2001-05-03', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(56, 30, '1995-04-02', '2018-03-13', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(38, 8, '2006-01-14', '1982-04-04', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(28, 1, '1971-06-17', '1970-12-17', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(44, 30, '1970-10-21', '1985-10-24', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 25, '1973-10-12', '1975-10-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(22, 15, '1978-10-06', '1993-07-10', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 7, '1992-07-27', '1975-03-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(36, 26, '1970-01-06', '1971-05-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(23, 16, '1982-09-28', '2008-02-24', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(39, 9, '2008-12-05', '1972-02-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(37, 27, '1971-05-27', '1993-02-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(49, 27, '1990-08-01', '1970-01-03', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 8, '1980-12-05', '1982-01-25', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 2, '2020-01-08', '1975-07-18', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 11, '1991-03-05', '1993-08-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(16, 1, '2020-02-23', '1970-01-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(40, 10, '2016-12-29', '1970-01-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 30, '1995-05-13', '1981-05-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 12, '2019-11-19', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(24, 17, '1990-04-23', '1984-08-17', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(38, 28, '1991-02-03', '2001-10-11', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(17, 2, '2016-07-17', '1972-06-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(50, 28, '1970-01-04', '1970-01-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 9, '1980-09-11', '1971-07-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(25, 18, '2019-11-10', '2003-12-28', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 3, '1987-01-01', '1998-02-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(22, 20, '1997-10-02', '1995-02-20', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(51, 29, '1971-07-03', '1971-06-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(36, 10, '1986-01-10', '1978-12-31', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 13, '1995-11-19', '1970-02-23', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(52, 30, '2003-09-04', '1972-06-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(39, 29, '1991-05-13', '2016-07-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(26, 19, '1996-12-29', '1970-01-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(7, 1, '1988-02-13', '1999-11-14', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(41, 11, '1990-06-10', '1970-04-08', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 4, '1995-10-22', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(37, 11, '2016-05-08', '2012-01-27', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(24, 1, '1978-02-20', '1975-05-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(23, 21, '2019-02-03', '1971-06-16', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(18, 3, '1985-04-22', '1973-11-20', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(42, 12, '1970-01-04', '2005-10-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 5, '1970-02-13', '1975-04-02', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(43, 13, '2011-02-18', NULL, 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(38, 12, '1982-05-29', '1970-11-01', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 6, '2008-11-14', '1977-05-29', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 14, '1975-12-22', '1983-03-26', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(19, 4, '1972-09-02', '1976-01-22', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(8, 2, '1998-05-04', '2016-10-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(40, 30, '1970-01-02', '2018-03-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(25, 2, '1972-08-14', '1970-02-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(27, 20, '1973-08-16', '1972-12-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(24, 22, '1980-06-05', '2006-05-12', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(44, 14, '2000-05-05', '1988-10-19', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(39, 13, '1986-07-10', '1993-03-15', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 15, '1987-10-14', '1973-04-21', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(9, 3, '2011-01-02', '2010-11-26', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 7, '1970-03-25', '1988-05-08', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(20, 5, '1979-12-11', '1991-01-14', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(45, 15, '2000-08-14', '1981-11-17', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(25, 23, '2010-10-24', '1988-08-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(10, 4, '2002-11-09', '2018-11-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(26, 24, '1995-05-30', '2002-10-18', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(26, 3, '1970-01-03', '1970-04-08', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 8, '1970-03-10', '1989-03-23', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(12, 1, '2014-12-04', '2005-11-24', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(11, 5, '1988-04-04', '1988-10-19', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(28, 21, '2002-05-31', '1971-07-25', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 16, '2011-04-21', '1970-01-01', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(46, 16, '1975-11-28', '2013-04-25', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(21, 6, '2003-06-12', '2012-02-14', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(36, 9, '1986-05-22', '1972-08-20', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(13, 2, '1980-05-07', '2002-09-24', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(47, 17, '2009-04-25', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(37, 10, '1974-03-26', '1970-01-01', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 22, '1990-04-26', '2001-09-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(14, 3, '1986-11-22', '2001-05-25', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(40, 14, '1972-11-18', '1996-01-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(27, 25, '1970-02-14', '2016-04-11', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(27, 4, '1992-02-21', '1970-04-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(41, 15, '2005-09-15', '1970-01-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 23, '2000-04-01', '1981-07-04', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(48, 18, '1971-02-11', '1995-04-03', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 17, '2003-12-03', '1972-09-15', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(12, 6, '1971-06-25', '1982-07-17', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(15, 4, '1995-01-11', '2014-11-26', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(28, 5, '1987-05-20', '1970-12-30', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 24, '2000-10-09', '2020-03-12', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(42, 16, '1972-06-05', '1996-12-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(22, 7, '2013-04-25', '1981-09-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(38, 11, '1970-02-08', '2010-11-16', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(49, 19, '1994-05-14', '1970-06-16', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(28, 26, '1970-01-03', '1973-04-27', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 6, '1970-02-20', '1971-02-21', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(13, 7, '2005-05-04', '2018-02-03', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(16, 5, '1990-07-06', '1972-07-16', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(39, 12, '2018-07-31', '1994-01-24', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 25, '1980-12-22', '1989-04-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 27, '2007-08-17', '1970-03-24', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(36, 18, '2020-01-26', '1987-04-23', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(50, 20, '1986-01-30', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(17, 6, '1990-12-15', '2008-11-16', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(40, 13, '1995-06-17', '1995-01-31', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 26, '1982-01-13', '1990-08-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(43, 17, '1992-02-15', '1970-01-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 7, '1980-07-26', '1993-03-31', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(51, 21, '2007-01-07', '2005-09-29', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(14, 8, '1991-06-11', '2007-04-28', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(41, 14, '1975-06-08', '1976-09-11', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(23, 8, '2016-12-31', '2019-03-03', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(18, 7, '1992-09-19', '1971-12-03', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(37, 19, '1988-07-14', '2012-03-13', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(52, 22, '2004-03-20', '2001-11-13', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(42, 15, '1980-05-30', '1970-01-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 27, '1971-05-17', '1970-01-01', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(44, 18, '2000-10-11', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(19, 8, '1970-02-18', '1993-09-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 8, '2012-04-20', '1992-04-29', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(24, 9, '1988-04-28', '1970-04-07', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(38, 20, '2016-11-13', '2009-09-27', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(25, 10, '1971-04-11', '2019-02-13', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(45, 19, '1970-01-09', '1970-09-23', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(39, 21, '2013-10-15', '1996-12-25', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 28, '1991-11-02', '1970-02-19', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(15, 9, '1981-09-19', '2008-05-30', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 9, '2006-05-18', '1993-11-17', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(53, 23, '2008-05-12', '2010-10-05', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(26, 11, '1987-07-20', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(43, 16, '1970-01-05', '1986-08-22', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(46, 20, '1986-03-12', '2006-10-15', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 28, '1977-06-26', '1970-02-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(54, 24, '1980-06-07', '1992-07-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(40, 22, '1986-12-09', '1999-03-24', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 10, '1996-08-30', '1980-01-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(44, 17, '1980-06-12', '1995-01-15', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(27, 12, '1975-02-28', '2007-10-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(41, 23, '2015-01-20', '1998-04-25', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(28, 13, '1971-04-08', '1996-04-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(47, 21, '1970-04-01', '1976-06-03', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(20, 9, '1970-01-13', '1970-03-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(36, 29, '1975-09-08', '2015-12-03', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(42, 24, '2009-11-18', '1996-06-09', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 14, '1979-11-05', '1993-01-23', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(21, 10, '1980-07-22', '1970-02-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(37, 30, '1971-12-02', '1991-04-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(22, 11, '1970-02-23', '2000-05-15', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 29, '2002-01-26', '2011-01-13', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(9, 1, '1972-08-31', '1982-03-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(43, 25, '2002-07-07', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(23, 12, '1970-03-31', '1986-09-24', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(55, 25, '1990-05-10', '2009-04-13', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(10, 2, '2013-03-13', '1970-01-14', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(45, 18, '1970-01-07', '1970-03-20', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 15, '1999-07-14', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(56, 26, '1987-09-30', '2001-02-05', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(24, 13, '1978-11-02', '1970-01-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 11, '2014-11-10', '2001-12-26', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(46, 19, '1970-11-13', '1981-03-18', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(11, 3, '1981-01-31', '1974-07-30', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(16, 10, '1995-09-24', '2006-04-30', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(57, 27, '2001-12-18', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(44, 26, '2009-06-20', '1975-11-22', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(25, 14, '1983-12-13', '1990-12-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 30, '2013-06-05', '1972-12-18', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(17, 11, '1970-02-01', '2012-06-11', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 16, '2005-06-07', '2009-01-18', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(47, 20, '1972-04-28', '1990-08-30', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(12, 4, '1993-09-28', '1979-09-19', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(58, 28, '2010-12-21', '1971-11-04', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(48, 22, '1996-10-22', '1982-07-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(26, 15, '1976-08-27', '1972-07-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 12, '1991-06-01', '2015-11-25', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(48, 21, '1979-12-25', '2009-06-18', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(4, 1, '1970-01-08', '2007-02-01', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(13, 5, '2009-04-04', '1990-11-23', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(18, 12, '1982-06-09', '1995-12-18', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(27, 16, '1989-07-23', '1970-03-24', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(14, 6, '1970-01-14', '1976-08-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(59, 29, '2012-03-04', '1970-01-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(49, 23, '1984-08-01', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(36, 13, '1977-04-13', '1985-07-10', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(50, 24, '1986-01-02', '1970-01-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(5, 2, '2000-02-17', '1998-08-17', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(45, 27, '1996-02-18', '1980-03-03', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(49, 22, '2007-03-14', '1991-09-25', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(60, 30, '2011-06-28', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(50, 23, '1983-09-08', '2015-07-11', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(19, 13, '2005-01-19', '2017-03-11', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 1, '1996-12-19', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(37, 14, '1994-06-25', '1970-10-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(51, 24, '2016-10-16', '2000-10-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(24, 4, '1975-08-22', '2016-03-31', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(28, 17, '2020-04-25', '1970-02-11', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(38, 15, '1970-01-07', '1975-06-03', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(15, 7, '1987-05-05', '1987-12-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(25, 5, '2001-07-04', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 18, '1986-11-15', '2006-03-12', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(16, 8, '1977-09-13', '1970-03-21', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(6, 3, '1972-04-30', '1989-01-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 2, '1992-10-03', '2014-02-19', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 19, '1985-09-28', '1970-01-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(39, 16, '1972-07-24', '1975-11-21', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(17, 9, '2013-03-05', '1988-04-01', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(26, 6, '1972-09-17', '2016-10-20', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(20, 14, '1971-11-28', '1988-06-24', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(7, 4, '1996-07-26', '1983-11-12', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 20, '2006-07-26', '2013-03-01', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(40, 17, '1970-03-08', '1989-11-28', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(21, 15, '2014-04-20', '1994-06-24', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(27, 7, '1997-10-10', '2011-07-24', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(52, 25, '2001-04-24', '1977-09-25', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(18, 10, '1970-01-09', '2016-01-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 3, '1979-01-15', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(41, 18, '1972-09-16', '2004-08-11', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 21, '2010-01-03', '2004-06-19', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(19, 11, '1976-08-17', '2011-03-17', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 22, '2020-03-26', '1970-01-24', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(28, 8, '2019-01-07', '1970-02-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(20, 12, '1971-10-31', '2004-07-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(8, 5, '1971-04-19', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 17, '1971-01-18', '1987-08-08', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(53, 26, '1984-01-22', '2011-05-28', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(46, 28, '2006-08-31', '1982-03-02', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 23, '1970-02-23', '2018-04-19', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 4, '2012-02-17', '1991-07-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(42, 19, '2014-08-11', '1971-10-03', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 18, '2012-02-03', '1992-07-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(21, 13, '1970-03-26', '1970-01-13', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 9, '2007-09-08', '1987-08-15', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(47, 29, '2000-01-30', '1972-09-01', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(43, 20, '2005-07-28', '2009-09-25', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(22, 16, '1970-01-09', '1991-11-30', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(9, 6, '2009-06-29', '1993-10-13', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(23, 17, '1970-03-21', '2003-02-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 24, '1982-11-24', '1996-11-22', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(54, 27, '1986-08-05', '1989-06-20', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(22, 14, '1995-04-07', '1970-03-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(36, 25, '1970-01-05', '1984-04-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 19, '1970-01-06', '1970-01-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 10, '2010-05-04', '1996-06-14', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(48, 30, '2012-02-27', '1970-02-20', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(23, 15, '1975-01-23', '2007-01-26', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(36, 5, '2007-06-07', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(10, 7, '1991-08-17', '1990-03-13', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(44, 21, '1970-12-04', '1982-12-24', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 20, '1974-12-25', '1973-09-16', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 11, '1970-04-03', '1984-12-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(45, 22, '1971-10-21', '1999-05-13', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(37, 26, '2012-01-06', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(24, 18, '1986-04-01', '2015-03-22', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(20, 1, '2018-10-20', '2013-09-16', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(36, 21, '1970-02-22', '1992-10-16', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(55, 28, '1988-10-31', '1979-02-26', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(11, 8, '2015-05-29', '1970-01-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(24, 16, '1990-02-04', '1990-09-26', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(21, 2, '1973-03-04', '1970-04-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(38, 27, '2003-09-08', '2017-11-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(25, 19, '1989-11-16', '1970-08-14', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 12, '2000-01-17', '1974-03-02', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(37, 22, '1973-11-19', '1970-01-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(46, 23, '2009-09-11', '1978-07-12', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(12, 9, '1992-01-04', '1970-01-07', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(37, 6, '2018-08-24', '1979-07-27', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(22, 3, '1991-07-25', '2010-06-01', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(56, 29, '1993-11-16', '1992-06-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(38, 7, '1980-04-26', '1986-11-23', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(25, 17, '2008-10-18', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(57, 30, '1987-12-16', '1996-01-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(39, 28, '2019-12-21', '2018-04-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(38, 23, '1992-09-27', '1970-03-23', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(39, 8, '1979-12-04', '1993-03-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(26, 20, '1970-01-05', '1970-01-28', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(26, 18, '1971-04-09', '1970-03-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 13, '1972-01-13', '1992-08-18', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(47, 24, '1970-09-14', '1970-01-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(13, 10, '2001-07-28', '1986-07-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(27, 21, '1992-04-01', '1987-11-08', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(40, 29, '2003-11-14', '2010-12-03', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(23, 4, '1970-02-01', '1994-09-19', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(39, 24, '2003-11-16', '1997-09-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 1, '1999-12-19', '1994-07-21', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(40, 9, '1970-01-11', '1972-06-25', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(14, 11, '2010-03-02', '2008-11-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 14, '1972-02-29', '1994-11-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(1, 1, '1978-04-04', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(27, 19, '1998-06-21', '1995-11-12', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(41, 30, '1977-10-01', '1970-01-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(15, 12, '1996-07-31', '2015-12-23', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(48, 25, '1991-01-25', '2008-01-28', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 2, '1983-07-23', '1992-04-15', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(24, 5, '2005-01-12', '2006-02-19', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(28, 20, '1995-12-26', '1990-04-26', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 15, '2020-01-25', '2007-01-24', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(2, 2, '1979-09-20', '2020-04-19', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(41, 10, '1970-01-02', '1970-03-05', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(40, 25, '1971-07-14', NULL, 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(13, 1, '2015-05-30', '1973-01-12', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 21, '1970-04-01', '1973-02-15', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(16, 13, '1982-02-13', '1970-02-26', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 3, '1970-03-19', '1970-11-01', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(3, 3, '1982-06-12', '1970-02-27', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(42, 11, '1970-01-09', '1970-01-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(14, 2, '1970-05-26', '1991-08-31', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(49, 26, '1979-11-07', '1994-04-21', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(17, 14, '1986-09-13', '1971-05-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(25, 6, '1980-10-25', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 22, '1970-01-09', '2020-02-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(4, 4, '1970-01-07', '1971-08-11', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(15, 3, '1988-08-26', '1971-07-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 23, '2006-12-24', '1978-01-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(41, 26, '2001-11-27', '1999-11-20', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 4, '1982-07-01', '1995-09-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(26, 7, '1970-01-10', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(18, 15, '2005-11-28', NULL, 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(42, 27, '1981-08-20', '1970-02-06', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(43, 12, '1972-07-31', '1987-08-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 5, '1974-09-21', '1979-01-31', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(16, 4, '1976-11-21', '2000-07-29', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(27, 8, '2019-10-05', '1970-03-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(36, 16, '2002-11-12', '2009-08-22', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(50, 27, '1981-12-30', '1972-05-17', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(5, 5, '2005-09-08', '1986-06-20', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 24, '2016-10-02', '2017-03-12', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(43, 28, '1970-03-08', '1994-08-13', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(44, 13, '1970-01-07', '2019-04-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(37, 17, '1988-08-14', '1983-11-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(17, 5, '1974-12-19', '1980-05-28', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 25, '1970-01-07', '1983-01-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 6, '1970-01-05', '2005-08-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(19, 16, '1979-04-26', '1972-11-22', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(45, 14, '2003-12-13', '1996-07-12', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(51, 28, '1973-10-22', '1998-01-12', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(18, 6, '2000-07-12', '1982-08-13', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(28, 9, '2010-05-02', '1970-01-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 26, '1971-03-13', '1970-07-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(6, 6, '1994-12-23', '2018-03-23', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(44, 29, '1986-11-03', '1984-05-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(19, 7, '1970-07-30', '1973-03-28', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(38, 18, '2002-08-18', '1982-12-17', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(52, 29, '2017-12-22', '1991-10-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 7, '1977-12-22', '1971-03-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(20, 17, '1994-02-01', '1976-11-12', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(39, 19, '1970-03-26', '2018-01-15', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 10, '1995-11-19', '1970-01-03', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(45, 30, '1987-01-13', '2010-10-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(53, 30, '2006-02-13', '1970-07-23', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(40, 20, '1971-09-26', '1977-02-19', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 11, '1970-03-25', '2013-02-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(17, 1, '2003-03-04', '1978-01-02', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(7, 7, '1970-01-25', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 12, '2012-09-29', '1986-10-14', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(21, 18, '1988-08-27', '1985-04-25', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(46, 15, '1987-02-21', '1984-07-15', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 27, '1970-01-10', '1970-01-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(20, 8, '1992-02-02', '1987-11-28', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(36, 8, '1971-02-14', '1993-11-20', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(25, 1, '2006-11-08', '1971-05-24', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(47, 16, '1970-04-10', '1990-11-15', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(37, 9, '2001-11-12', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(36, 28, '2010-11-09', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(8, 8, '1979-02-17', '1993-03-14', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(22, 19, '1977-05-13', '1978-04-23', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(21, 9, '2014-09-08', '1970-10-22', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(37, 29, '1994-09-08', '1976-06-13', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(18, 2, '2016-04-27', '1970-08-17', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(41, 21, '2007-03-05', '1998-10-21', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(26, 2, '2008-08-20', '2008-10-14', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(9, 9, '1999-09-03', '1973-03-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(48, 17, '1980-06-02', '1970-01-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 13, '1977-03-26', '2014-01-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(22, 10, '1970-09-15', '1970-01-12', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(42, 22, '2016-05-11', '1970-01-01', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(27, 3, '1980-04-17', '2010-04-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(43, 23, '1984-05-30', '1988-08-17', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(28, 4, '1971-06-15', '1974-10-21', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(19, 3, '1993-02-05', '1970-03-20', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 14, '1980-03-19', '1985-03-20', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(44, 24, '1970-01-10', '1984-10-17', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(38, 10, '1971-03-13', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(38, 30, '1988-11-10', '2008-05-29', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(49, 18, '1974-07-26', '1983-06-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(39, 11, '1970-01-13', '2006-11-16', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 5, '1990-01-02', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(45, 25, '2007-10-11', '1970-01-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 6, '1980-03-20', '2009-01-24', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(46, 26, '2017-10-10', '2014-10-27', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(50, 19, '1970-01-07', '1970-01-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(40, 12, '1992-08-23', '1998-07-25', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(23, 11, '1970-07-19', '1995-01-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(20, 4, '2003-12-01', '1970-01-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 15, '2005-12-27', '1984-10-01', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(21, 5, '1970-01-11', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 16, '1970-12-06', '2006-09-08', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(10, 1, '2011-09-13', '1999-05-30', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 7, '1976-04-23', '1983-01-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(51, 20, '1971-05-21', '2010-02-21', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(41, 13, '1994-12-20', '2005-01-12', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(22, 6, '2000-02-28', '1970-10-25', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(23, 20, '1970-04-05', '1972-12-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(47, 27, '1970-01-08', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(10, 10, '1970-06-05', '2019-12-01', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 8, '1980-06-19', '2016-11-14', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(48, 28, '2015-10-31', '2000-01-26', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(24, 21, '1974-04-02', '1970-02-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 9, '2004-08-23', '2009-07-24', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(52, 21, '1971-04-11', '1970-01-03', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(36, 17, '2004-01-04', '1971-05-30', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(11, 11, '1983-06-17', '1970-01-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(23, 7, '1992-02-27', '2016-12-30', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(42, 14, '1976-02-18', '2007-08-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(37, 18, '2019-02-07', '1976-03-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(53, 22, '1997-12-19', NULL, 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(43, 15, '1989-01-11', '1982-05-25', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(49, 29, '1992-08-20', '1994-06-16', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(24, 8, '2015-02-07', '1971-05-13', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(25, 22, '1989-01-23', '1974-07-07', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(24, 12, '1990-02-13', NULL, 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(38, 19, '2009-09-30', '1981-02-01', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 10, '2013-11-16', '1993-08-17', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(50, 30, '1970-01-22', '1970-01-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(54, 23, '2013-06-26', '2005-08-03', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 11, '2008-03-17', '2015-01-12', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(22, 1, '1985-05-04', '1972-12-11', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(11, 2, '1993-05-10', '1995-06-22', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(44, 16, '1972-06-29', '2011-10-11', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(55, 24, '2003-11-14', '1989-03-30', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(12, 12, '1999-08-15', '1970-01-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(25, 13, '1981-01-16', '1971-06-14', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(26, 23, '1971-04-17', '2004-11-12', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(45, 17, '1974-10-21', '1970-01-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(36, 12, '2000-01-18', '2013-11-27', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(13, 13, '1980-12-24', '1975-08-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(23, 2, '1970-03-18', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(12, 3, '2001-03-20', '1972-12-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(27, 24, '1984-08-05', '1973-05-05', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(26, 14, '2008-11-22', '1971-05-12', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(13, 4, '1981-10-12', '1998-06-24', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(37, 13, '2001-06-11', '1981-09-07', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(14, 14, '2009-03-13', '1973-05-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(24, 3, '1993-11-29', '2005-06-13', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(27, 15, '2018-07-05', '1970-01-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(38, 14, '1995-06-13', NULL, 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(14, 5, '1995-10-04', '2014-12-11', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(25, 9, '1972-10-25', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(56, 25, '1981-03-23', '1994-12-26', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(25, 4, '1990-11-01', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(39, 15, '1977-12-14', '1972-12-21', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(46, 18, '1997-11-29', '1972-01-29', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(57, 26, '1984-08-07', '2010-06-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(39, 20, '1970-01-08', '2013-08-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(26, 10, '1970-04-10', '1983-11-13', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(28, 16, '1972-01-08', '1976-08-17', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(47, 19, '1975-09-23', '1970-01-08', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(40, 21, '1970-01-06', '2019-06-26', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(58, 27, '2001-08-06', '1975-03-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(27, 11, '1970-03-04', '2018-12-11', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(48, 20, '1996-03-18', '2011-08-22', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(15, 6, '1970-01-01', '1979-12-15', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(28, 25, '2004-08-13', '1974-04-22', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(59, 28, '2016-01-21', '1980-09-12', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(26, 5, '2009-07-22', '1991-03-21', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(15, 15, '1991-04-20', '1987-11-22', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 26, '1990-12-29', '1970-01-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(40, 16, '2017-05-13', '2007-06-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(16, 16, '1975-04-02', '2008-09-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(41, 22, '1970-01-25', '2007-12-19', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 27, '1972-04-30', '2014-03-24', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(27, 6, '1981-05-17', '2018-09-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(28, 12, '1970-01-05', '1970-12-19', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 17, '1970-01-10', '1993-12-20', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(41, 17, '1979-09-30', '1970-05-19', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(28, 7, '1995-09-01', '1970-02-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(42, 23, '2004-03-09', '2011-11-21', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(49, 21, '1981-04-25', '2004-10-16', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(16, 7, '1979-11-09', '1981-02-18', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(17, 17, '2001-04-06', '1975-11-15', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 18, '1987-07-04', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(17, 8, '1988-08-19', '1972-08-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 13, '2012-10-11', '2005-02-15', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 19, '1970-01-02', '2012-11-12', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(18, 9, '2018-08-07', '2007-01-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 20, '1970-01-04', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(43, 24, '2001-03-21', '2019-10-30', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 14, '1996-04-13', '2006-01-26', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(19, 10, '1989-11-09', '1993-09-14', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 21, '2007-09-26', '1992-09-20', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 28, '2016-11-10', '1970-01-03', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(20, 11, '1971-04-23', '2000-12-21', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 22, '1970-01-01', '2020-04-24', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(42, 18, '1993-08-18', '1978-09-01', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(21, 12, '1973-04-07', '1997-02-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(18, 18, '1986-07-16', '1973-02-21', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(44, 25, '1970-11-16', '1991-07-01', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 23, '1995-04-21', '1989-03-27', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(60, 29, '1986-05-02', '1989-11-18', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(22, 13, '1972-08-30', '1971-09-16', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 8, '2001-11-27', NULL, 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(43, 19, '1979-05-25', '2012-07-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(36, 24, '2019-02-01', '2015-04-28', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 15, '1991-10-10', '1973-03-23', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(45, 26, '2004-03-03', '1972-03-22', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(50, 22, '1970-01-07', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(23, 14, '2010-06-22', '1984-09-23', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 16, '2007-06-09', '1978-10-11', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(61, 30, '1970-02-25', '1994-06-16', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(51, 23, '1970-03-21', '2009-04-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 1, '1977-10-17', '1984-02-10', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(52, 24, '2016-06-15', '1974-10-18', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 29, '1993-10-30', '1980-08-26', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 2, '1998-02-03', '1986-01-30', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(19, 19, '1987-08-17', '1972-07-26', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 9, '1985-04-20', '2010-04-16', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(53, 25, '1987-10-12', '1974-02-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(44, 20, '1973-06-17', '2009-06-25', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(46, 27, '2011-08-25', '1997-12-13', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 17, '1991-11-12', '1977-06-19', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(47, 28, '1986-06-19', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 10, '1970-03-19', '1970-03-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(37, 25, '1994-02-24', '2016-10-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(45, 21, '1979-05-07', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(24, 15, '1999-06-14', '1981-01-21', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 3, '1990-07-13', '1991-07-29', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 18, '1979-10-26', '2012-01-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(54, 26, '2011-11-12', '1970-02-17', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 30, '1970-02-16', '1971-07-27', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 11, '1994-05-20', '2004-08-17', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(20, 20, '2007-02-26', '1984-03-26', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(5, 1, '2019-11-09', '2018-04-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(36, 4, '1970-03-17', '1980-10-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(55, 27, '2007-12-18', '1991-09-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(48, 29, '1979-01-10', '2014-11-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 19, '1970-01-08', '1970-01-11', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(21, 21, '1982-01-21', '1976-04-21', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(37, 5, '2020-03-19', '1972-10-26', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(56, 28, '1972-05-09', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(38, 26, '1980-01-12', '2019-02-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(6, 2, '1981-09-26', '1979-11-26', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(38, 6, '1989-09-04', '1970-07-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(46, 22, '1987-03-26', '1970-01-07', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(25, 16, '1989-07-26', '1970-05-17', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(49, 30, '1970-01-02', '1990-08-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(39, 27, '1986-11-22', '2004-04-16', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(22, 22, '2002-02-09', '1972-03-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(36, 20, '1972-10-13', '1975-07-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(21, 1, '1971-08-27', '1970-01-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 12, '1981-09-11', '1975-01-03', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(26, 17, '2007-04-12', '1970-01-18', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(57, 29, '1983-11-30', '1993-06-26', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(47, 23, '1991-08-25', '2016-02-24', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(37, 21, '1992-07-25', '1992-01-01', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(39, 7, '1970-01-02', '1970-05-30', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(7, 3, '1993-12-08', '1970-04-10', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(40, 28, '1981-06-27', '1999-04-25', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(22, 2, '2018-10-06', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(23, 23, '1984-10-20', NULL, 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 13, '1986-03-16', '1998-10-01', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(38, 22, '1972-06-13', '2020-04-11', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(23, 3, '1973-03-12', '1970-01-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(39, 23, '1990-03-09', '1991-08-01', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(27, 18, '1997-09-14', '1974-09-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(58, 30, '1988-06-12', '1979-12-19', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(48, 24, '1970-06-20', '2014-04-22', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(40, 8, '1996-04-07', '1972-08-31', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(41, 29, '1973-02-27', '2007-12-11', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(28, 19, '1970-03-11', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 14, '1970-01-07', '1994-11-11', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(26, 13, '1970-01-05', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 1, '2002-01-29', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(41, 9, '1993-03-16', '2015-06-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(42, 30, '1988-05-29', '1970-01-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 20, '2016-03-24', '1994-12-20', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(40, 24, '1970-02-17', '1998-03-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 2, '1970-01-10', '1997-08-17', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(8, 4, '2000-10-17', '1972-08-29', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(14, 1, '1991-12-20', '1978-11-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(24, 24, '1970-01-01', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(49, 25, '1981-12-25', '1970-02-09', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(42, 10, '1976-11-21', '2009-06-29', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 21, '1981-01-28', '1970-02-17', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(15, 2, '2017-07-08', '2020-03-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 22, '2015-01-21', '1970-01-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(16, 3, '1970-02-12', '1972-06-16', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(27, 14, '1982-08-29', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(41, 25, '1977-07-21', '1990-07-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 3, '1982-08-20', '1970-01-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(28, 15, '1971-11-04', '1970-11-22', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(9, 5, '1977-03-21', '1990-01-12', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(36, 15, '2008-07-15', '1971-03-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(43, 11, '1991-12-30', '1987-12-11', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(25, 25, '1999-06-08', '1986-05-29', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 4, '1970-07-02', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(42, 26, '2020-04-05', '1997-01-22', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 16, '1980-02-15', '1970-01-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 23, '1983-12-15', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(17, 4, '1994-05-04', '2003-01-20', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(44, 12, '2001-03-25', '1993-03-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 24, '1985-04-02', '2011-09-24', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 5, '1989-04-24', '1985-10-25', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(10, 6, '2006-11-15', '1979-12-17', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(45, 13, '2015-11-23', '2005-12-21', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(26, 26, '1974-11-21', '1970-01-31', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(43, 27, '1985-01-11', '1984-04-14', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 6, '1970-03-24', '1970-01-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(18, 5, '2014-09-07', '1984-11-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(11, 7, '1972-11-22', '1970-12-25', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 17, '1979-10-10', '2010-10-18', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(27, 27, '1970-01-08', '1972-04-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 25, '1971-09-06', NULL, 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(19, 6, '1997-11-06', '1970-06-25', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(12, 8, '1971-02-23', '1970-04-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(28, 28, '1978-11-17', '1992-03-12', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(44, 28, '1988-08-11', '1971-03-20', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 26, '1970-04-10', '1970-01-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(13, 9, '1972-09-19', '1989-04-02', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(46, 14, '1995-10-15', '2014-07-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(50, 26, '1995-07-24', '1970-01-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(20, 7, '1977-07-16', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 18, '1971-11-28', '2004-02-26', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(36, 27, '1970-01-28', '2016-12-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(37, 16, '1975-11-11', '2001-09-20', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(45, 29, '2018-12-23', '1976-07-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(36, 7, '1988-08-27', '1998-01-16', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(47, 15, '1973-01-08', '1995-11-06', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(51, 27, '1997-10-23', '1981-08-12', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 19, '2017-06-14', '1971-12-30', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(37, 8, '1996-02-20', '1970-04-14', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 29, '1972-10-22', '1995-12-03', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(46, 30, '1970-01-01', '1974-04-01', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(38, 17, '1976-01-09', '2013-02-14', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 20, '1971-11-18', '1986-02-08', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(52, 28, '2013-01-20', '1993-01-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(48, 16, '2000-08-09', '1973-06-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(38, 9, '1970-02-27', '1970-01-17', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(21, 8, '1992-12-15', '1990-07-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(37, 28, '1992-07-28', '1980-02-03', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(49, 17, '1979-02-03', '1970-01-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(22, 9, '1985-10-01', '1985-06-27', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(14, 10, '2002-08-31', '2007-07-17', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(38, 29, '1971-09-06', '1977-07-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(18, 1, '2004-05-02', '1970-01-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 21, '1996-08-26', '2019-07-12', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(39, 10, '2011-09-03', '1970-02-27', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(23, 10, '1970-01-08', '2009-06-29', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(39, 18, '1979-02-06', '2010-07-14', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 30, '1970-05-22', '1982-08-22', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(39, 30, '2012-05-13', '1970-01-07', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(24, 11, '2017-08-01', '1995-04-21', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(11, 1, '1971-05-03', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(19, 2, '1990-10-21', '1970-01-01', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(53, 29, '2017-11-10', '2007-10-29', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 22, '1977-05-27', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(40, 19, '2015-06-30', '2009-12-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(20, 3, '1980-12-27', '2006-06-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(25, 12, '1971-06-13', NULL, 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(36, 23, '1979-03-07', '1996-10-01', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(15, 11, '2016-11-21', '1990-09-17', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(21, 4, '2005-08-14', '1971-07-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(2, 1, '2016-11-08', '1970-08-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(50, 18, '1973-10-19', '1971-10-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(12, 2, '2001-11-18', '2010-02-03', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(37, 24, '1998-03-16', '1970-02-07', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(22, 5, '1989-08-28', '1970-02-25', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(54, 30, '1976-11-14', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(41, 20, '2004-01-09', '1976-07-17', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(28, 22, '1970-01-02', '1970-01-02', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(13, 3, '1971-11-11', '1973-01-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(26, 1, '2014-06-14', '1995-09-04', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(40, 11, '1971-01-20', '1977-08-25', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(51, 19, '1979-10-08', '1975-02-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(16, 12, '1970-01-04', '2010-11-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(38, 25, '1972-09-03', '1992-05-02', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(3, 2, '1976-02-11', '1970-01-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 23, '1981-05-21', '1970-01-06', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(41, 12, '1983-09-18', '2005-01-04', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(14, 4, '1970-01-09', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(42, 21, '1970-01-05', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(23, 6, '2012-03-29', '1970-02-23', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(17, 13, '2000-02-05', '1970-03-26', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(27, 2, '1998-04-10', '1983-01-18', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(4, 3, '1973-02-05', '1970-03-24', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(39, 26, '1991-07-13', '1993-05-13', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(52, 20, '1971-02-14', '2003-10-28', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(18, 14, '2014-07-26', '2004-02-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(5, 4, '1973-12-01', '2018-10-29', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(42, 13, '2004-06-29', '1975-09-27', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(19, 15, '1988-07-20', '1981-11-16', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 24, '1970-02-07', '1974-02-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(24, 7, '1983-08-15', '2001-11-12', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(43, 22, '2015-12-28', '1978-01-27', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(6, 5, '1978-01-07', '1981-04-18', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(15, 5, '2005-12-31', '2002-03-17', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 25, '2006-03-04', '1975-01-19', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(20, 16, '1993-07-15', '1970-10-23', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(53, 21, '1970-04-02', '1980-10-17', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(7, 6, '1972-04-15', '2000-12-14', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(21, 17, '1982-11-01', '1978-10-29', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(8, 7, '1971-09-15', '2004-01-18', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(40, 27, '1975-05-07', '1971-10-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(25, 8, '2002-05-16', '1972-02-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(43, 14, '2011-05-04', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(28, 3, '1995-11-12', '1992-06-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(16, 6, '2011-07-18', '1992-02-22', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 26, '2001-10-30', '2005-12-23', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(17, 7, '2010-12-01', '1970-02-16', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(22, 18, '1970-09-01', '1970-02-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 27, '1971-05-02', '1973-08-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(41, 28, '2014-07-21', '1977-07-23', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(26, 9, '1970-01-08', '1976-12-25', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(9, 8, '1970-05-24', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(54, 22, '1988-12-29', '2014-12-19', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(44, 23, '1973-02-25', '1970-01-01', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(18, 8, '1981-06-03', '1981-08-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 28, '1970-03-18', '1990-04-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 4, '2010-05-20', '1971-04-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(44, 15, '1986-10-06', '1971-07-25', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(45, 24, '1970-01-03', '1971-06-23', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(19, 9, '2020-04-25', '1989-11-12', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 5, '1972-05-07', '1982-12-03', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(55, 23, '1977-01-05', '1970-03-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 29, '1992-12-06', '2003-02-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(45, 16, '1971-12-21', '2001-09-20', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(23, 19, '1971-04-04', '2010-06-12', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(20, 10, '1995-10-05', '2017-04-23', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(46, 25, '1979-02-28', '1970-02-15', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(56, 24, '1999-09-04', '1972-11-22', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(10, 9, '2016-03-30', '1978-06-19', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(24, 20, '1993-10-23', '1971-10-18', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(36, 30, '2020-04-01', '1974-12-29', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(46, 17, '1980-08-06', '1976-06-24', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(21, 11, '1992-02-15', '1975-10-20', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(42, 29, '1970-03-01', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(57, 25, '1977-03-16', '1970-01-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 6, '1983-01-22', '1980-04-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(47, 18, '2019-12-05', '2014-09-25', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(27, 10, '1970-08-23', '1970-02-25', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(11, 10, '1995-01-18', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(43, 30, '1987-10-31', '2006-01-26', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(58, 26, '2014-02-02', '2016-12-23', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(48, 19, '1985-12-11', '1970-01-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(28, 11, '1994-05-05', '1978-04-22', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(15, 1, '1985-04-30', '2006-10-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(59, 27, '1971-06-16', '2013-12-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 12, '1971-10-24', '1976-09-17', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(49, 20, '1989-04-19', '2009-03-30', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(16, 2, '1970-01-05', '1973-11-13', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(60, 28, '2015-03-23', '1985-03-03', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(50, 21, '1981-08-26', '1990-09-21', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 13, '1987-11-18', '2011-09-23', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(25, 21, '2016-05-10', '2002-05-15', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(17, 3, '2006-01-13', '1971-06-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(47, 26, '1970-04-09', '1970-01-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 14, '1970-01-09', '2009-08-12', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 7, '2003-07-06', '2008-11-03', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(8, 1, '2001-07-21', '1978-05-11', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(61, 29, '1981-04-27', '2000-02-11', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(18, 4, '2009-12-03', '2001-07-18', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(12, 11, '1986-09-16', '1994-08-14', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 15, '2001-01-19', '1970-01-03', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(19, 5, '1987-05-05', '1980-07-18', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(26, 22, '2013-09-17', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(22, 12, '2013-11-30', '1970-05-19', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(9, 2, '2011-07-10', '1992-10-23', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(48, 27, '1987-11-04', '1995-05-23', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(51, 22, '1974-10-09', '2001-03-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 16, '1973-09-25', '1979-11-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 8, '2000-01-29', '1975-10-03', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(49, 28, '1985-09-28', '1976-06-14', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(20, 6, '1999-04-11', '1970-01-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(23, 13, '1994-11-02', '1997-11-15', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 9, '2018-07-30', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(13, 12, '1978-07-23', '1972-01-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 17, '1970-01-03', '2002-02-22', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(50, 29, '1994-10-14', '2005-11-11', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(21, 7, '2010-12-07', '1996-12-11', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 10, '1980-09-06', '1970-01-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(27, 23, '1971-01-07', '2006-06-29', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(51, 30, '1998-09-03', '2007-01-20', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(10, 3, '1976-03-13', '2020-02-29', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(24, 14, '1970-05-02', '2003-03-26', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(62, 30, '2006-07-02', '1987-10-03', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(11, 4, '1971-11-29', '1997-06-01', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 18, '1970-02-21', '1970-01-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(14, 13, '2012-06-20', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(22, 8, '2014-03-14', '1970-01-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(36, 19, '1970-02-04', '1993-02-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(52, 23, '2013-07-04', '1972-11-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(25, 15, '1993-05-29', '1984-12-31', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(12, 5, '1984-09-21', '1990-12-29', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 1, '1997-01-19', '2008-09-13', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(53, 24, '2005-08-17', NULL, 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 2, '2008-03-07', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(54, 25, '1986-01-22', '1972-12-16', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(26, 16, '1975-01-26', '1972-02-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(36, 11, '2009-12-29', '1998-03-12', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(28, 24, '1971-02-18', '1990-09-27', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(13, 6, '1980-01-29', '1970-01-08', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(27, 17, '1988-10-26', '1972-01-26', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(23, 1, '1995-11-28', '1992-07-20', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(14, 7, '1970-03-28', '1986-07-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(37, 12, '1985-11-11', '2000-05-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(28, 18, '2005-10-29', '2010-12-03', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(15, 8, '2011-02-26', '1993-08-19', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 19, '1980-09-06', '2011-06-21', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(16, 9, '2015-12-12', '2015-10-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 20, '1982-02-20', '1970-01-01', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(17, 10, '1970-01-02', '1970-01-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(24, 2, '1987-01-03', '2001-05-12', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 21, '1992-04-05', '2005-12-16', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(23, 9, '1975-02-13', '1973-08-12', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(36, 3, '1985-05-08', '1998-08-25', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(55, 26, '1997-11-24', '1990-07-16', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(37, 4, '2015-04-13', '1972-06-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(18, 11, '1978-10-12', '1991-07-08', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(37, 20, '1978-01-06', '1970-01-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(56, 27, '1987-02-26', '2003-02-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(38, 5, '1974-06-24', '1987-01-17', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(15, 14, '2016-10-29', '1970-01-20', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 25, '1978-05-03', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 22, '1977-12-13', '1999-08-18', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(24, 10, '1987-05-06', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(38, 21, '1990-05-28', '2015-03-27', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(19, 12, '1971-06-02', '1996-11-15', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(38, 13, '2000-12-23', '1992-05-18', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 23, '2017-09-14', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(57, 28, '1970-01-11', '2010-06-22', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(16, 15, '1970-01-03', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 26, '1987-10-15', '1981-12-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(39, 6, '1970-10-28', '1970-01-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(20, 13, '1972-09-22', '1988-11-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(25, 11, '1975-01-25', '1970-01-02', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(58, 29, '1971-11-24', '1979-07-01', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(40, 7, '1984-04-14', '2005-05-28', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(17, 16, '1989-11-06', '1989-11-18', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(25, 3, '1997-09-19', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(39, 14, '1971-09-22', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(59, 30, '1988-09-16', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 24, '1999-06-03', '1984-01-26', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(41, 8, '1970-03-11', '1970-04-09', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(39, 22, '1977-12-16', '1998-08-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 27, '2015-07-14', '1986-09-12', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(18, 17, '2014-01-12', '1995-10-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 1, '1972-02-17', '1986-06-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(42, 9, '1988-03-12', '1970-01-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 28, '1989-09-29', '2020-01-31', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(19, 18, '2001-12-18', '1997-05-20', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(26, 4, '1985-12-02', '1996-11-12', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 2, '1971-08-28', '2016-11-01', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(51, 25, '1977-07-12', '1991-01-13', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(26, 12, '1970-01-03', '2011-04-12', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 29, '2004-10-05', '1991-11-21', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(20, 19, '1970-01-07', '2008-01-11', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(40, 23, '2006-12-14', '1970-12-17', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 3, '1997-01-24', '1970-02-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 30, '1971-02-16', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(52, 26, '2017-09-01', '1970-04-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(40, 15, '1978-07-24', '2020-03-21', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 4, '1970-01-08', '1981-01-12', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(27, 13, '1971-07-17', '1971-06-17', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(21, 20, '1994-01-02', '1990-04-01', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(6, 1, '1973-12-14', '2010-03-02', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(27, 5, '2001-12-07', '1978-09-29', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(53, 27, '2002-12-29', '2010-03-19', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(22, 21, '2017-06-11', '1990-07-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(7, 2, '1984-07-22', '1970-01-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 5, '1985-10-19', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(54, 28, '1993-01-06', '1999-03-23', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(41, 16, '2009-07-15', '1983-08-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(36, 6, '1970-10-21', '1994-07-25', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(55, 29, '1995-03-16', '1981-07-22', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(41, 24, '1970-11-30', '2004-11-09', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(28, 14, '1973-08-30', '1970-01-28', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(23, 22, '1994-05-23', '1972-08-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(8, 3, '1996-10-24', '1970-02-01', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(42, 25, '1991-09-09', '1990-10-29', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 15, '1972-03-30', '2010-10-16', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(28, 6, '2010-02-09', '1970-01-15', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(24, 23, '1994-03-04', '1970-01-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(42, 17, '1970-01-10', NULL, 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(43, 26, '2006-03-15', '1993-08-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 16, '1986-03-26', '1970-01-07', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(9, 4, '2006-04-02', '2001-09-06', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(25, 24, '1970-01-04', '1972-09-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(44, 27, '1970-04-02', '1970-01-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 7, '1975-03-07', '1979-11-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 17, '2005-02-02', '1987-01-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(10, 5, '1995-07-07', '1970-02-27', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(45, 28, '1970-01-22', '1970-02-21', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(43, 18, '1976-09-15', '1983-01-19', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(26, 25, '2009-04-13', '2018-03-21', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 18, '1990-04-24', '2015-03-01', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(46, 29, '2016-04-07', '2002-02-27', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(11, 6, '2007-10-27', '1970-04-12', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(27, 26, '2011-05-23', '1989-06-11', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 19, '2009-11-01', '2015-09-24', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(12, 7, '2008-07-08', '1983-11-29', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 8, '1970-01-03', '1971-05-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(44, 19, '2014-12-24', '1976-08-31', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(28, 27, '1999-04-01', '1994-02-16', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(13, 8, '2012-02-15', '1970-01-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 28, '2017-06-21', '1999-09-10', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 9, '1972-02-21', '1984-10-25', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(14, 9, '1999-06-13', '1970-01-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(45, 20, '1979-10-29', '1983-05-22', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 10, '1973-04-17', '1997-02-20', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 29, '2017-11-01', '1970-01-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(15, 10, '1972-05-17', '1983-12-13', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(47, 30, '1979-08-21', '1972-07-21', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(46, 21, '2013-10-27', '1971-02-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 20, '1985-07-02', '1996-04-19', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 11, '1987-04-22', '1982-03-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(19, 1, '1971-12-05', '1971-06-18', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 21, '1976-03-28', '1970-07-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(20, 2, '1970-01-03', '2019-08-18', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(47, 22, '1979-02-01', '2004-06-30', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(34, 12, '2004-12-30', '2006-08-06', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(48, 23, '1983-12-11', '1970-06-18', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(36, 22, '2007-09-10', '1992-09-27', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 30, '1970-10-14', '1970-02-15', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(16, 11, '1970-02-10', '1970-01-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(21, 3, '1990-06-23', '2017-06-18', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(3, 1, '1970-08-31', '1990-08-23', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(17, 12, '1971-09-16', '2003-03-09', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(35, 13, '2004-09-07', '1970-01-03', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(4, 2, '2015-11-12', '1970-03-30', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(49, 24, '1970-01-07', '1982-12-13', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(36, 14, '2019-04-07', '2017-03-14', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(50, 25, '1981-05-23', '1970-01-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(37, 15, '1984-09-14', '1980-04-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(18, 13, '1970-01-09', '1984-10-20', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(51, 26, '2011-08-27', '2017-05-02', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(38, 16, '2006-04-04', '1981-02-15', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(5, 3, '1971-02-26', '1996-11-20', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(37, 23, '2008-06-27', '1976-05-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(22, 4, '1970-04-26', '1999-07-05', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(38, 24, '1970-01-06', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(52, 27, '1970-01-04', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(19, 14, '2015-04-21', '1997-06-19', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(39, 17, '1985-09-08', '2006-04-01', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(23, 5, '1972-02-12', NULL, 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(39, 25, '1983-08-03', '1998-01-26', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(53, 28, '1975-07-06', '1971-04-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(24, 6, '1976-04-09', '1994-10-08', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(6, 4, '1986-09-22', '1979-05-29', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(20, 15, '1975-09-20', NULL, NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(40, 18, '1970-07-12', '1985-10-11', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(54, 29, '1970-01-22', '1981-06-03', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(40, 26, '1971-07-25', '1970-01-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(25, 7, '1990-05-09', '1987-04-29', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(41, 19, '1970-03-21', '1989-06-04', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(7, 5, '1981-11-17', '1988-02-23', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(21, 16, '1990-12-05', '1976-07-16', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(41, 27, '1989-02-18', '1970-01-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(55, 30, '1970-02-06', '2017-07-11', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(42, 20, '1970-01-04', '1985-09-01', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(27, 1, '2014-05-23', '1992-06-05', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(43, 21, '1983-01-08', '1974-11-25', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(8, 6, '2005-08-11', '2002-01-14', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(28, 2, '1972-08-27', '1970-01-04', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(44, 22, '1983-05-04', '1970-01-07', NULL);
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(26, 8, '1970-01-02', '1990-04-20', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(42, 28, '1982-02-20', '1990-01-04', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(27, 9, '2000-04-07', '2000-12-14', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(43, 29, '1970-01-03', '2004-01-12', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(22, 17, '1988-11-02', '2018-09-06', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(9, 7, '2004-11-22', '1976-02-06', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 3, '1972-12-24', '1970-01-05', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(23, 18, '1998-11-01', '2010-01-26', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(10, 8, '1996-10-25', NULL, 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(45, 23, '1975-07-23', '1984-12-16', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 4, '1984-02-08', '2019-02-27', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(46, 24, '1970-01-14', '2006-10-25', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 5, '1972-04-15', '1970-01-08', 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(47, 25, '2018-01-01', '2001-12-21', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 6, '2015-09-15', '2006-11-03', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(48, 26, '1996-06-23', '1974-03-06', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(24, 19, '1970-02-02', '2016-10-09', 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(11, 9, '1987-12-28', '1976-02-19', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(25, 20, '1970-01-02', '2013-03-28', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(12, 10, '1970-02-14', '1973-12-01', 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(26, 21, '1970-01-08', '1991-01-20', 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(13, 11, '1970-01-22', NULL, 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(27, 22, '2007-03-24', NULL, 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(14, 12, '1987-10-24', NULL, 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(28, 23, '1970-01-02', NULL, 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(15, 13, '1975-09-07', NULL, 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(29, 24, '2017-10-04', NULL, 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(16, 14, '1970-01-09', NULL, 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(30, 25, '1981-08-06', NULL, 'treasurer');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(17, 15, '1998-10-23', NULL, 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(31, 26, '1978-05-05', NULL, 'secretary');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(18, 16, '1991-04-09', NULL, 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(32, 27, '2017-07-21', NULL, 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(19, 17, '1974-02-15', NULL, 'vocal');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(33, 28, '2004-01-30', NULL, 'president');
INSERT INTO ciudadano_gremio(id_ciudadano, id_gremio, fecha_alta, fecha_baja, cargo) VALUES
(20, 18, '1976-07-31', NULL, 'vocal');

--
-- Inserting data into table negocio
--
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(1, 'Federal Space Explore Inc.', 'hospedaje', 18);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(2, 'Smart Research Corporation', 'taberna', 901);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(3, 'National Space Explore Group', 'hospedaje', 896);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(4, 'WorldWide High-Technologies Inc.', 'comercio', 311);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(5, 'East Systems Group', 'hospedaje', 437);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(6, 'Future Protection Inc.', 'hospedaje', 256);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(7, 'General High-Technologies Corporation', 'comercio', 520);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(8, 'United Logics Group', NULL, NULL);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(9, 'North Wind Resources Corp.', 'comercio', 172);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(10, 'Western High-Technologies Inc.', 'hospedaje', 632);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(11, 'Home High-Technologies Corporation', 'hospedaje', 510);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(12, 'Domestic Foods Inc.', 'hospedaje', 366);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(13, 'General 4G Wireless Corporation', 'taberna', 706);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(14, 'Home Cars Battery Inc.', 'hospedaje', 335);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(15, 'East Business Group', 'hospedaje', 567);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(16, 'Federal 2G Wireless Co.', 'comercio', 113);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(17, 'City Business Group', 'comercio', 493);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(18, 'Home High-Technologies Inc.', 'hospedaje', 708);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(19, 'International High-Technologies Corp.', 'comercio', 578);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(20, 'WorldWide High-Technologies Corporation', 'comercio', 234);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(21, 'Pacific Space Explore Group', 'comercio', 126);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(22, 'Professional D-Mobile Group', 'hospedaje', 95);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(23, 'National Computers Group', 'comercio', 665);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(24, 'Domestic Entertainment Inc.', 'comercio', 645);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(25, 'Advanced High-Technologies Group', 'comercio', 760);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(26, 'Special Thermal Power Co.', 'comercio', 894);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(27, 'General Space Research Inc.', 'comercio', 606);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(28, 'First I-Mobile Co.', 'hospedaje', 484);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(29, 'Home Solar Energy Corporation', 'comercio', 78);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(30, 'Smart Space Explore Group', 'comercio', 525);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(31, 'Beyond Oil Corporation', 'comercio', 920);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(32, 'WorldWide High-Technologies Corporation', 'hospedaje', 411);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(33, 'International Automotive Co.', 'hospedaje', 808);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(34, 'Global A-Mobile Corporation', 'comercio', 358);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(35, 'Domestic Space Explore Corporation', 'hospedaje', 726);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(36, 'Home Y-Mobile Corporation', 'hospedaje', 178);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(37, 'Western High-Technologies Corp.', 'hospedaje', 605);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(38, 'Federal Delivery Co.', 'taberna', 970);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(39, 'Beyond Systems Corp.', 'comercio', 718);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(40, 'North Space Explore Corp.', 'hospedaje', 206);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(41, 'Future Space Research Corporation', 'hospedaje', 354);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(42, 'North Space Explore Corp.', 'comercio', 426);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(43, 'East High-Technologies Inc.', 'comercio', 792);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(44, 'Pacific Petroleum Corporation', 'hospedaje', 733);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(45, 'General G-Mobile Inc.', 'comercio', 357);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(46, 'Future Systems Inc.', 'comercio', 968);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(47, 'Smart Entertainment Corporation', 'hospedaje', 705);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(48, 'Pacific J-Mobile Inc.', 'taberna', 434);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(49, 'Advanced Equipment Inc.', 'comercio', 362);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(50, 'Global Space Explore Inc.', 'hospedaje', 99);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(51, 'Beyond Wave Resources Corporation', NULL, NULL);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(52, 'Home Space Explore Corporation', 'comercio', 260);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(53, 'Future Space Research Corporation', 'comercio', 93);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(54, 'Western Space Research Inc.', NULL, NULL);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(55, 'Flexible Green Energy Inc.', 'comercio', 64);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(56, 'Professional Natural Gas Power Corp.', 'comercio', 873);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(57, 'Home J-Mobile Inc.', 'comercio', 51);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(58, 'WorldWide Optics Group', 'hospedaje', 542);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(59, 'Beyond Insurance Corporation', 'comercio', 641);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(60, 'Global Optics Inc.', 'hospedaje', 214);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(61, 'Western Cars Group', 'comercio', 567);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(62, 'National Engineering Group', 'taberna', 916);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(63, 'Union Logistic Inc.', 'comercio', 943);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(64, 'Advanced S-Mobile Group', 'comercio', 355);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(65, 'City Insurance Inc.', 'hospedaje', 655);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(66, 'General Business Group', 'hospedaje', 884);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(67, 'First F-Mobile Co.', 'hospedaje', 558);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(68, 'Beyond Industry Group', NULL, NULL);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(69, 'Pacific O-Mobile Inc.', 'taberna', 795);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(70, 'Creative Fossil Fuel Resources Corp.', 'taberna', 297);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(71, 'South Explore Inc.', 'hospedaje', 964);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(72, 'Future Delivery Corporation', 'comercio', 603);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(73, 'City G-Mobile Inc.', 'comercio', 986);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(74, 'Domestic High-Technologies Corp.', 'hospedaje', 959);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(75, 'North Financial Experts Inc.', 'comercio', 657);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(76, 'First Space Explore Co.', 'comercio', 809);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(77, 'General Laboratories Co.', 'comercio', 173);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(78, 'International High-Technologies Group', 'comercio', 168);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(79, 'General Research Co.', 'comercio', 694);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(80, 'East Thermal Resources Corporation', 'comercio', 76);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(81, 'Smart Automotive Corp.', 'taberna', 528);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(82, 'Global Space Explore Corporation', 'hospedaje', 994);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(83, 'Pacific N-Mobile Inc.', 'hospedaje', 512);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(84, 'Special Solar Energy Inc.', 'hospedaje', 59);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(85, 'South Mobile Co.', 'comercio', 245);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(86, 'Advanced High-Technologies Inc.', 'comercio', 234);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(87, 'Special 3G Wireless Corporation', 'hospedaje', 250);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(88, 'North High-Technologies Co.', 'comercio', 503);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(89, 'Future 5G Wireless Inc.', 'hospedaje', 964);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(90, 'Professional High-Technologies Corp.', 'comercio', 510);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(91, 'WorldWide O-Mobile Group', 'comercio', 87);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(92, 'Home Solar Resources Corporation', 'comercio', 989);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(93, 'Special Systems Corp.', 'comercio', 910);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(94, 'Professional P-Mobile Inc.', 'comercio', 819);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(95, 'Domestic Space Research Corporation', NULL, 488);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(96, 'City O-Mobile Corporation', NULL, NULL);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(97, 'National X-Mobile Group', NULL, 833);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(98, 'National Space Research Corporation', NULL, 675);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(99, 'Federal High-Technologies Inc.', NULL, NULL);
INSERT INTO negocio(id, nombre, tipo, id_edificio) VALUES
(100, 'First High-Technologies Corp.', NULL, 926);